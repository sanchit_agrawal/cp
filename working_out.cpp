#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	vector <vector <int> > v(n+2, vector <int>(m+2, 0));
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			cin >> v[i][j];
		}
	}
	vector <vector <int>> topleft(n+2, vector <int>(m+2, 0));
	vector <vector <int>> bottomright(n+2, vector <int>(m+2, 0));
	vector <vector <int>> topright(n+2, vector <int>(m+2, 0));
	vector <vector <int>> bottomleft(n+2, vector <int>(m+2, 0));;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			topleft[i][j] = v[i][j] + max(topleft[i-1][j], topleft[i][j-1]);
			topright[i][m-j+1] = v[i][m-j+1] + max(topright[i-1][m-j+1], topright[i][m-j+2]);
			bottomleft[n-i+1][j] = v[n-i+1][j] + max(bottomleft[n-i+2][j], bottomleft[n-i+1][j-1]);
			bottomright[n-i+1][m-j+1] = v[n-i+1][m-j+1] + max(bottomright[n-i+2][m-j+1], bottomright[n-i+1][m-j+2]);
		}
	}
	int ans = 0;
	for (int i = 2; i < n; i++) {
		for (int j = 2; j < m; j++) {
			ans = max(ans, topleft[i-1][j] + bottomleft[i][j-1] + bottomright[i+1][j] + topright[i][j+1]);
			ans = max(ans, topleft[i][j-1] + bottomleft[i+1][j] + bottomright[i][j+1] + topright[i-1][j]);
		}
	}
	cout << ans;
	return 0;
}