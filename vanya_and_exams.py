n, r, avg = map(int, raw_input().split())
tot = avg * n
exams = []
for i in range(n):
	a, b = map(int, raw_input().split())
	exams.append((a, b))
exams.sort(key = lambda x : x[1])
curr_score = sum([e[0] for e in exams])
ans = 0
for (a, b) in exams:
	extra_score = min(r-a, tot-curr_score)
	ans += b * extra_score
	curr_score += extra_score
print max(ans, 0)