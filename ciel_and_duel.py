n, m = map(int, raw_input().split())
jiro = []
for _ in range(n):
	t, s = raw_input().split()
	s = int(s)
	if t == 'ATK':
		jiro.append((s, 1))
	else:
		jiro.append((s+1, 0))
ciel = []
for _ in range(m):
	ciel.append(int(raw_input()))
jiro.sort()
ciel.sort()
print jiro
i, j = 0, 0
damage_1 = 0
done = True
while True:
	if i == n and j == m:
		break
	elif i == n:
		damage_1 += ciel[j]
	elif j == m:
		done = False
		break
	else:
		if ciel[j] >= jiro[i][0]:
			if jiro[i][1] == 1:
				damage_1 += ciel[j] - jiro[i][0]
			i += 1
		else:
			damage_1 += ciel[j]
	j += 1
if not done:
	damage_1 = 0
atks = [s for s, t in jiro if t == 1]
n = len(atks)
ciel.reverse()
damage_2 = 0
i, j = 0, 0
while True:
	if i == n or j == m:
		break
	else:
		if ciel[j] < atks[i]:
			break
		else:
			damage_2 += ciel[j] - atks[i]
			i += 1
			j += 1
print (damage_1, damage_2)
