#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <utility>

using namespace std;

int n, l, k;
double dp[201][201][201];
double prob[201];
int capacity[201];

double ans(int r, int won, int cap) {
	if (r == 0) {
		if (won < l || cap < won) {
			return 0;
		} else {
			return 1;
		}
	} 
	if (dp[r][won][cap] != -1) {
		return dp[r][won][cap];
	}
	double out = 0;
	out = prob[r] * ans(r-1, won+1, min(n, cap + capacity[r]));
	out += (1-prob[r]) * ans(r-1, won, cap);
	return dp[r][won][cap] = out;
}

int main() {
	cin >> n >> l >> k;
	for (int i = 1; i <= n; i++) {
		cin >> prob[i];
		prob[i] /= 100;
	}
	for (int i = 1; i <= n; i++) {
		cin >> capacity[i];
		capacity[i]++;
	}
	for (int i = 0; i <= n; i++) {
		for (int j = 0; j <= n; j++) {
			for (int p = 0; p <= n; p++) {
				dp[i][j][p] = -1;
			}
		}
	}
	cout << setprecision(8) << ans(n, 0, min(n, k));
	return 0;
}