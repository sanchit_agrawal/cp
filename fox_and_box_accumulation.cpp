#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	sort(v.begin(), v.end());
	int ans = 0;
	int curr_max = 0;
	for(int i = n-1; i >= 0; i--)
	{
		if(curr_max == 0)
		{
			ans++;
			curr_max = v[i];
		}
		else
		{
			curr_max--;
			curr_max = min(v[i], curr_max);
		}
	}
	cout << ans;
	return 0;
}