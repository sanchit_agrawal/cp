n = int(raw_input())
adjacency_list = [[] for i in range(n)]
for i in range(n-1):
	a, b = map(int, raw_input().split())
	adjacency_list[a-1].append(b-1)
	adjacency_list[b-1].append(a-1)
vals = map(int, raw_input().split())
def dfs(root, parent, adjacency_list, vals):
	child_add = [0]
	child_sub = [0]
	for child in adjacency_list[root]:
		if child != parent:
			a, s = dfs(child, root, adjacency_list, vals)
			child_add.append(a)
			child_sub.append(s)
	add = max(child_add)
	sub = max(child_sub)
	if add - sub + vals[root] < 0:
		add += -(add - sub + vals[root])
	else:
		sub += add - sub + vals[root]
	return (add, sub)
add, sub = dfs(0, -1, adjacency_list, vals)
print add + sub
