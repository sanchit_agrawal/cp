n = int(raw_input())
d = {}
ans = 1
for i in range(n):
	h, m = map(int, raw_input().split())
	if (h, m) in d:
		d[(h, m)] += 1
		ans = max(ans, d[(h, m)])
	else:
		d[(h, m)] = 1
print ans