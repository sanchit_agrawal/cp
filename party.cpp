#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int bfs(vector<int>& queue, vector<vector<int>>& adjacency_list)
{
	if(queue.size() == 0)
		return 0;
	vector <int> new_queue;
	for(int q : queue)
		for(int neighbor : adjacency_list[q])
			new_queue.push_back(neighbor);
	return 1 + bfs(new_queue, adjacency_list);
}

int main()
{
	int n;
	cin >> n;
	vector <int> manager(n);
	for(int i = 0; i < n; i++)
	{
		int p;
		cin >> p;
		manager[i] = p-1;
	}
	vector <int> top;
	vector <vector<int>> juniors(n);
	for(int i = 0; i < n; i++)
		if(manager[i] < 0)
			top.push_back(i);
		else
			juniors[manager[i]].push_back(i);
	cout << bfs(top, juniors);
	return 0;
}