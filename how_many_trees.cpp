#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

long long ans[36][36];
long long ans2[36];

long long n_trees(int size, int h) {
	if (size == 0) return 1;
	if (h == 0) return 0;
	if (size == 1)	return 1;
	if (ans[size][h] != 0) return ans[size][h];
	long long out = 0;
	for (int i = 0; i <= size-1; i++) {
		out += n_trees(i, h-1) * n_trees(size-1-i, h-1);
	}
	return ans[size][h] = out;
}

long long tot_bin_trees(int n) {
	if (n == 0) return 1;
	if (ans2[n] != 0) return ans2[n];
	long long out = 0;
	for (int i = 0; i <= n-1; i++) {
		out += tot_bin_trees(i) * tot_bin_trees(n-i-1);
	}
	return ans2[n] = out;
}

int main() {
	int n, h;
	cin >> n >> h;
	cout << tot_bin_trees(n) - n_trees(n, h-1);
	return 0;
}