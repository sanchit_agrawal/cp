#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

int main()
{
	int a, b, c;
	cin >> a >> b >> c;
	int m;
	cin >> m;
	vector <pair<int, bool>> mouse(m);
	for(int i = 0; i < m; i++)
	{
		cin >> mouse[i].first;
		string s;
		cin >> s;
		if(s[0] == 'U')
			mouse[i].second = true;
		else
			mouse[i].second = false;
	}
	sort(mouse.begin(), mouse.end());
	int i = 0;
	int total = a + b + c;
	long long cost = 0;
	while(i < m && a + b + c > 0)
	{
		if(mouse[i].second)
		{
			if(a)
			{
				a--;
				cost += mouse[i].first;
			}
			else if(c)
			{
				c--;
				cost += mouse[i].first;
			}
		}
		else
		{
			if(b)
			{
				b--;
				cost += mouse[i].first;
			}
			else if(c)
			{
				c--;
				cost += mouse[i].first;
			}
		}
		i++;
	}
	cout << total - (a + b + c) << ' ' << cost << '\n';
	return 0;
}