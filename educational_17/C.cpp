#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	string s, t;
	cin >> s >> t;
	int n = s.size(), m = t.size();
	int l = -1, r = n;
	vector <int> left(m);
	for(int i = 0; i < m; i++)
	{
		while(l < n-1 && s[l+1] != t[i])
			l++;
		if(l < n-1)
			left[i] = ++l;
		else
			left[i] = n;
	}
	vector <int> right(m);
	for(int i = m-1; i >= 0; i--)
	{
		while(r > 0 && s[r-1] != t[i])
			r--;
		if(r > 0)
			right[i] = --r;
		else
			right[i] = -1;
	}
	int currr = 0;
	while(currr < m && right[currr] == -1)
		currr++;
	int bestl = -1;
	int bestr = currr;
	int currl = m-1;
	while(currl >= 0 && left[currl] >= n)
		currl--;
	if(currl+1 >= (m-bestr))
	{
		bestl = currl;
		bestr = m;
	}
	currr = 0;
	for(int i = 0; i < m && left[i] != n; i++)
	{
		while(currr < m && right[currr] <= left[i])
			currr++;

		if(m-currr+i+1 > bestl+1 + m-bestr)
		{
			// cout << (currr < m) << endl;
			// cout << (right[currr] <= left[i]) << endl;
			// cout << right[currr] << ' ' << left[i] << endl;
			bestl = i;
			bestr = currr;
			// cout << "update" << endl;
			// cout << bestl << endl;
			// cout << bestr << endl;

		}
	}
	// cout << "bestl = " << bestl << endl;
	// cout << "bestr = " << bestr << endl;
	// cout << "left = " << left[bestl] << endl;
	// cout << "right = " << right[bestr] << endl;

	if(bestl+1 + m-bestr == 0)
		cout << '-';
	else
	{
		for(int i = 0; i <= bestl; i++)
			cout << t[i];
		for(int i = bestr; i < m; i++)
			cout << t[i];
	}
	return 0;
}	