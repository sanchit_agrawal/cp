#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	long long n, k;
	cin >> n >> k;
	int i;
	int count = 0;
	vector <long long> divs;
	for(i = 1; i*i < n; i++)
		if(n % i == 0)
		{
			divs.push_back(i);
			count += 2;
		}
	if(i*i == n)
	{
		divs.push_back(i);
		count++;
	}
	if(count < k)
		cout << -1;
	else
	{
		if(k < divs.size())
			cout << divs[k-1];
		else
			cout << n/divs[k-divs.size()];
	}
	return 0;
}