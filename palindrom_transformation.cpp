#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	int n, p;
	cin >> n >> p;
	string s;
	cin >> s;
	int score = 0;
	for(int i = 0; i < n/2; i++)
		score += min(abs(s[i]-s[n-1-i]), 26-abs(s[i]-s[n-1-i]));
	if(n % 2)
		score += n/2 + min(min(p - 1, n - p) - 1, abs((n + 1)/2 - p));
	else
		score += n/2 + min(min(p - 1, n - p) - 1, min(abs((n + 2)/2 - p), abs(n/2 - p)) - 1);
	cout << score;
	return 0;
}