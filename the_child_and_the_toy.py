n, m = map(int, raw_input().split())
value = map(int, raw_input().split())
cost = 0
for i in range(m):
	u, v = map(int, raw_input().split())
	cost += min(value[u-1], value[v-1])
print cost