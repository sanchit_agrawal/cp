#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int main() {
	string s, t;
	cin >> s >> t;
	int n = s.size(), m = t.size();
	int s_div_ind = n-1;
	for (int i = 0; i < n; i++) {
		if (n % (i + 1) != 0) {
			continue;
		}
		bool found = true;
		for (int j = 0; j < n; j++) {
			if (s[j] != s[j % (i + 1)]) {
				found = false;
				break;
			}
		}
		if (found) {
			s_div_ind = i;
			break;
		}
	}
	int t_div_ind = m-1;
	for (int i = 0; i < m; i++) {
		if (m % (i + 1) != 0) {
			continue;
		}
		bool found = true;
		for (int j = 0; j < m; j++) {
			if (t[j] != t[j % (i + 1)]) {
				found = false;
				break;
			}
		}
		if (found) {
			t_div_ind = i;
			break;
		}
	}
	if (s.substr(0, s_div_ind + 1) != t.substr(0, t_div_ind + 1)) {
		cout << 0;
	} else {
		int count = 0;
		int i;
		for (i = 1; i*i < n / (s_div_ind + 1); i++) {
			if ((n / (s_div_ind + 1)) % i == 0) {
				if ((m / (t_div_ind + 1)) % i == 0) {
					count++;
				}
				if (((m / (t_div_ind + 1)) % ((n / (s_div_ind + 1)) / i) == 0)) {
					count++;
				}
			}
		}
		if (i*i == n / (s_div_ind + 1) && (m / (t_div_ind + 1)) % i == 0) {
			count++;
		}
		cout << count;
	}
	return 0;
}