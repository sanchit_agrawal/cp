#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector <int> primes;

int binsearch(int x, int l, int r)
{
	if(l == r)
		return primes[l];
	int mid = (l+r)/2;
	if(primes[mid] == x)
		return x;
	else if(primes[mid] < x)
		return binsearch(x, mid+1, r);
	else
		return binsearch(x, l, mid);
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector <bool> is_prime(100100, true);
	for(int i = 2; i < 100100; i++)
		if(is_prime[i])
		{
			primes.push_back(i);
			for(int j = 2*i; j < 100100; j += i)
				is_prime[j] = false;
		}
	vector <vector <int> > mat(n, vector <int>(m));
	for(int i = 0; i < n; i++)
		for(int j = 0; j < m; j++)
		{
			cin >> mat[i][j];
			mat[i][j] = binsearch(mat[i][j], 0, (int) primes.size()-1) - mat[i][j];
		}

	// for(int i = 0; i < n; i++)
	// {
	// 	for(int j = 0; j < m; j++)
	// 		cout << mat[i][j] << ' ';
	// 	cout << '\n';
	// }

	int ans = 100000 * 500;
	for(int i = 0; i < n; i++)
	{
		int curr = 0;
		for(int j = 0; j < m; j++)
			curr += mat[i][j];
		ans = min(ans, curr);
	}
	for(int j = 0; j < m; j++)
	{
		int curr = 0;
		for(int i = 0; i < n; i++)
			curr += mat[i][j];
		ans = min(ans, curr);
	}
	cout << ans;
	return 0;
}