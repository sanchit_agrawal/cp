#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector <int> nums;
	for(int i = 1; ; i++)
	{
		long long ans = (long long) i*(i + 1)/2;
		if(ans > 1000000000)
			break;
		nums.push_back(ans);
	}
	int n = nums.size();
	int l = 0, r = n - 1;
	int needle;
	cin >> needle;
	while(l <= r)
	{
		int ans = nums[l] + nums[r];
		if(ans == needle)
			break;
		else if(ans > needle)
			r--;
		else
			l++;
	}
	if(l > r)
		cout << "NO";
	else
		cout << "YES";
	return 0;
}