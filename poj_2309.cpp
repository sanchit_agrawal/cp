#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	while(n--)
	{
		long root;
		cin >> root;
		if(root % 2 == 1)
		{
			cout << root << ' ' << root << endl;
			continue;
		}
		long max2pow = 0;
		for(long p = 1; p <= root; p *= 2)
		{
			// cout << "here1" << endl;
			if(root % p == 0)
				max2pow = p;
		}
		long diff = max2pow-1;
		cout << root-diff << ' ' << root+diff << endl;
	}
	return 0;
}