#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	if(n > m)
	{
		int temp = n;
		n = m;
		m = temp;
	}
	bool last_was_red = true;
	int red_count = n-1;
	int blue_count = m;
	int score1 = 0;
	int score2 = 0;
	for(int i = 1; i < n+m; i++)
	{
		if(i % 2 == 0)
		{
			if(last_was_red)
			{
				if(red_count)
				{
					red_count--;
					score1++;
				}
				else
				{
					blue_count--;
					score2++;
					last_was_red = false;
				}
			}
			else
			{
				if(blue_count)
				{
					blue_count--;
					score1++;
				}
				else
				{
					red_count--;
					score2++;
					last_was_red = true;
				}
			}
		}
		else
		{
			if(last_was_red)
			{
				if(blue_count)
				{
					blue_count--;
					score2++;
					last_was_red = false;
				}
				else
				{
					red_count--;
					score1++;
				}
			}
			else
			{
				if(red_count)
				{
					red_count--;
					score2++;
					last_was_red = true;
				}
				else
				{
					blue_count--;
					score1++;
				}
			}
		}
	}
	int tscore1 = score1;
	int tscore2 = score2;
	score1 = 0;
	score2 = 0;
	red_count = n;
	blue_count = m-1;
	last_was_red = false;
	for(int i = 1; i < n+m; i++)
	{
		if(i % 2 == 0)
		{
			if(last_was_red)
			{
				if(red_count)
				{
					red_count--;
					score1++;
				}
				else
				{
					blue_count--;
					score2++;
					last_was_red = false;
				}
			}
			else
			{
				if(blue_count)
				{
					blue_count--;
					score1++;
				}
				else
				{
					red_count--;
					score2++;
					last_was_red = true;
				}
			}
		}
		else
		{
			if(last_was_red)
			{
				if(blue_count)
				{
					blue_count--;
					score2++;
					last_was_red = false;
				}
				else
				{
					red_count--;
					score1++;
				}
			}
			else
			{
				if(red_count)
				{
					red_count--;
					score2++;
					last_was_red = true;
				}
				else
				{
					blue_count--;
					score1++;
				}
			}
		}
	}
	cout << max(tscore1, score1) << ' ' << min(tscore2, score2);
	return 0;
}