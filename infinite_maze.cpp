#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>

#define NONE 1000000000

using namespace std;

int n, m, sx, sy;

int mod(int a, int b) {
	while (a < 0) a += b;
	return a % b;
}

bool dfs(int x, int y, char v[1500][1501], pair <int, int> visited[1500][1500]) {
	int xx = mod(x, n), yy = mod(y, m);
	if (visited[xx][yy] != make_pair(NONE, NONE)) {
		return true;
	}
	visited[xx][yy] = make_pair(x, y);
	int cx, cy;
	cx = mod(x-1, n);
	cy = mod(y, m);
	if (v[cx][cy] != '#' && visited[cx][cy] != make_pair(x-1, y)) {
		bool b = dfs(x-1, y, v, visited);
		if (b) return true;
	}
	cx = mod(x, n);
	cy = mod(y-1, m);
	if (v[cx][cy] != '#' && visited[cx][cy] != make_pair(x, y-1)) {
		bool b = dfs(x, y-1, v, visited);
		if (b) return true;
	}
	cx = mod(x, n);
	cy = mod(y+1, m);
	if (v[cx][cy] != '#' && visited[cx][cy] != make_pair(x, y+1)) {
		bool b = dfs(x, y+1, v, visited);
		if (b) return true;
	}
	cx = mod(x+1, n);
	cy = mod(y, m);
	if (v[cx][cy] != '#' && visited[cx][cy] != make_pair(x+1, y)) {
		bool b = dfs(x+1, y, v, visited);
		if (b) return true;
	}
	return false;
}

char v[1500][1501];
pair <int, int> visited[1500][1500];

int main() {
	scanf("%d%d", &n, &m);
	for (int i = 0; i < n; i++) {
		scanf("%s", v[i]);
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (v[i][j] == 'S') {
				sx = i;
				sy = j;
				break;
			}
		}
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			visited[i][j] = make_pair(NONE, NONE);
		}
	}
	bool ans = dfs(sx, sy, v, visited);
	printf("%s", ans ? "Yes" : "No");
	return 0;
}