#include <iostream>
#include <vector>
#include <list>
#include <utility>

using namespace std;

#define P 100000

vector <list<pair<int, int>>> hashtable(P);

int * get(int a, vector <list<pair<int, int>>>& hashtable)
{
	int index = a % P;
	for(auto& p : hashtable[index])
		if(p.first == a)
			return &(p.second);
	return nullptr;
}

void set(pair <int, int> p, vector <list<pair<int, int>>>& hashtable)
{
	int a = p.first;
	int * b = get(a, hashtable);
	if(b != nullptr)
		*b = p.second;
	else
	{
		int index = a % P;
		hashtable[index].push_back(make_pair(a, p.second));
	}
}

void remove(int a, vector <list<pair<int, int>>>& hashtable)
{
	int index = a % P;
	for(auto i = hashtable[index].begin(); i != hashtable[index].end(); ++i)
		if(i->first == a)
		{
			hashtable[index].erase(i);
			return;
		}
}

int main()
{
	int n;
	cin >> n;
	vector <pair<int, int>> v(n);
	for(int i = 0; i < n; i++)
	{
		int a, b;
		cin >> a >> b;
		v[i] = make_pair(a, b);
		set(v[i], hashtable);
	}
	for(int i = 0; i < n; i++)
	{
		cout << v[i].first << ' ' << *(get(v[i].first, hashtable)) << endl;
	}
	for(int i = 0; i < 2; i++)
	{
		remove(v[i].first, hashtable);
	}
	for(int i = 0; i < n; i++)
	{
		if(get(v[i].first, hashtable) != nullptr)
			cout << v[i].first << ' ' << *(get(v[i].first, hashtable)) << endl;
	}
	return 0;
}