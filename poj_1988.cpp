#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

int n = 30001;
vector <int> parent(n, 0);
vector <int> size(n, 1);
vector <int> update(n, 0);

int find(int x)
{
	int p = x;
	int tot_update = 0;
	while(parent[p] != 0)
	{
		tot_update += update[p];
		p = parent[p];
	}
	int t = x;
	while(t != p)
	{
		int next = parent[t];
		tot_update -= update[t];
		update[t] += tot_update;
		parent[t] = p;
		t = next;
	}
	return p;
}

int get_size(int x)
{
	int p = find(x);
	if(p == x)
		return 0;
	else
		return (size[p] - (size[x] + update[x]));
}

void place(int x, int y)
{
	y = find(y);
	x = find(x);
	update[y] = update[y] + size[x] + update[x];
	update[x] = update[x] - update[y];
	parent[x] = y;
}

int main()
{
	int p;
	cin >> p;
	while(p--)
	{
		char op;
		cin >> op;
		if(op == 'M')
		{
			int x, y;
			cin >> x >> y;
			place(x, y);
		}
		else
		{
			int x;
			cin >> x;
			cout << get_size(x) << '\n';
		}
	}
	return 0;
}