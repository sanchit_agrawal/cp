#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

long long NUM = 1000000007;

int main()
{
	string s;
	cin >> s;
	int n = s.size();
	long long g = 0;
	long long a = 1;
	long long sum = 0;
	for(int i = 0; i < n; i++)
	{
		a = ((g+1)*('Z'-s[i])) % NUM;
		g = (g*26 + 'Z'-s[i]) % NUM;
		sum = (sum + a) % NUM;
	}
	cout << sum << '\n';
	return 0;
}