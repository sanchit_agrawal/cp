#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int hundreds;
	for(int i = 0; i < n; i++)
	{
		int curr;
		cin >> curr;
		if(curr == 100)
			hundreds++;
	}
	if(hundreds % 2)
		cout << "NO" << '\n';
	else if(hundreds == 0 && n % 2 == 1)
		cout << "NO" << '\n';
	else
		cout << "YES" << '\n';
	return 0;
}