#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int l = 1, r = 0;
	int count = 0;
	int curr = 0;
	while(r <= n)
	{	
		if(curr < n)
		{
			curr += ++r;
		}
		else if(curr == n)
		{
			count++;
			curr -= l++;
			curr += ++r;
		}
		else
		{
			curr -= l++;
		}
	}
	cout << count << endl;
	return 0;
}