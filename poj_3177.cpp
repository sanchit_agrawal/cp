#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <set>

using namespace std;

int t = 0;

void dfs(int curr, int parent, vector <int>& d, vector <int>& low, vector <vector<int> >& adjacency_list)
{
	low[curr] = d[curr];
	bool parent_done = false;
	for(int i = 0; i < adjacency_list[curr].size(); i++)
	{
		int next = adjacency_list[curr][i];
		if(d[next] == -1)
		{
			d[next] = ++t;
			dfs(next, curr, d, low, adjacency_list);
			low[curr] = min(low[curr], low[next]);
		}
		else
		{
			if(next != parent)
				low[curr] = min(low[curr], d[next]);
			else
			{
				if(!parent_done)
					parent_done = true;
				else
				{
					low[curr] = min(low[curr], d[next]);
				}
			}
		}
	}
}

set <int> is_not_leaf;
int max_comp_num = 0;

void dfs2(int curr, int curr_comp, vector <bool>& visited, vector <int>& low, vector <int>& d, vector <vector<int> >& adjacency_list)
{
	if(curr_comp > max_comp_num)
		max_comp_num = curr_comp;
	for(int i = 0; i < adjacency_list[curr].size(); i++)
	{
		int next = adjacency_list[curr][i];
		if(!visited[next])
		{
			visited[next] = true;
			int next_comp_num = (low[next] > d[curr] ? curr_comp+1 : curr_comp);
			if(next_comp_num != curr_comp)
				is_not_leaf.insert(curr_comp);
			dfs2(next, next_comp_num, visited, low, d, adjacency_list);
		}
	}
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector <vector<int> > adjacency_list(n);
	for(int i = 0; i < m; i++)
	{
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		adjacency_list[u].push_back(v);
		adjacency_list[v].push_back(u);
	}
	vector <int> d(n, -1);
	vector <int> low(n, -1);
	d[0] = 0;
	dfs(0, -1, d, low, adjacency_list);
	vector <bool> visited(n, false);
	visited[0] = true;
	dfs2(0, 0, visited, low, d, adjacency_list);
	cout << (max_comp_num+1-is_not_leaf.size()+1)/2 << '\n';
	return 0;
}