#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	vector <int> color(n);
	map <int, vector <int>> col_to_node;
	for (int i = 0; i < n; i++) {
		cin >> color[i];
		col_to_node[color[i]].push_back(i);
	}
	vector <vector <int>> adjacency_list(n);
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		adjacency_list[u].push_back(v);
		adjacency_list[v].push_back(u);
	}
	map <int, set <int>> reachable_colors;
	for (auto& p : col_to_node) {
		int col = p.first;
		for (int i : p.second) {
			for (int j : adjacency_list[i]) {
				if (color[j] != col)
					reachable_colors[col].insert(color[j]);
			}
		}
	}
	int best_col = (col_to_node.begin())->first;
	int best_size = 0;
	for (auto& p : reachable_colors) {
		if (best_col == -1 || p.second.size() > best_size) {
			best_size = p.second.size();
			best_col = p.first;
		}
	}
	cout << best_col;
	return 0;
}