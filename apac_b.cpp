#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
	int t;
	cin >> t;
	for(int test = 1; test <= t; test++)
	{
		int r, c;
		cin >> r >> c;
		vector<vector<int>> grid(r, vector<int>(c));
		for(int i = 0; i < r; i++)
			for(int j = 0; j < c; j++)
				cin >> grid[i][j];
		vector<vector<int>> dp(r, vector<int>(c));
		for(int i = c-1; i >= 0; i--)
			dp[r-1][i] = grid[r-1][i];
		for(int i = r-1; i >= 0; i--)
			dp[i][c-1] = grid[i][c-1];
		for(int i = r-2; i >= 0; i--)
			for(int j = c-2; j >= 0; j--)
				dp[i][j] = max(grid[i][j], min(dp[i+1][j], dp[i][j+1]));
		vector<vector<int>> fh(r, vector<int>(c));
		for(int i = 0; i < r; i++)
			fh[i][0] = grid[i][0];
		for(int i = 0; i < c; i++)
			fh[0][i] = grid[0][i];
		for(int i = 1; i < r; i++)
			for(int j = 1; j < c; j++)
				fh[i][j] = max(min(min(fh[i-1][j], fh[i][j-1]), dp[i][j]), grid[i][j]);
		long long total = 0;
		for(int i = 0; i < r; i++)
			for(int j = 0; j < c; j++)
				total += fh[i][j] - grid[i][j];
		cout << "Case #" << test << ": " << total << '\n';
	}
	return 0;
}