#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector <int> v(n);
		for(int i = 0; i < n; i++)
			cin >> v[i];
		int d1 = 1000000001, d2 = 1000000001;
		int d1_count = 0, d2_count = 0;
		int first_d1_index = -1;
		int misindex = -1;
		bool possible = true;
		for(int i = 1; i < n; i++)
		{
			int d = v[i]-v[i-1]; 
			if(d == d1)
				d1_count++;
			else if(d == d2)
			{
				d2_count++;
				if(d2_count > d1_count)
				{
					int temp = d2;
					d2 = d1;
					d1 = temp;
					temp = misindex;
					misindex = first_d1_index;
					first_d1_index = temp;
				}
				if(d2_count > 1)
				{
					possible = false;
					break;
				}
			}
			else if(d1 == 1000000001)
			{
				first_d1_index = i;
				d1 = d;
			}
			else if(d2 = 1000000001)
			{
				misindex = i;
				d2 = d;
			}
			else
			{
				if(i != misindex + 1)
				{
					possible = false;
					break;
				}
			}
		}
		if(!possible)
		{
			cout << -1 << endl;
			continue;
		}
		if(misindex == n-1)
		{
			cout << v[n-1] << endl;
			continue;
		}
		if(misindex == -1)
		{
			cout << min(v[0], v[n-1]) << endl;
			continue;
		}
		if(v[misindex+1]-v[misindex-1] != d1)
		{
			cout << -1 << endl;
		}
		else
		{
			cout << v[misindex] << endl;
		}
	}
	return 0;
}