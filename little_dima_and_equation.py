def dig_sum(x):
	if x == 0:
		return 0
	else:
		return (x%10) + dig_sum(x/10)
a, b, c = map(int, raw_input().split())
ls = []
for s in range(1, 82):
	x = b*(s**a)+c
	if x > 0 and x < 10**9 and dig_sum(x) == s:
		ls.append(x)
print len(ls)
for l in ls:
	print l,