#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

long long dfs(int root, vector <vector <int>>& adjacency_list, vector <long long>& sizes) {
	if (sizes[root] != -1) {
		return sizes[root];
	}
	long long ans = 1;
	for (int child : adjacency_list[root]) {
		ans += dfs(child, adjacency_list, sizes);
	}
	return sizes[root] = ans;
}

int main() {
	string s;
	cin >> s;
	int n = s.size();
	vector <int> f(n+1);
	f[0] = f[1] = 0;
	for (int i = 2; i <= n; i++) {
		int j = f[i-1];
		while (true) {
			if (s[j] == s[i-1]) {
				f[i] = j + 1;
				break;
			} else {
				j = f[j];
			}
			if (j == 0) {
				f[i] = 0;
				break;
			}
		}
	}
	vector <int> prefix_idx;
	vector <int> times;
	for (int i = n; i > 0; i = f[i]) {
		prefix_idx.push_back(i);
	}
	vector <vector <int>> adjacency_list(n+1);
	for (int i = 0; i <= n; i++) {
		adjacency_list[f[i]].push_back(i);
	}
	vector <long long> sizes(n+1, -1);
	for (int i = 1; i <= n; i++) {
		sizes[i] = dfs(i, adjacency_list, sizes);
	}
	cout << prefix_idx.size() << '\n';
	for (int i = prefix_idx.size()-1; i >= 0; i--) {
		cout << prefix_idx[i] << ' ' << sizes[prefix_idx[i]] << '\n';
	}
	return 0;
}