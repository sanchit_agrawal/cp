#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <queue>

using namespace std;

int n;

pair<int,int> bfs(int root, vector <vector<int>>& alist, vector <bool>& visited, pair<int,int>& e) {
	queue <int> q;
	visited[root] = true;
	for (int next : alist[root]) {
		auto a = make_pair(root, next);
		auto b = make_pair(next, root);
		if (e != a && e != b) {
			q.push(next);
			visited[next] = true;
		}
	}
	if (q.size() == 0) {
		return make_pair(-1, 0);
	}
	int dist = 0;
	int rem = q.front();
	while (q.size() > 0) {
		dist++;
		int size = q.size();
		for (int i = 0; i < size; i++) {
			int top = q.front();
			q.pop();
			for (int next : alist[top]) {
				if (visited[next]) {
					continue;
				}
				auto a = make_pair(top, next);
				auto b = make_pair(next, top);
				if (e != a && e != b) {
					q.push(next);
					visited[next] = true;
					rem = next;
				}
			}
		}
	}
	return make_pair(rem, dist);
}

int minlen(int root, vector <vector<int>>& alist, vector <bool>& visited, pair<int,int>& e) {
	vector <bool> dummy_visit(n, false);
	auto p = bfs(root, alist, dummy_visit, e);
	if (p.first == -1) {
		return 0;
	}
	auto q = bfs(p.first, alist, visited, e);
	return q.second;	
}

int main() {
	cin >> n;
	vector <vector<int>> alist(n);
	vector <pair<int,int>> edges(n-1);
	for (int i = 0; i < n-1; i++) {
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		alist[u].push_back(v);
		alist[v].push_back(u);
		edges[i].first = u;
		edges[i].second = v;
	}
	int ans = 0;
	for (auto e : edges) {
		vector <bool> visited(n, false);
		int max_0_len = -1;
		int max_1_len = -1;
		for (int i = 0; i < n; i++) {
			if (!visited[i]) {
				if (max_0_len == -1) {
					max_0_len = minlen(i, alist, visited, e);
				} else {
					max_1_len = minlen(i, alist, visited, e);
				}
			}
		}
		ans = max(ans, max_0_len * max_1_len);
	}
	cout << ans;
	return 0;
}