n,m = map(int, raw_input().split())
if n > m + 1:
	print -1
elif n == m + 1:
	print '01'*m+'0'
else:
	delta = m-n
	if n < delta-2:
		print -1
	elif n == delta-1:
		print '110'*(delta-1)+'1'
	elif n == delta-2:
		print '110'*(delta-2)+'11'
	else:
		print '110'*delta+'10'*(n-delta)