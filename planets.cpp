#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;

int get_delay(int time, vector <int>& blocked) {
	int i = 0;
	while (i < blocked.size() && blocked[i] < time) {
		i++;
	}
	int delay = 0;
	while (i < blocked.size() && blocked[i] == time + delay) {
		delay++;
		i++;
	}
	return delay;
}

int main() {
	int n, m;
	cin >> n >> m;
	vector <vector <pair<int, int>>> adjacency_list(n);
	for (int i = 0; i < m; i++) {
		int u, v, t;
		cin >> u >> v >> t;
		adjacency_list[u-1].push_back(make_pair(-t, v-1));
		adjacency_list[v-1].push_back(make_pair(-t, u-1));
	}
	vector <vector <int>> blocked(n);
	for (int i = 0; i < n; i++) {
		int k;
		cin >> k;
		for (int j = 0; j < k; j++) {
			int t;
			cin >> t;
			blocked[i].push_back(t);
		}
	}
	priority_queue <pair <int, int>> pq;
	int delay = get_delay(0, blocked[0]);
	for (auto p : adjacency_list[0]) {
		auto q = make_pair(p.first - delay, p.second);
		pq.push(q);
	}
	vector <int> dist(n, (int) 2e9);
	dist[0] = 0;
	while (pq.size() > 0) {
		auto top = pq.top();
		pq.pop();
		int t = -top.first, u = top.second;
		if (t < dist[u]) {
			dist[u] = t;
			int delay = get_delay(t, blocked[u]);
			for (auto p : adjacency_list[u]) {
				int edge = -p.first;
				int v = p.second;
				if (t + edge + delay < dist[v]) {
					pq.push(make_pair(-t-edge-delay, v));
				}
			}
		}
	}
	if (dist[n-1] < 2e9) {
		cout << dist[n-1];
	} else {
		cout << -1;
	}
	return 0;
}