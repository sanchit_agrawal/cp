#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <long long> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	long long last_ans = 0;
	int m;
	cin >> m;
	for(int i = 0; i < m; i++)
	{
		int w, h;
		cin >> w >> h;
		cout << max(last_ans, v[w-1]) << '\n';
		last_ans = max(last_ans, v[w-1])+h;
	}
	return 0;
}