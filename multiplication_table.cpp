#include <iostream>
#include <algorithm>

using namespace std;

int main() {
	long long n, m, k;
	cin >> n >> m >> k;
	long long l = 1;
	long long r = m*n;
	while (l < r) {
		long long mid = (l + r + 1) / 2;
		long long count = 0;
		for (int i = 1; i <= n; i++) {
			count += min(m, (mid - 1) / i);
		}
		if (count < k) {
			l = mid;
		} else {
			r = mid - 1;
		}
	}
	cout << l;
	return 0;
}