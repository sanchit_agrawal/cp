#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <bool> indexed(3001, false);
	for(int i = 0; i < n; i++)
	{
		int curr;
		cin >> curr;
		indexed[curr] = true;
	}
	int i;
	for(i = 1; i <= 3000; i++)
		if(!indexed[i])
			break;
	cout << i << '\n';
	return 0;
}