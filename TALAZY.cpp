#include <iostream>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		long long n, b, m;
		cin >> n >> b >> m;
		long long total = 0;
		while(n > 0)
		{
			int next = (n+1)/2;
			total += next * m;
			n -= next;
			m = m * 2;
			total += b;
		}
		total -= b;
		cout << total << endl;
	}
	return 0;
}