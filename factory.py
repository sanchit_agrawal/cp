a, m = map(int, raw_input().split())
occurred = [False]*m
while True:
	if occurred[a%m]:
		print 'No'
		break
	elif a%m == 0:
		print 'Yes'
		break
	else:
		occurred[a%m] = True
		a = (a + a%m)%m
