#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <utility>
#include <cmath>
#include <iomanip>

using namespace std;

int prime_count = 0;

void bfs(int root, vector <bool>& visited, vector <vector<int>>& adjacency_list, vector<bool>& is_prime)
{
	queue <int> q;
	q.push(root);
	visited[root] = true;
	int level = 1;
	while(q.size() != 0)
	{
		int k = q.size();
		for(int i = 0; i < k; i++)
		{
			int top = q.front();
			q.pop();
			for(int child: adjacency_list[top])
				if(!visited[child])
				{
					visited[child] = true;
					if(is_prime[level])
						prime_count++; 
					q.push(child);
				}
		}
		level++;
	}
}

int main()
{
	int n;
	cin >> n;
	vector <vector<int>> adjacency_list(n);
	for(int i = 0; i < n-1; i++)
	{
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		adjacency_list[u].push_back(v);
		adjacency_list[v].push_back(u);		
	}
	vector <bool> is_prime(n, true);
	is_prime[0] = is_prime[1] = false;
	for(int i = 2; i < n; i++)
		if(is_prime[i])
		{
			for(int j = 2*i; j < n; j += i)
				is_prime[j] = false;
		}
	bfs(0, visited, ad)
	for(int i = 0; i < n; i++)
	{
		vector <bool> visited(n, false);
		bfs(i, visited, adjacency_list, is_prime);
	}
	cout << (double) prime_count / (n * (n-1)) << endl;
	return 0;
}