#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

int main()
{
	int n;
	cin >> n;
	string s;
	cin >> s;
	vector <int> ans0(n, 0);
	vector <int> ans1(n, 0);
	for(int k = 2; k <= n; k++)
	{
		vector <int> new_ans(n, 0);
		for(int i = 0; i <= n-k; i++)
		{
			int j = i+k-1;
			if(s[i] == s[j])
				new_ans[i] = ans0[i+1];
			else
				new_ans[i] = min(ans1[i+1], ans1[i]) + 1;
		}
		ans0 = ans1;
		ans1 = new_ans;
	}
	cout << ans1[0] << '\n';
	return 0;
}