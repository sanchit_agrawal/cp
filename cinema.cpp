#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	ifstream fin("input.txt");
	ofstream fout("output.txt");
	int m, k;
	fin >> m >> k;
	vector <bool> is_fav(m+1, false);
	for (int i = 0; i < k; i++) {
		int id;
		fin >> id;
		is_fav[id] = true;
	}
	int n;
	fin >> n;
	vector <int> n_not_fav(n, 0);
	vector <int> n_unknown(n, 0);
	vector <int> n_fav(n, 0);
	vector <int> min_fav(n);
	vector <int> max_fav(n);
	for (int i = 0; i < n; i++) {
		string s;
		fin >> s;
		int l;
		fin >> l;
		for (int j = 0; j < l; j++) {
			int id;
			fin >> id;
			if (id == 0) {
				n_unknown[i]++;
			} else if (is_fav[id]) {
				n_fav[i]++;
			} else {
				n_not_fav[i]++;
			}
		}
		min_fav[i] = n_fav[i] + max(0, n_unknown[i] - ((m - k) - n_not_fav[i]));
		max_fav[i] = n_fav[i] + min(n_unknown[i], k - n_fav[i]);
	}
	vector <int> mf_left(n, 0);
	vector <int> mf_right(n, 0);
	vector <int> Mf_left(n, 0);
	vector <int> Mf_right(n, 0);
	for (int i = 1; i < n; i++) {
		mf_left[i] = max(mf_left[i-1], min_fav[i-1]);
		Mf_left[i] = max(Mf_left[i-1], max_fav[i-1]);
	}
	for (int i = n-2; i >= 0; i--) {
		mf_right[i] = max(mf_right[i+1], min_fav[i+1]);
		Mf_right[i] = max(Mf_right[i+1], max_fav[i+1]);
	}
	for (int i = 0; i < n; i++) {
		if (min_fav[i] >= max(Mf_left[i], Mf_right[i])) {
			fout << 0 << '\n';
		} else if (max_fav[i] < max(mf_left[i], mf_right[i])) {
			fout << 1 << '\n';
		} else {
			fout << 2 << '\n';
		}
	}
	return 0;
}