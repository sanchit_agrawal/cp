#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

long long gcd(long long a, long long b)
{
	if(b == 0)
		return a;
	return gcd(b, a % b);
}

int main()
{
	int n;
	cin >> n;
	if(n == 1)
		cout << 1;
	else if(n == 2)
		cout << 2;
	else
	{
		long long ans = n * (n-1);
		long long out = -1;
		for(int i = 1; i < n-1; i++)
		{
			long long temp = (ans * i) / gcd(i, ans);
			if(temp > out)
			{
				// cout << "i: " << i << endl;
				out = temp;
			}
		}
		cout << out;
	}
	return 0;
}