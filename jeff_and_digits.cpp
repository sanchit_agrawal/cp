// must end with 0
// 5n % 9 = 0

#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int five;
	for(int i = 0; i < n; i++)
	{
		int curr;
		cin >> curr;
		if(curr == 5)
			five++;
	}
	if(n-five == 0)
		cout << -1;
	else
	{
		int new_five = five - five % 9;
		for(int i = 0; i < new_five; i++)
			cout << '5';
		if(new_five == 0)
			cout << '0';
		else
			for(int i = 0; i < n-five; i++)
				cout << '0';
	}
	return 0;
}