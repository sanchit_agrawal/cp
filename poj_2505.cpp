#include <iostream>
#include <map>
using namespace std;

long long n;
// long long ceil(double d)
// {
// 	if((long long) d == d)
// 		return (long long) d;
// 	else
// 		return (long long) (d + 1);
// }

bool compute_state(long long a, map <long long, bool>& winning_state)
{
	if(a >= n)
		return false;
	map <long long, bool>::iterator f = winning_state.find(a);
	if(f != winning_state.end())
		return f->second;
	bool is_winning = false;
	for(int i = 2; i <= 9; i++)
	{
		is_winning = is_winning || !compute_state(i*a, winning_state);
		if(is_winning)
			break;
	}
	return winning_state[a] = is_winning;
}

int main()
{
	while(cin >> n)
	{
		map<long long, bool> winning_state;
		bool ans = compute_state(1, winning_state);
		if(ans)
			cout << "Stan wins." << '\n';
		else
			cout << "Ollie wins." << '\n';
	}
	return 0;
}