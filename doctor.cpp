#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <deque>

using namespace std;

int main() {
	int n;
	long long k;
	cin >> n >> k;
	map <int, vector <int>> m;
	vector <int> visits(n);
	for (int i = 0; i < n; i++) {
		cin >> visits[i];
		m[visits[i]].push_back(i);
	}
	vector <int> lengths(m.size());
	vector <int> arity(m.size());
	int i = 0;
	for (auto& p: m) {
		lengths[i] = p.second.size();
		arity[i] = p.first;
		i++;
	}
	for (int i = m.size()-2; i >= 0; i--) {
		lengths[i] = lengths[i] + lengths[i+1];
	}
	long long sub = 0;
	int ind = 0;
	while (ind < m.size() && (long long) (arity[ind] - sub) * lengths[ind] <= k) {
		k -= (long long) (arity[ind] - sub) * lengths[ind];
		sub = arity[ind];
		ind++;
	}
	if (ind != m.size()) {
		long long full_rounds = k/lengths[ind];
		sub += full_rounds;
		k -= full_rounds * lengths[ind]; 
		deque <int> d;
		for (int i = 0; i < n; i++) {
			if (visits[i] - sub > 0) {
				d.push_back(i);
			}
		}
		for (int i = 0; i < k; i++) {
			int front = d.front();
			d.pop_front();
			if (visits[front] - sub - 1 > 0) {
				d.push_back(front);
			}
		}
		for (int i : d) {
			cout << i+1 << ' ';
		}
	} else if (k != 0) {
		cout << -1;
	}
	return 0;
}