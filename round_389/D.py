k, n = map(int, raw_input().split())
beauties = {}
for i in range(k):
	s, b = raw_input().split()
	b = int(b)
	if s not in beauties:
		beauties[s] = [b]
	else:
		beauties[s].append(b)
for s, lb in beauties.items():
	lb.sort()
doubles = []
singles = []
score = 0
for s, lb in beauties.items():
	s_rev = s[::-1]
	if s == s_rev:
		while len(lb) >= 2 and lb[-1] > 0 and lb[-2] > 0:
			score += lb[-1] + lb[-2]
			lb.pop()
			lb.pop()
		if len(lb) > 0 and lb[-1] > 0:
			if len(lb) > 1 and lb[-1] + lb[-2] > 0:
				doubles.append((lb[-1], lb[-2]))
			else:
				singles.append(lb[-1])
	elif s_rev not in beauties:
		continue
	else:
		lb_rev = beauties[s_rev]
		while len(lb) > 0 and len(lb_rev) > 0 and lb_rev[-1] + lb[-1] > 0:
			score += lb_rev[-1] + lb[-1]
			lb_rev.pop()
			lb.pop()

singles.sort()
doubles.sort()
# Try 1: Taking largest double
tscore1 = 0
if len(doubles) > 0:
	tscore1 += doubles[-1][0]
for a, b in doubles[:-1]:
	tscore1 += a + b
# Try 2: Taking largest single
tscore2 = 0
if len(singles) > 0:
	tscore2 += singles[-1]
for a, b in doubles:
	tscore2 += a + b
print score + max(tscore1, tscore2)