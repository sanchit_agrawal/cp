#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	string s;
	string t;
	cin >> s >> t;
	int n = s.size();
	vector <int> replace(26, -1);
	for(int i = 0; i < n; i++)
	{
		if(s[i] == t[i])
		{
			if(replace[s[i]-'a'] == -1)
				replace[s[i]-'a'] = s[i]-'a';
			else if(replace[s[i]-'a'] == s[i]-'a')
				continue;
			else
			{
				cout << -1;
				return 0;
			}
		}
		else
		{
			if(replace[s[i]-'a'] == -1)
				replace[s[i]-'a'] = t[i]-'a';
			else if(replace[s[i]-'a'] == t[i]-'a')
				continue;
			else
			{
				cout << -1;
				return 0;
			}

			if(replace[t[i]-'a'] == -1)
				replace[t[i]-'a'] = s[i]-'a';
			else if(replace[t[i]-'a'] == s[i]-'a')
				continue;
			else
			{
				cout << -1;
				return 0;
			}
		}
	}
	string out;
	for(int i = 0; i < 26; i++)
	{
		if(replace[i] == -1)
			continue;
		if(replace[i] == i)
			continue;
		out += (replace[i] + 'a');
		out += (i + 'a');
		replace[replace[i]] = -1;
	}
	cout << out.length()/2 << '\n';
	for(int i = 0; i < out.length()/2; i++)
		cout << out[2*i] << ' ' << out[2*i+1] << '\n';
	return 0;
}