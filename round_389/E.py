import heapq

n, k = map(int, raw_input().split())
ls = map(int, raw_input().split())
if sum(ls) < k:
	print -1
else:
	ls.sort()
	ls_min = ls[:]
	min_q = heapq.heapify(ls_min)
	while len(ls_min) > k:
		heapq.heappop(ls_min)
	min_element = ls_min[0]
	ls = map(lambda x : -x, ls)
	heapq.heapify(ls)
	while len(ls) < k:
		top = -heapq.heappop(ls)
		heapq.heappush(ls, -top/2)
		heapq.heappush(ls, -(top+1)/2)
		heapq.heappush(ls_min, top/2)
		heapq.heappush(ls_min, (top+1)/2)
	if len(ls_min) > k:
		heapq.heappop(ls_min)
	min_element = ls_min[0]
	while -ls[-1]/2 > min_element:
		top = -heapq.heappop(ls)
		heapq.heappush(ls, -top/2)
		heapq.heappush(ls, -(top+1)/2)
		heapq.heappush(ls_min, top/2)
		heapq.heappush(ls_min, (top+1)/2)
		heapq.heappop(ls_min)
		heapq.heappop(ls_min)
		min_element = ls_min[0]
	print min_element
		