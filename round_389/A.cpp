#include <iostream>

using namespace std;

int main()
{
	int n, m, k;
	cin >> n >> m >> k;
	cout << (k-1)/(2*m)+1 << ' ';
	cout << ((k-1) % (2*m))/2 + 1 << ' ';
	cout << (k % 2 ? 'L' : 'R');
	return 0;
}