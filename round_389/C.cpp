#include <iostream>
#include <string>

using namespace std;

int main()
{
	int n;
	cin >> n;
	string s;
	cin >> s;
	int is_going_down = -1;
	int is_going_right = -1;
	int seq_len = 1;
	for(char c : s)
	{
		if(c == 'R')
		{
			if(is_going_right == -1)
				is_going_right = 1;
			else if(is_going_right == 0)
			{
				seq_len++;
				is_going_down = -1;
				is_going_right = 1;
			}
		}
		else if(c == 'L')
		{
			if(is_going_right == -1)
				is_going_right = 0;
			else if(is_going_right == 1)
			{
				seq_len++;
				is_going_down = -1;
				is_going_right = 0;
			}
		}
		else if(c == 'U')
		{
			if(is_going_down == -1)
				is_going_down = 0;
			else if(is_going_down == 1)
			{
				seq_len++;
				is_going_right = -1;
				is_going_down = 0;
			}
		}
		else
		{
			if(is_going_down == -1)
				is_going_down = 1;
			else if(is_going_down == 0)
			{
				seq_len++;
				is_going_right = -1;
				is_going_down = 1;
			}
		}
	}
	cout << seq_len;
	return 0;
}