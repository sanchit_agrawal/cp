k = int(raw_input())
s = raw_input()
n = len(s)
l, r = -1, 0
ans = 0
curr = 0
# [l, r)
for i in range(n):
	l = max(i-1, l)
	while l < n and curr < k:
		l += 1
		if l != n:
			curr += int(s[l])
	r = max(l+1, r)
	while r < n and s[r] != '1':
		r += 1
	ans += (r-l)
print ans