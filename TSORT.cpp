#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

int v[1000001];

int main()
{
	int t;
	scanf("%d", &t);
	int temp;
	for(int i = 0; i < t; i++)
	{
		scanf("%d", &temp);
		v[temp]++;
	}
	for(int i = 0; i < 1000001; i++)
	{
		for(int j = 0; j < v[i]; j++)
			printf("%d\n", i);
	}
	return 0;
}