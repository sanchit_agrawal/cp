#include <iostream>
using namespace std;

#define abs(x) ((x) < 0 ? -(x):(x))

int main()
{
	int x_pos, y_pos;
	for(int i = 0; i < 5; i++)
		for(int j = 0; j < 5; j++)
		{
			int curr;
			cin >> curr;
			if(curr == 1)
			{
				x_pos = i+1;
				y_pos = j+1;
			}
		}
	cout << abs(3-x_pos)+abs(3-y_pos) << endl;
	return 0;
}