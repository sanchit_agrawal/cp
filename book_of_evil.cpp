#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;

int n, m, d;

int bfs1(int root, vector <bool>& isp, vector <vector <int>>& alist) {
	vector <bool> visited(n, false);
	visited[root] = true;
	queue <int> q;
	q.push(root);
	int lastp = root;
	int pdist = 0;
	int d = 0;
	while (q.size()) {
		int size = q.size();
		d++;
		for (int i = 0; i < size; i++) {
			int top = q.front();
			q.pop();
			for (int next : alist[top]) {
				if (!visited[next]) {
					visited[next] = true;
					q.push(next);
					if (isp[next]) {
						lastp = next;
						pdist = d;
					}
				}
			}
		}
	}
	return lastp;
}

vector <bool> bfs2(int root, vector <vector <int>>& alist) {
	vector <bool> visited(n, false);
	queue <int> q;
	q.push(root);
	visited[root] = true;
	for (int i = 0; i < d; i++) {
		int size = q.size();
		for (int j = 0; j < size; j++) {
			int top = q.front();
			q.pop();
			for (int next : alist[top]) {
				if (!visited[next]) {
					q.push(next);
					visited[next] = true;
				}
			}
		}
	}
	return visited;
}

int main() {
	cin >> n >> m >> d;
	vector <int> p(m);
	for (int i = 0; i < m; i++) {
		cin >> p[i];
		p[i]--;
	}
	vector <bool> isp(n, false);
	for (int i : p) {
		isp[i] = true;
	}
	vector <vector <int>> alist(n);
	for (int i = 0; i < n-1; i++) {
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		alist[u].push_back(v);
		alist[v].push_back(u);
	}
	int a = bfs1(p[0], isp, alist);
	int b = bfs1(a, isp, alist);
	auto s = bfs2(a, alist);
	auto t = bfs2(b, alist);
	int ans = 0;
	for (int i = 0; i < n; i++) {
		ans += (s[i] && t[i]);
	}
	cout << ans;
	return 0;
}