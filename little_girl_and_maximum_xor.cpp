#include <iostream>
#include <algorithm>

using namespace std;

int main() {
	long long l, r;
	cin >> l >> r;
	bool lbits[64] = {0}, rbits[64] = {0};
	for (int i = 0; i < 64; i++) {
		lbits[i] = l % 2;
		rbits[i] = r % 2;
		l /= 2;
		r /= 2;
	}
	int i = 63;
	while (i >= 0 && lbits[i] == rbits[i]) {
		i--;
	}
	cout << ((long long)1 << (i+1)) - 1;
	return 0;
}