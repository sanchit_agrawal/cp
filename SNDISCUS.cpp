#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

int dist(int x, int y, int a, int b, int c, int d) {
	int x_dist, y_dist;
	if (x >= min(a, c) && x <= max(a, c)) {
		x_dist = 0;
	} else {
		x_dist = min(abs(x-a), abs(x-c));
	}
	if (y >= min(b, d) && y <= max(b, d)) {
		y_dist = 0;
	} else {
		y_dist = min(abs(y-b), abs(y-d));
	}
	return x_dist + y_dist;
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		vector <int> startx(n);
		vector <int> starty(n);
		vector <int> endx(n);
		vector <int> endy(n);
		for (int i = 0; i < n; i++) {
			cin >> startx[i] >> starty[i] >> endx[i] >> endy[i];
		}
		int min_dist = 100;
		for (int x = 1; x <= 50; x++) {
			for (int y = 1; y <= 50; y++) {
				int max_dist = 0;
				for (int i = 0; i < n; i++) {
					max_dist = max(max_dist, dist(x, y, startx[i], starty[i], endx[i], endy[i]));
				}
				min_dist = min(min_dist, max_dist);
			}
		}
		cout << min_dist << '\n';
	}
	return 0;
}