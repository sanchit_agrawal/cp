import random
import math

def quickselect(ls, l, r, k):
	if l == r:
		return l
	elif l == r-1:
		if k == l:
			return min(range(l, l+2), key = lambda x: ls[x])
		else:
			return max(range(l, l+2), key = lambda x: ls[x])
	else:
		pivot = random.randint(l, r)
		ls[r], ls[pivot] = ls[pivot], ls[r]
		left, right = l, r-1
		while left < right:
			if ls[left] < ls[r]:
				left += 1
			elif ls[right] >= ls[r]:
				right -= 1
			else:
				ls[left], ls[right] = ls[right], ls[left]
		if ls[left] >= ls[r]:
			pivot = left
		else:
			pivot = left+1
		ls[r], ls[pivot] = ls[pivot], ls[r]
		if pivot == k:
			return pivot
		elif pivot < k:
			return quickselect(ls, pivot+1, r, k)
		else:
			return quickselect(ls, l, pivot-1, k)

n = int(raw_input())
ls = []
for i in range(n):
	x, y = map(float, raw_input().split())
	ls.append((x,y))
ind = min(range(n), key = lambda x: ls[x][1])
rem = ls[:ind]+ls[ind+1:]
x0, y0 = ls[ind][0], ls[ind][1]
args = map(lambda x: math.atan2(x[1]-y0,x[0]-x0), rem)
median = quickselect(args, 0, n-2, (n-2)/2)
print (x0, y0), rem[median]