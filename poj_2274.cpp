#include <cstdio>
#include <vector>
#include <map>
#include <utility>
#include <set>

using namespace std;

int main()
{
	int n;
	scanf("%d", &n);
	// velocity, i, position
	set <pair<int, pair<int, int> > > m;
	// time, i, j
	set <pair<double, pair<int, int> > > s;
	int count = 0;
	for(int i = 0; i < n; i++)
	{
		int start, velocity;
		scanf("%d%d", &start, &velocity);
		if(m.size() != 0)
		{
			set <pair<int, pair<int, int> > >::reverse_iterator iter = m.rbegin();
			while(iter != m.rend() && iter->first > velocity)
			{
				int j = (iter->second).first;
				int old_vel = iter->first;
				int old_pos = (iter->second).second;
				double time = double(start-old_pos)/(old_vel-velocity);
				if(s.size() < 10000)
				{
					s.insert(make_pair(time, make_pair(j, i)));
					count = (count + 1) % 1000000;
				}
				else if(s.rbegin()->first > time)
				{
					set <pair<double, pair<int, int> > >::iterator temp = s.end();
					temp--;
					s.erase(temp);
					s.insert(make_pair(time, make_pair(j, i)));
					count = (count + 1) % 1000000;
				}
				iter++;
			}
		}
		m.insert(make_pair(velocity, make_pair(i, start)));
	}
	printf("%d\n", count);
	for(set<pair<double, pair<int, int> > >::iterator i = s.begin(); i != s.end(); i++)
	{
		printf("%d %d\n", (i->second).first+1, (i->second).second+1);
	}
	return 0;
}