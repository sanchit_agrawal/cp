#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <pair<int, int> > v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i].first >> v[i].second;
	sort(v.begin(), v.end());
	int max_q = 0;
	for(int i = 0; i < n; i++)
	{
		if(v[i].second < max_q)
		{
			cout << "Happy Alex" << endl;
			return 0;
		}
		max_q = v[i].second;
	}
	cout << "Poor Alex" << endl;
	return 0;
}