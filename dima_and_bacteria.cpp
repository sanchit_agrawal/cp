#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int find(int a, vector <int>& parent) {
	int p = a;
	while (parent[p] != -1) {
		p = parent[p];
	}
	int curr = a;
	while (curr != p) {
		int next = parent[curr];
		parent[curr] = p;
		curr = next;
	}
	return p;
}

int merge(int a, int b, vector <int>& parent) {
	int a_ = find(a, parent);
	int b_ = find(b, parent);
	return parent[b_] = a_;
}

int main() {
	int n, m, k;
	cin >> n >> m >> k;
	vector <int> count(k);
	for (int i = 0; i < k; i++) {
		cin >> count[i];
	}
	int counter = 0;
	vector <int> color(n);
	for (int i = 0; i < k; i++) {
		for (int j = 0; j < count[i]; j++) {
			color[counter++] = i;
		}
	}
	vector <int> parent(n, -1);
	vector <vector <int>> adjacency_matrix(k, vector <int>(k, -1));
	for (int i = 0; i < m; i++) {
		int u, v, w;
		cin >> u >> v >> w;
		u--;
		v--;
		if (w == 0) {
			merge(u, v, parent);
		}
		if (adjacency_matrix[color[u]][color[v]] == -1 || adjacency_matrix[color[u]][color[v]] > w) {
			adjacency_matrix[color[u]][color[v]] = adjacency_matrix[color[v]][color[u]] = w; 
		}
	}
	vector <int> label(k, -1);
	for (int i = 0; i < n; i++) {
		if (label[color[i]] == -1) {
			label[color[i]] = find(i, parent);
		}
		else if (label[color[i]] != find(i, parent)) {
			cout << "No" << '\n';
			return 0;
		}
	}
	cout << "Yes" << '\n';
	for (int i = 0; i < k; i++) {
		adjacency_matrix[i][i] = 0;
	}
	for (int t = 0; t < k; t++) {
		for (int i = 0; i < k; i++) {
			for (int j = 0; j < k; j++) {
				if (adjacency_matrix[i][t] != -1 && adjacency_matrix[t][j] != -1) {
					if (adjacency_matrix[i][j] == -1) {
						adjacency_matrix[i][j] = adjacency_matrix[i][t] + adjacency_matrix[t][j];
						// adjacency_matrix[j][i] = adjacency_matrix[i][t] + adjacency_matrix[t][j];
					} else {
						adjacency_matrix[i][j] = min(adjacency_matrix[i][j], adjacency_matrix[i][t] + adjacency_matrix[t][j]);
						// adjacency_matrix[j][i] = min(adjacency_matrix[i][j], adjacency_matrix[i][t] + adjacency_matrix[t][j]);
					}
				}
			}
		}
	}
	for (int i = 0; i < k; i++) {
		for (int j = 0; j < k; j++) {
			cout << adjacency_matrix[i][j] << ' ';
		}
		cout << '\n';
	}
	return 0;
}