#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <queue>

using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	priority_queue <pair<int, int> > valid_edges;
	vector <bool> visited(n, false);
	visited[0] = true;
	vector <vector <pair<int, int> > > adjacency_list(n);
	for(int i = 0; i < m; i++)
	{
		int u, v, w;
		cin >> u >> v >> w;
		u--;
		v--;
		adjacency_list[u].push_back(make_pair(-w, v));
		adjacency_list[v].push_back(make_pair(-w, u));
	}
	for(int i = 0; i < adjacency_list[0].size(); i++)
		valid_edges.push(adjacency_list[0][i]);
	int visited_count = 1;
	int longest = 0;
	while(visited_count != n)
	{
		pair <int, int> best_edge = valid_edges.top();
		valid_edges.pop();
		if(!visited[best_edge.second])
		{
			if(-best_edge.first > longest)
				longest = -best_edge.first;
			visited[best_edge.second] = true;
			for(int i = 0; i < adjacency_list[best_edge.second].size(); i++)
				if(!visited[adjacency_list[best_edge.second][i].second])
					valid_edges.push(adjacency_list[best_edge.second][i]);
			visited_count++;
		}
	}
	cout << longest << '\n';
	return 0;
}