#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#define MOD 1000000007

vector <int> fact(101);

long long pow_mod(int a, int b, int m) {
	if (b == 0) {
		return 1;
	}
	long long ans = pow_mod(a, b/2, m);
	ans = (ans * ans) % m;
	if (b % 2) {
		ans = (ans * a) % m;
	}
	return ans;
}

long long inv(int a) {
	return pow_mod(a, MOD-2, MOD);
}

long long C(int n, int r) {
	long long ans = fact[n];
	ans = (ans * inv(fact[r])) % MOD;
	ans = (ans * inv(fact[n-r])) % MOD;
	return ans;
}

int main() {
	int n;
	cin >> n;
	int a[10];
	for (int i = 0; i < 10; i++) {
		cin >> a[i];
	}
	fact[0] = 1;
	for (int i = 1; i <= 100; i++) {
		fact[i] = ((long long) fact[i-1] * i) % MOD;
	}
	vector <vector <int>> dp(n+1, vector <int>(10, 0));
	for (int i = 0; i <= 9; i++) {
		int sum = 0;
		for (int j = i; j <= 9; j++) {
			sum += a[j];
		}
		if (sum == 0) {
			dp[0][i] = 1;
		}
	}
	for (int i = 1; i <= n; i++) {
		dp[i][9] = (i >= a[9] ? 1 : 0);
		for (int j = 8; j >= 1; j--) {
			for (int k = a[j]; k <= i; k++) {
				long long ans = (C(i, k) * dp[i-k][j+1]) % MOD;
				dp[i][j] = (dp[i][j] + ans) % MOD;
			}
		}
		for (int k = a[0]; k <= i-1; k++) {
			long long ans = (C(i-1, k) * dp[i-k][1]) % MOD;
			dp[i][0] = (dp[i][0] + ans) % MOD;
		}
	}
	long long ans = 0;
	for (int i = 1; i <= n; i++) {
		ans = (ans + dp[i][0]) % MOD;
	}
	cout << ans;
	return 0;
}