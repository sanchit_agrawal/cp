#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	vector <bool> row_on(n, true);
	vector <bool> col_on(n, true);
	row_on[0] = row_on[n-1] = col_on[0] = col_on[n-1] = false;
	for (int i = 0; i < m; i++) {
		int x, y;
		cin >> x >> y;
		row_on[x-1] = false;
		col_on[y-1] = false;
	}
	int total = 0;
	for (int i = 1; i < n/2; i++) {
		total += row_on[i] + row_on[n-1-i] + col_on[i] + col_on[n-1-i];
	}
	if ((n % 2) && (row_on[n/2] || col_on[n/2])) {
		total++;
	}
	cout << total;
	return 0;
}