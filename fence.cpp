#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n, k;
	cin >> n >> k;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int curr_sum = 0;
	for(int i = 0; i < k; i++)
		curr_sum += v[k];
	int best_sum = curr_sum;
	int best_ind = 0;
	for(int i = 1; i < n-k+1; i++)
	{
		curr_sum = curr_sum - v[i-1] + v[i+k-1];
		if(curr_sum < best_sum)
		{
			best_sum = curr_sum;
			best_ind = i;
		}
	}
	cout << best_ind + 1 << endl;
	return 0;
}