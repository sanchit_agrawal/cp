s = raw_input()
weights = []
for i in range(len(s)):
	if s[i] == '1':
		weights.append(i+1)
# print weights
m = int(raw_input())
l_sum, r_sum = 0, 0
ls = [-1]
for i in range(m):
	found = False
	if i%2:
		for w in weights:
			if w != ls[-1] and r_sum + w > l_sum:
				ls.append(w)
				r_sum += w
				found = True
				break
		if not found:
			break
	else:
		for w in weights:
			if w != ls[-1] and l_sum + w > r_sum:
				ls.append(w)
				l_sum += w
				found = True
				break
		if not found:
			break
if not found:
	print 'NO'
	print ls[1:]
else:
	print 'YES'
	for w in ls[1:]:
		print w,