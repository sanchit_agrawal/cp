#include <iostream>
#include <string>

using namespace std;

string decode(string& s, int l)
{
	if(l == s.size()-1)
	{
		string a = "";
		a += s[l];
		return a;
	}
	char mid = s[l];
	string t = decode(s, l+1);
	return t.substr(0, t.length()/2)+mid+t.substr(t.length()/2);
}

int main()
{
	int n;
	cin >> n;
	string s;
	cin >> s;
	cout << decode(s, 0);
	return 0;
}