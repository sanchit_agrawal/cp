#include <iostream>

using namespace std;

int min(int a, int b, int c)
{
	if(a <= b && a <= c)
		return a;
	else if(b <= a && b <= c)
		return b;
	else
		return c;
}

int main()
{
	int a, b, c;
	cin >> a >> b >> c;
	int a_equiv = a;
	int b_equiv = b/2;
	int c_equiv = c/4;
	cout << 7*min(a_equiv, b_equiv, c_equiv);
	return 0;
}