n,k,a,b = map(int, raw_input().split())
green_is_a = True if a <= b else False
a, b = min(a, b), max(a, b)
s = ''
if b <= k*(a+1):
	while b-a > k:
		if green_is_a:
			s += 'B'*k+'G'
		else:
			s += 'G'*k+'B'
		b -= k
		a -= 1
	if green_is_a:
		s += 'B'*(b-a)
		s += 'GB'*a
	else:
		s += 'G'*(b-a)
		s += 'BG'*a
	print s
else:
	print 'NO'