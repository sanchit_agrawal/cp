n = int(raw_input())
As = [0]*n
Bs = [0]*n
opened = [False]*n
for i in range(n):
	a, b = map(int, raw_input().split())
	As[i] = a
	Bs[i] = b
for i in range(n):
	for j in range(i):
		if As[j] == Bs[i]:
			opened[j] = True
	for j in range(i+1, n):
		if As[j] == Bs[i]:
			opened[j] = True
print n - sum(opened)