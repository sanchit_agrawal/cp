#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, m, d;
	cin >> n >> m >> d;
	vector <int> v(n*m);
	int minimum = 10000;
	for(int i = 0; i < n; i++)
		for(int j = 0; j < m; j++)
		{
			cin >> v[i*m+j];
			minimum = min(minimum, v[i*m+j]);
		}
	bool possible = true;
	for(int i = 0; i < m*n; i++)
	{
		if((v[i] - minimum)%d != 0)
		{
			possible = false;
			break;
		}
	}
	if(!possible)
		cout << -1;
	else
	{
		sort(v.begin(), v.end());
		int median = v[(n*m)/2];
		int ans = 0;
		for(int i = 0; i < n*m; i++)
			ans += abs(v[i]-median);
		cout << ans/d;
	}
	return 0;
}