#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int r, g;
	cin >> r >> g;
	int h = 0;
	while (h*(h+1)/2 <= r + g) {
		h++;
	}
	h--;
	vector <int> dp(r+1, 0);
	dp[0] = 1;
	dp[1] = 1;
	vector <int> dp2(r+1);
	for (int i = 2; i <= h; i++) {
		fill(dp2.begin(), dp2.end(), 0);
		for (int j = 0; j <= r; j++) {
			if (j >= i) {
				dp2[j] = (dp2[j] + dp[j-i]) % 1000000007;
			}
			if (i*(i+1)/2 - j >= i) {
				dp2[j] = (dp2[j] + dp[j]) % 1000000007;
			}
		}
		dp = dp2;
	}
	int ans = 0;
	for (int i = r; i >= 0 && h*(h+1)/2 - i <= g; i--) {
		ans = (ans + dp[i]) % 1000000007;
	}
	cout << ans;
	return 0;
}