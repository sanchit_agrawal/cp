#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

int n, m;

int find(int u, vector <int>& parent) {
	int p = u;
	while (parent[p] != p) {
		p = parent[p];
	}
	int c = u;
	while (c != p) {
		int next = parent[c];
		parent[c] = p;
		c = next;
	}
	return p;
}

int merge(int u, int v, vector <int>& parent) {
	int u_p = find(u, parent);
	int v_p = find(v, parent);
	parent[u_p] = v_p;
	return v_p;
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		cin >> n >> m;
		vector <vector <int>> adjacency_list(n);
		for (int i = 0; i < m; i++) {
			int u, v;
			cin >> u >> v;
			adjacency_list[u-1].push_back(v-1);
			adjacency_list[v-1].push_back(u-1);
		}
		vector <vector <int>> nodes_with_degree(n);
		for (int i = 0; i < n; i++) {
			nodes_with_degree[adjacency_list[i].size()].push_back(i);
		}
		vector <int> parent(n);
		for (int i = 0; i < n; i++) {
			parent[i] = i;
		}
		vector <int> n_components(n);
		n_components[n-1] = n;
		for (int d = n-2; d >= 0; d--) {
			int decrease = 0;
			for (int u : nodes_with_degree[d+1]) {
				for (int v : adjacency_list[u]) {
					if (adjacency_list[v].size() > d) {
						int u_p = find(u, parent);
						int v_p = find(v, parent);
						if (u_p != v_p) {
							decrease++;
							merge(u, v, parent);
						}
					}
				}
			}
			n_components[d] = n_components[d+1] - decrease;
		}
		for (int d = 0; d < n; d++) {
			cout << n_components[d] - 1 << ' ';
		}
		cout << '\n';
	}
	return 0;
}