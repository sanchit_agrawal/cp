#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

#define abs(x) ((x) < 0 ? -(x) : (x))

using namespace std;

int main() {
	int a, b, c, d;
	cin >> a >> b >> c >> d;
	string root;
	if (abs(c - d) > 1) {
		root = "-1";
	} else {
		if (a < c || a < d || b < c || b < d) {
			root = "-1";
		} else {
			if (c == d) {
				if (a < c + 1 && b < d + 1) {
					root = "-1";
				}
				else if (a >= c + 1) {
					for (int i = 0; i < a-c; i++) {
						root += '4';
					}
					root += '7';
					for (int i = 0; i < c-1; i++) {
						root += "47";
					}
					for (int i = 0; i < b-c; i++) {
						root += '7';
					}
					root += '4';
				} else {
					for (int i = 0; i < c; i++) {
						root += "74";
					}
					for (int i = 0; i < b-c; i++) {
						root += '7';
					}
				}
			} else if (c > d) {
				for (int i = 0; i < a-c+1; i++) {
					root += '4';
				}
				root += '7';
				for (int i = 0; i < c-1; i++) {
					root += "47";
				}
				for (int i = 0; i < b-c; i++) {
					root += '7';
				}
			} else {
				root += "74";
				for (int i = 0; i < a-d; i++) {
					root += '4';
				}
				for (int i = 0; i < d-2; i++) {
					root += "74";
				}
				for (int i = 0; i < b-d+1; i++) {
					root += '7';
				}
				root += '4';
			}
		}
	}
	cout << root;
	return 0;
}