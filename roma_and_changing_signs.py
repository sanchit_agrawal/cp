n, k = map(int, raw_input().split())
total = 0
ls = map(int, raw_input().split())
total_negative = 0
for l in ls:
	if l <= 0:
		total_negative += 1
	else:
		break
if k <= total_negative:
	for i in range(k):
		ls[i] = -ls[i]
else:
	ls = map(abs, ls)
	ls.sort()
	if (k-total_negative) % 2 != 0:
		ls[0] = -ls[0]
print sum(ls)