#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <long long> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	if(n <= 2)
	{
		cout << n << endl;
		return 0;
	}
	int longest = 2;
	int l = 0, r = 0;
	while(r < n)
	{
		if(r <= l+1)
			r++;
		else
		{
			if(v[r] == v[r-1]+v[r-2])
			{
				longest = max(longest, r-l+1);
				r++;
			}
			else
			{
				l = r-1;
			}
		}
	}
	cout << longest << endl;
	return 0;
}