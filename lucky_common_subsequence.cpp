#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int dp[101][101][101];
int is[101][101][101];
int js[101][101][101];
int ks[101][101][101];

int main() {
	string s, t, v;
	cin >> s >> t >> v;
	int n = s.size(), m = t.size(), p = v.size();
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			if (dp[i-1][j][0] >= dp[i][j][0]) {
				dp[i][j][0] = dp[i-1][j][0];
				is[i][j][0] = i-1;
				js[i][j][0] = j;
				ks[i][j][0] = 0;
			} 
			if (dp[i][j-1][0] > dp[i][j][0]) {
				dp[i][j][0] = dp[i][j-1][0];
				is[i][j][0] = i;
				js[i][j][0] = j-1;
				ks[i][j][0] = 0;
			}
			if (s[i-1] == t[j-1] && s[i-1] != v[0]) {
				for (int k = 0; k < p; k++) {
					if (s[i-1] != v[k]) {
						if (dp[i-1][j-1][k] + 1 > dp[i][j][0]) {
							dp[i][j][0] = dp[i-1][j-1][k] + 1;
							is[i][j][0] = i-1;
							js[i][j][0] = j-1;
							ks[i][j][0] = k;
						}
					}
				}
			}
			for (int k = 1; k < p; k++) { 
				if (dp[i-1][j][k] >= dp[i][j][k]) {
					dp[i][j][k] = dp[i-1][j][k];
					is[i][j][k] = i-1;
					js[i][j][k] = j;
					ks[i][j][k] = k;
				}
				if (dp[i][j-1][k] > dp[i][j][k]) {
					dp[i][j][k] = dp[i][j-1][k];
					is[i][j][k] = i;
					js[i][j][k] = j-1;
					ks[i][j][k] = k;
				}
				if (s[i-1] == t[j-1] && s[i-1] == v[k-1] && dp[i-1][j-1][k-1] + 1 > dp[i][j][k]) {
					dp[i][j][k] = dp[i-1][j-1][k-1] + 1;
					is[i][j][k] = i-1;
					js[i][j][k] = j-1;
					ks[i][j][k] = k-1;
				}
			}
		}
	}
	int ans = 0;
	int best_k = 0;
	for (int k = 0; k < p; k++) {
		if (dp[n][m][k] > ans) {
			ans = dp[n][m][k];
			best_k = k;
		}
	}
	// cout << ans << '\n';
	if (ans == 0) {
		cout << 0;
	} else {
		string out;
		int i = n, j = m, k = best_k;
		while (i > 0 && j > 0) {
			if (is[i][j][k] == i-1 && js[i][j][k] == j-1) {
				out += s[i-1];
			}
			i = is[i][j][k];
			j = js[i][j][k];
			k = ks[i][j][k];
		}
		cout << out;
	}
	return 0;
}
