#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int mod_num = (int) 1e9 + 7;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		vector <int> v(n);
		for (int i = 0; i < n; i++) {
			cin >> v[i];
		}
		vector <int> premulsum(n);
		premulsum[0] = v[0];
		premulsum[1] = (v[1] + (long long) v[1] * v[0]) % mod_num;
		int two_pow = 1;
		for (int i = 2; i < n; i++) {
			two_pow = ((long long) two_pow * 2) % mod_num;
			premulsum[i] = ((long long) v[i] * two_pow + (long long) v[i] * premulsum[i-1]) % mod_num;
		}
		// dp[i] = complete answer for [0..i] = presum(dp[i-1]) + premulsum[i]
		vector <int> dp(n);
		vector <int> dppresum(n);
		dp[0] = dppresum[0] = v[0];
		for (int i = 1; i < n; i++) {
			dp[i] = ((long long) dppresum[i-1] + premulsum[i]) % mod_num;
			dppresum[i] = ((long long) dppresum[i-1] + dp[i]) % mod_num;
		}
		cout << dp[n-1] << '\n';
	}
	return 0;
}