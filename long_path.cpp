#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int mod(int x) {
	if (x >= 0) {
		return x % 1000000007;
	} else {
		return mod(x + 1000000007);
	}
}

int main() {
	int n;
	cin >> n;
	vector <int> p(n+2);
	for (int i = 1; i <= n; i++) {
		cin >> p[i];
	}
	vector <int> dp(n+2);
	dp[1] = 0;
	for (int i = 1; i <= n; i++) {
		dp[i+1] = mod(2*dp[i] - dp[p[i]] + 2);
	}
	cout << dp[n+1];
	return 0;
}