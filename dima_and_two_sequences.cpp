#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

using namespace std;

long long pow_mod(int a, int b, int mod) {
	if (b == 0) {
		return 1;
	}
	long long ans = pow_mod(a, b/2, mod);
	ans = (ans * ans) % mod;
	if (b % 2) {
		ans = (ans * a) % mod;
	}
	return ans;
}

int main() {
	int n;
	cin >> n;
	map <int,map<int,int>> m;
	for (int i = 0; i < n; i++) {
		int x;
		cin >> x;
		m[x][i+1]++;
	}
	for (int i = 0; i < n; i++) {
		int x;
		cin >> x;
		m[x][i+1]++;
	}
	int mod;
	cin >> mod;
	vector <int> fact_wo_2(2*n+1);
	vector <int> fact_2_pow(2*n+1);
	fact_wo_2[0] = 1;
	fact_2_pow[0] = 0;
	for (int i = 1; i <= 2*n; i++) {
		int pow_2 = 0;
		int j = i;
		while (j % 2 == 0) {
			j /= 2;
			pow_2++;
		}
		fact_2_pow[i] = fact_2_pow[i-1] + pow_2;
		fact_wo_2[i] = ((long long) j * fact_wo_2[i-1]) % mod;
	}
	long long ans = 1;
	for (auto it = m.begin(); it != m.end(); ++it) {
		int size = 0;
		int repeats = 0;
		for (auto itt = it->second.begin(); itt != it->second.end(); ++itt) {
			size += itt->second;
			repeats += itt->second/2;
		}
		// cout << "size: " << size << endl;
		// cout << "repeats: " << repeats << endl;
		int add_2_pow = fact_2_pow[size] - repeats;
		ans = (((ans * fact_wo_2[size]) % mod) * pow_mod(2, add_2_pow, mod)) % mod;
		// cout << it->first << " : " << ans << endl;
		// cout << fact_wo_2[size] << endl;
		// cout << add_2_pow << endl;
		// cout << pow_mod(2, add_2_pow, mod) << endl;
	}
	cout << ans;
	return 0;
}