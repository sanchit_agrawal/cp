#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, v;
	cin >> n >> v;
	vector <int> arr(3002, 0);
	for(int i = 0; i < n; i++)
	{
		int a, b;
		cin >> a >> b;
		arr[a] += b;
	}
	int count = 0;
	for(int i = 1; i <= 3001; i++)
	{
		if(arr[i-1] >= v)
		{
			count += v;
			arr[i-1] -= v;
		}
		else
		{
			if(arr[i-1] + arr[i] <= v)
			{
				count += arr[i-1] + arr[i];
				arr[i] = arr[i-1] = 0;
			}
			else
			{
				count += v;
				arr[i] -= (v - arr[i-1]);
				arr[i-1] = 0;
			}
		}
	}
	cout << count << '\n';
	return 0;
}