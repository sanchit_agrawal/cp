#include <iostream>
#include <string>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		string s;
		cin >> s;
		int l = 0, r = n-1;
		bool is_palin = true;
		while(l < r)
		{
			if(s[l] != s[r])
			{
				is_palin = false;
				break;
			}
			l++;
			r--;
		}
		cout << (is_palin ? 0 : 1) << '\n';
	}
	return 0;
}