#include <iostream>
#include <vector>

using namespace std;

int n, m;

void dfs2(int root, vector <bool>& visited, vector <vector<int> >& adjacency_list)
{
	visited[root] = true;
	for(int i = 0; i < adjacency_list[root].size(); i++)
	{
		if(!visited[adjacency_list[root][i]])
			dfs2(adjacency_list[root][i], visited, adjacency_list);
	}
}

int dfs(vector <vector<int> >& adjacency_list)
{
	vector <bool> visited(n+m, false);
	int count = 0;
	for(int i = 0; i < n; i++)
		if(!visited[i])
		{
			count++;
			dfs2(i, visited, adjacency_list);
		}
	return count;
}

int main()
{
	cin >> n >> m;
	vector <vector <int> > adjacency_list(n+m);
	bool atleast_one = false;
	for(int i = 0; i < n; i++)
	{
		int nlangs;
		cin >> nlangs;
		if(nlangs)
			atleast_one = true;
		for(int j = 0; j < nlangs; j++)
		{
			int langno;
			cin >> langno;
			langno--;
			adjacency_list[i].push_back(langno+n);
			adjacency_list[langno+n].push_back(i);
		}
	}
	int ans = dfs(adjacency_list);
	cout << (!atleast_one ? n : (ans-1)) << '\n';
	return 0;
}