s = raw_input().strip()
t = raw_input().strip()
# p[i] denotes the index of the longest prefix which is also a proper suffix of the prefix t[0..i]
p = [0]*len(t)
p[0] = -1
for i in range(1, len(t)):
	p[i] = -1 if t[0] != t[i] else 0
	best_guess = p[i-1]
	while best_guess != -1:
		if t[i] == t[best_guess+1]:
			p[i] = best_guess+1
			break
		else:
			best_guess = p[best_guess]
j = -1
end = []
for i in range(len(s)):
	if s[i] == t[j+1]:
		if j == len(t)-2:
			end.append(i)
			j = p[j+1]
		else:
			j += 1
	else:
		while j != -1 and s[i] != t[j+1]:
			j = p[j]
		if t[j+1] == s[i]:
			j += 1
if len(end) == 0:
	print 0
else:
	start = [e - len(t) + 1 for e in end]
	prev = []
	j = len(start)-1
	for i in range(len(start)-1, -1, -1):
		while j >= 0 and end[j] >= start[i]:
			j -= 1
		prev.append(j)
	prev.reverse()
	print start
	print end
	print prev
	dp = [0]*(len(start)+1)
	dp_sum = [0]*(len(start)+1)
	dp_1 = [0]*(len(start)+1)
	dp_2 = [0]*(len(start)+1)
	def absmod(a, b):
		while a < 0:
			a += b
		return a % b
	for i in range(len(start)):
		dp[i] = absmod((dp_sum[prev[i]]*(start[i]+start[i]**2)/2)-dp_1[prev[i]]*start[i]+dp_2[prev[i]]/2+start[i]+1, 1000000007)
		dp_sum[i] = absmod(dp_sum[i-1] + dp[i], 1000000007)
		dp_1[i] = absmod(dp_1[i-1] + dp[i]*end[i], 1000000007)
		dp_2[i] = absmod(dp_2[i-1] + dp[i]*((end[i]**2)-end[i]) , 1000000007)
	ans = absmod(-dp_1[-2]+dp_sum[-2]*(len(s)+1), 1000000007)
	# print ans
	print dp