#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;

int r, c;

void bfs(queue <pair <int, int>>& q, int d, vector <vector <int>>& dist, vector <string>& v) {
	int size = q.size();
	if (size == 0) {
		return;
	}
	for (int i = 0; i < size; i++) {
		auto top = q.front();
		q.pop();
		int x = top.first;
		int y = top.second;
		if (x-1 >= 0 && dist[x-1][y] == -1 && v[x-1][y] != 'T') {
			q.push(make_pair(x-1, y));
			dist[x-1][y] = d + 1;
		}
		if (x+1 < r && dist[x+1][y] == -1 && v[x+1][y] != 'T') {
			q.push(make_pair(x+1, y));
			dist[x+1][y] = d + 1;
		}
		if (y-1 >= 0 && dist[x][y-1] == -1 && v[x][y-1] != 'T') {
			q.push(make_pair(x, y-1));
			dist[x][y-1] = d + 1;
		}
		if (y+1 < c && dist[x][y+1] == -1 && v[x][y+1] != 'T') {
			q.push(make_pair(x, y+1));
			dist[x][y+1] = d + 1;
		}
	}
	bfs(q, d+1, dist, v);
}

int main() {
	cin >> r >> c;
	vector <string> v(r);
	for (int i = 0; i < r; i++) {
		cin >> v[i];
	}
	int endr, endc;
	int startr, startc;
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			if (v[i][j] == 'E') {
				endr = i;
				endc = j;
			}
			if (v[i][j] == 'S') {
				startr = i;
				startc = j;
			}
		}
	}
	vector <vector <int>> dist(r, vector <int>(c, -1));
	queue <pair <int, int>> q;
	q.push(make_pair(endr, endc));
	dist[endr][endc] = 0;
	bfs(q, 0, dist, v);
	int my_dist = dist[startr][startc];
	int ans = 0;
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			if (v[i][j] >= '1' && v[i][j] <= '9') {
				if (dist[i][j] != -1 && dist[i][j] <= my_dist) {
					ans += v[i][j] - '0';
				}
			}
		}
	}
	cout << ans;
	return 0;
}