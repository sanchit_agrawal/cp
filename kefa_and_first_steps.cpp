#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int max_len = 1;
	int l = 0, r = 0;
	while(r < n)
	{
		if(r == l)
			r++;
		else
		{
			if(v[r] >= v[r-1])
			{
				max_len = max(max_len, r-l+1);
				r++;
			}
			else
			{
				l = r;
			}
		}
	}
	cout << max_len << '\n';
	return 0;
}