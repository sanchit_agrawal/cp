#include <iostream>
#include <iomanip>

using namespace std;

int main() {
	double s;
	cin >> s;
	double a, b, c;
	cin >> a >> b >> c;
	if (a == 0 && b == 0 && c == 0) {
		cout << 0 << ' ' << 0 << ' ' << 0;
	} else {
		cout << setprecision(15) << (a*s/(a + b + c)) << ' ' << (b*s/(a + b + c)) << ' ' << (c*s/(a + b + c));
	}
	return 0;
}