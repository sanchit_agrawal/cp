t = int(raw_input())
for t_ in range(t):
	n = int(raw_input())
	hero_count = 0
	villain_count = 0
	done = False
	for i in range(n):
		s = raw_input().strip()
		if not done:
			if s[-3:] == 'man':
				hero_count += 1
			else:
				villain_count += 1
			if hero_count == villain_count + 2:
				print 'superheroes'
				done = True
			if villain_count == hero_count + 3:
				print 'villains'
				done = True
	if not done:
		print 'draw'