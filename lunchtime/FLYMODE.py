n = int(raw_input())
v = map(int, raw_input().split())
s = set(v)
s2 = set()
last = 0
for h in sorted(s):
	s2.add(float(last+h)/2)
	last = h
s = s | s2
n_heights = len(s)
index = {}
for i, h in enumerate(sorted(s)):
	index[h] = i
deltas = [0]*(len(s)+1)
if n == 1:
	print 1
else:
	if v[1] > v[0]:
		deltas[index[v[0]]] += 1
		deltas[index[v[1]]+1] -= 1
	else:
		deltas[index[v[1]]] += 1
		deltas[index[v[0]+1]] -= 1
	for i in range(2, n):
		if v[i] > v[i-1]:
			deltas[index[v[i-1]]+1] += 1
			deltas[index[v[i]]+1] -= 1
		else:
			deltas[index[v[i]]] += 1
			deltas[index[v[i-1]]] -= 1
	curr = 0
	ans = 0
	for d in deltas:
		curr += d
		ans = max(ans, curr)
	print ans
