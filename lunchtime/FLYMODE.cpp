#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	set <int> s;
	for(int i = 0; i < n; i++)
	{
		cin >> v[i];
		s.insert(v[i]);
	}
	int n_heights = s.size();
	map <int, int> index;
	int curr_ind = 0;
	for(int h : s)
		index[h] = curr_ind++;
	// Checked till here.
	vector <int> deltas(n_heights+1, 0);
	if(n == 1)
	{
		cout << 1;
	}
	else
	{
		if(v[1] > v[0])
		{
			deltas[index[v[1]]+1]--;
			deltas[index[v[0]]]++;
		}
		else
		{
			deltas[index[v[1]]]++;
			deltas[index[v[0]]+1]--;
		}
		for(int i = 2; i < n; i++)
		{
			if(v[i] > v[i-1])
			{
				deltas[index[v[i-1]]+1]++;
				deltas[index[v[i]]+1]--;
			}
			else
			{
				deltas[index[v[i]]]++;
				deltas[index[v[i-1]]]--;
			}
		}
		int curr = 0;
		int maximum = 0;
		for(int i = 0; i < n_heights+1; i++)
		{
			curr += deltas[i];
			maximum = max(curr, maximum);
		}
		cout << maximum;
	}
	return 0;
}