#include <iostream>
#include <vector>
#include <string>

using namespace std;

int n, m, k;

#define abs(x) ((x) < 0 ? -(x) : (x))

bool in_range(int i, int j)
{
	return i >= 0 && i < n && j >= 0 && j < m;
}

void dfs(int i, int j, vector <string>& v)
{
	if(k == 0)
		return;
	v[i][j] = -1*'.';
	if(in_range(i-1, j) && v[i-1][j] == '.')
		dfs(i-1, j, v);
	if(in_range(i, j-1) && v[i][j-1] == '.')
		dfs(i, j-1, v);
	if(in_range(i, j+1) && v[i][j+1] == '.')
		dfs(i, j+1, v);
	if(in_range(i+1, j) && v[i+1][j] == '.')
		dfs(i+1, j, v);
	if(k > 0)
	{
		v[i][j] = 'X';
		k--;
	}
}

int main()
{
	cin >> n >> m >> k;
	vector <string> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int i = 0, j = 0;
	for(i = 0; i < n; i++)
	{
		bool found = false;
		for(j = 0; j < m; j++)
			if(v[i][j] == '.')
			{
				found = true;
				break;
			}
		if(found)
			break;
	}
	// cout << i << ' ' << j << '\n';
	dfs(i, j, v);
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
			cout << (char)abs(v[i][j]);
		cout << '\n';
	}
	return 0;
}