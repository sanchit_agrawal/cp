from collections import deque
n, m, k = map(int, raw_input().split())
ls = map(int, raw_input().split())
sums = [ls[0]]*n
for i in range(1, n):
	sums[i] = sums[i-1] + ls[i]
dp = deque([[0]+[float('-inf')]*(k) for i in range(m)])
for i in range(m, n+1):
	ls = [0]*(k+1)
	oldest = dp[0]
	last = dp[-1]
	for j in range(1, k+1):
		ls[j] = max(oldest[j-1]+sums[i-1]-(sums[i-m-1] if i-m-1 >= 0 else 0), last[j])
	dp.popleft()
	dp.append(ls)
print dp[-1][-1]