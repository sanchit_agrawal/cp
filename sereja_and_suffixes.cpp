#include <iostream>
#include <vector>
#include <set>

using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	vector <bool> occurred(100001, false);
	vector <int> lens(n);
	int num = 0;
	for(int i = n-1; i >= 0; i--)
	{
		if(!occurred[v[i]])
		{
			occurred[v[i]] = true;
			num++;
		}
		lens[i] = num;
	}
	for(int i = 0; i < m; i++)
	{
		int ind;
		cin >> ind;
		ind--;
		cout << lens[ind] << '\n';
	}
	return 0;
}