#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int max_height_ind = 0;
	int min_height_ind = 0;
	for(int i = 0; i < n; i++)
	{
		if(v[i] <= v[min_height_ind])
			min_height_ind = i;
		if(v[i] > v[max_height_ind])
			max_height_ind = i;
	}
	if(max_height_ind == min_height_ind)
		cout << 0 << endl;
	else if(max_height_ind < min_height_ind)
		cout << max_height_ind + (n-1-min_height_ind) << endl;
	else
		cout << max_height_ind + (n-1-min_height_ind) - 1 << endl;
	return 0;
}