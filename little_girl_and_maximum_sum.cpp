#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, q;
	scanf("%d%d", &n, &q);
	vector <int> arr(n);
	int temp;
	for(int i = 0; i < n; i++)
	{
		scanf("%d", &temp);
		arr[i] = temp;
	}
	vector <int> delta(n, 0);
	for(int i = 0; i < q; i++)
	{
		int l, r;
		scanf("%d%d", &l, &r);
		l--;
		r--;
		delta[l]++;
		if(r != n-1)
			delta[r+1]--;
	}
	vector <int> freq(n, 0);
	int curr = 0;
	for(int i = 0; i < n; i++)
	{
		curr += delta[i];
		freq[i] = curr;
	}
	sort(freq.begin(), freq.end());
	sort(arr.begin(), arr.end());
	long long ans = 0;
	for(int i = 0; i < n; i++)
		ans += (long long) freq[i] * arr[i];
	cout << ans;
	return 0;
}