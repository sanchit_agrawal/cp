#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	long long nums[3];
	cin >> nums[0] >> nums[1] >> nums[2];
	sort(nums, nums+3);
	long long ans1 = 0;
	ans1 += nums[0];
	ans1 += (nums[1]-nums[0])/3;
	ans1 += (nums[2]-nums[0])/3;
	long long ans2 = 0;
	ans2 += max(nums[0]-1, (long long) 0);
	ans2 += (nums[1]-max(nums[0]-1, (long long) 0))/3;
	ans2 += (nums[2]-max(nums[0]-1, (long long) 0))/3;
	long long ans3 = 0;
	ans3 += max(nums[0]-2, (long long) 0);
	ans3 += (nums[1]-max(nums[0]-2, (long long) 0))/3;
	ans3 += (nums[2]-max(nums[0]-2, (long long) 0))/3;
	cout << max(ans1, max(ans2, ans3));
	return 0;
}