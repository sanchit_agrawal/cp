n = int(raw_input())
curr = [0]*4
curr[3] = 1
for i in range(1, n+1):
	temp = [0]*4
	temp[0] = (curr[1] + curr[2] + curr[3]) % 1000000007
	temp[1] = (curr[0] + curr[2] + curr[3]) % 1000000007
	temp[2] = (curr[1] + curr[0] + curr[3]) % 1000000007
	temp[3] = (curr[1] + curr[2] + curr[0]) % 1000000007
	curr = temp
print temp[3]
