#include <iostream>
#include <iomanip>

using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		double a, b;
		cin >> a >> b;
		if (a >= 4*b) {
			if (a == 0) {
				cout << 1 << '\n';
			} else {
				cout << setprecision(7) << 1 - b/a << '\n';
			}
		} else {
			cout << setprecision(7) << 0.5 + a/16/b << '\n';
		}
	}
	return 0;
}