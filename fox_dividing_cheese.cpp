#include <iostream>

using namespace std;

int gcd(int a, int b)
{
	if(b == 0)
		return a;
	return gcd(b, a % b);
}

int main()
{
	int a, b;
	cin >> a >> b;
	int g = gcd(a, b);
	int count = 0;
	int qa = a / g;
	while(qa % 2 == 0)
	{
		qa /= 2;
		count++;
	}
	while(qa % 3 == 0)
	{
		qa /= 3;
		count++;
	}
	while(qa % 5 == 0)
	{
		qa /= 5;
		count++;
	}
	if(qa != 1)
	{
		cout << -1;
		return 0;
	}
	int qb = b / g;
	while(qb % 2 == 0)
	{
		qb /= 2;
		count++;
	}
	while(qb % 3 == 0)
	{
		qb /= 3;
		count++;
	}
	while(qb % 5 == 0)
	{
		qb /= 5;
		count++;
	}
	if(qb != 1)
	{
		cout << -1;
		return 0;
	}
	cout << count;
	return 0;
}