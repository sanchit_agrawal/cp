n, m = map(int, raw_input().split())
ls = []
for i in range(n):
	ls.append(raw_input())
def is_proper(p):
	i, j = p
	return i < n and i >= 0 and j < m and j >= 0
def is_adjacent(i, j):
	pos = [(i, j-1), (i-1, j), (i, j+1), (i+1, j)]
	for i, j in pos:
		if is_proper((i, j)) and ls[i][j] == 'P':
			return True
	return False
count = 0
for i in range(n):
	for j in range(m):
		if ls[i][j] == 'W' and is_adjacent(i, j):
			count += 1
print count