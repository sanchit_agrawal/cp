#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

vector <long> lucky_numbers;

int binsearch(int n, int l, int r)
{
	if(l > r)
		return -1;
	else if(l == r)
		return (lucky_numbers[l] >= n ? l : -1);
	int mid = (l+r)/2;
	if(lucky_numbers[mid] < n)
		return binsearch(n, mid+1, r);
	else if(lucky_numbers[mid] == n)
		return mid;
	else
		return binsearch(n, l, mid);
}

int main()
{
	int n;
	cin >> n;
	for(int i = 2; i <= 8; i+=2)
	{
		string s;
		int j = i/2;
		while(j--)
			s += '4';
		j = i/2;
		while(j--)
			s += '7';
		lucky_numbers.push_back(stoi(s));
		while(next_permutation(s.begin(), s.end()))
			lucky_numbers.push_back(stoi(s));
	}
	int ind = binsearch(n, 0, lucky_numbers.size()-1);
	if(ind == -1)
		cout << "4444477777";
	else
		cout << lucky_numbers[ind];
}