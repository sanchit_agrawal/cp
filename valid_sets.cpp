#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

int d, n, mod_num = (int) 1e9 + 7;

int dfs(int root, int min, int org_root, int parent, vector <vector <int>>& adjacency_list, vector <int>& val) {
	long long ans = 1;
	auto p = make_pair(min, org_root);
	for (int child : adjacency_list[root]) {
		auto q = make_pair(val[child], child);
		if (child != parent && p < q && val[child] <= min + d) {
			ans = (ans * (dfs(child, min, org_root, root, adjacency_list, val) + 1)) % mod_num;
		}
	}
	return ans;
}

int main() {
	cin >> d >> n;
	vector <int> val(n);
	for (int i = 0; i < n; i++) {
		cin >> val[i];
	}
	vector <vector <int>> adjacency_list(n);
	for (int i = 0; i < n-1; i++) {
		int u, v;
		cin >> u >> v;
		adjacency_list[u-1].push_back(v-1);
		adjacency_list[v-1].push_back(u-1);
	}
	long long ans = 0;
	for (int i = 0; i < n; i++) {
		ans = (ans + dfs(i, val[i], i, -1, adjacency_list, val)) % mod_num;
	}
	cout << ans;
	return 0;
}