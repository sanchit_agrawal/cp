#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	long long n, k;
	cin >> n >> k;
	vector <vector<int>> dp(k+1, vector<int>(n+1, 0));
	for(int i = 1; i <= n; i++)
		dp[1][i] = 1;
	for(int t = 1; t < k; t++)
		for(int i = 1; i <= n; i++)
			for(int j = i; j <= n; j += i)
				dp[t+1][j] = (dp[t][i] + dp[t+1][j]) % 1000000007;
	int ans = 0;
	for(int i = 1; i <= n; i++)
		ans = (ans + dp[k][i]) % 1000000007;
	cout << ans;
	return 0;
}