#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

bool used[1000001];
int ys[1000000];

int main() {
	int n;
	scanf("%d", &n);
	long long sum = 0;
	for (int i = 0; i < n; i++) {
		int x;
		scanf("%d", &x);
		used[x] = true;
		sum += x-1;
	}
	int m = 1000000 - n;
	int npairs = 0;
	int ind = 0;
	for (int i = 1; i <= 1000000/2; i++) {
		if (used[i] && used[1000000-i+1]) {
			npairs++;
		} else if (used[i]) {
			ys[ind++] = 1000000-i+1;
		} else if (used[1000000-i+1]) {
			ys[ind++] = i;
		}
	}
	int i = 1;
	while (npairs > 0) {
		if (!used[i] && !used[1000000-i+1]) {
			ys[ind++] = i;
			ys[ind++] = 1000000-i+1;
			npairs--;
 		}
 		i++;
	}
	printf("%d\n", ind);
	for (int i = 0; i < ind; i++) {
		printf("%d ", ys[i]);
	}
	return 0;
}