#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	while(n--)
	{
		string s1, s2;
		cin >> s1 >> s2;
		reverse(s1.begin(), s1.end());
		reverse(s2.begin(), s2.end());
		int a = stoi(s1);
		int b = stoi(s2);
		string c = to_string(a + b);
		reverse(c.begin(), c.end());
		int c_int = stoi(c);
		cout << c_int << endl;
	}
	return 0;
}