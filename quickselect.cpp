#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

void swap(vector <int>& v, int a, int b)
{
	int temp = v[a];
	v[a] = v[b];
	v[b] = temp;
}

int quickselect(vector <int>& v, int l, int r, int k)
{
	if(l == r)
		return k;
	else if(l == r-1)
	{
		if(k == l) return (v[l] <= v[r] ? l : r);
		else return (v[l] <= v[r] ? r : l);
	}
	else
	{
		int pivot = rand() % (r-l+1) + l;
		swap(v, r, pivot);
		int y = r-1, x = l;
		while(y > x)
		{
			if(v[x] < v[r])
				x++;
			else if(v[y] >= v[r])
				y--;
			else
				swap(v, x, y);
		}
		if(v[x] < v[r])
			pivot = x+1;
		else
			pivot = x;
		swap(v, r, pivot);
		if(pivot == k)
			return pivot;
		else if(pivot < k)
			return quickselect(v, pivot+1, r, k);
		else
			return quickselect(v, l, pivot-1, k);
	}
}

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	for(int i = 0; i < n; i++)
	{
		cout << v[quickselect(v, 0, n-1, i)] << " ";
	}
	cout << endl;
	// cout << v[quickselect(v, 0, n-1, 2)] << endl;
	return 0;
}