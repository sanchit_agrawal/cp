#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int add = 0;
	for(int i = 0; i < m; i++)
	{
		int t;
		cin >> t;
		if(t == 1)
		{
			int idx, val;
			cin >> idx >> val;
			v[idx-1] = val-add; 
		}
		else if(t == 2)
		{
			int addn;
			cin >> addn;
			add += addn;
		}
		else
		{
			int idx;
			cin >> idx;
			cout << v[idx-1]+add << '\n';
		}
	}
	return 0;
}