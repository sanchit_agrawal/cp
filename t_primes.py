n = int(raw_input())
ls = map(int, raw_input().split())
is_prime = [True]*((10**6)+1)
is_prime[0] = is_prime[1] = False
for i in range(2, 10**6+1):
	if is_prime[i]:
		for j in range(2*i, 10**6+1, i):
			is_prime[j] = False
for l in ls:
	sqrt = int(l**0.5)
	if sqrt**2 == l and is_prime[sqrt]:
		print 'YES'
	else:
		print 'NO'