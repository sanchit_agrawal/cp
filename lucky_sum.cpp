#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

long long next(int n)
{
	if(n <= 4)
		return 4;
	else if(n <= 7)
		return 7;
	long long ans = next(n/10);
	if(ans > n/10)
		return 10*ans+4;
	else
	{
		if(n%10 > 7)
			return 10*next(n/10+1)+4;
		else
			return 10*next(n/10)+next(n%10);
	}
}

long long next_spl(int n)
{
	if(n == 0)
		return 4;
	else if(n % 10 == 4)
		return 10*(n/10)+7;
	else
		return 10*next_spl(n/10)+4;
}

int main()
{
	int l, r;
	cin >> l >> r;
	long long ans = 0;
	long long curr = next(l);
	long long prev = l;
	ans += curr;
	while(curr < r)
	{
		ans += curr * (curr - prev);
		prev = curr;
		curr = next_spl(curr);
	}
	ans += curr * (r - prev);
	cout << ans << endl;
	return 0;
}