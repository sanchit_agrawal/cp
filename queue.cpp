#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int n;

void build(int root, int l, int r, vector <int>& tree, vector <int>& v) {
	if (l == r) {
		tree[root] = l;
		return;
	}
	int mid = (l+r)/2;
	build(2*root + 1, l, mid, tree, v);
	build(2*root + 2, mid+1, r, tree, v);
	if (v[tree[2*root + 1]] < v[tree[2*root + 2]]) {
		tree[root] = tree[2*root + 1];
	} else {
		tree[root] = tree[2*root + 2];
	}
}

int query(int q, int root, int l, int r, int x, int y, vector <int>& tree, vector <int>& v) {
	if (r < x || l > y) {
		return -1;
	}
	if (v[tree[root]] >= q) {
		return -1;
	}
	if (l == r) {
		return tree[root];
	}
	int mid = (l + r)/2;
	int rans = query(q, 2*root + 2, mid+1, r, x, y, tree, v);
	if (rans == -1) {
		return query(q, 2*root + 1, l, mid, x, y, tree, v);
	} else {
		return rans;
	}
}

int main() {
	cin >> n;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	vector <int> tree(4*n);
	build(0, 0, n-1, tree, v);
	for (int i = 0; i < n-1; i++) {
		int idx = query(v[i], 0, 0, n-1, i+1, n-1, tree, v);
		if (idx == -1) {
			cout << -1 << ' ';
		} else {
			cout << idx - i - 1 << ' ';
		}
	}
	cout << -1;
	return 0;
}