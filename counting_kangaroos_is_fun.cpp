#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> size(n);
	for(int i = 0; i < n; i++)
		cin >> size[i];
	sort(size.begin(), size.end());
	int l = 0, r = (n+1)/2;
	int visible = n;
	while(r < n)
	{
		if(size[r] >= 2*size[l])
		{
			visible--;
			l++;
			r++;
		}
		else
			r++;
	}
	cout << visible;
	return 0;
}