#include <iostream>
#include <vector>

using namespace std;

vector <float> v;

int binsearch(float req, int l = 0, int r = v.size()-1)
{
	if(l == r)
		return l+1;
	else
	{
		int mid = (l+r)/2;
		if(v[mid] == req)
			return mid+1;
		else if(v[mid] < req)
			return binsearch(req, mid+1, r);
		else
			return binsearch(req, l, mid);
	}
}

int main()
{
	float curr = 0;
	int n = 0;
	while(curr <= 5.20)
	{
		n++;
		curr += 1.0/(n+1);
		v.push_back(curr);
	}
	while(true)
	{
		cin >> curr;
		if(curr == 0)
			return 0;
		cout << binsearch(curr) << ' ' << "card(s)" << endl;
	}
}