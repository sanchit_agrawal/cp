#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	string s;
	while(true)
	{
		cin >> s;
		if(s == "0")
			return 0;
		long long prev_prev = 1, prev = 1;
		for(int i = 2; i <= s.size(); i++)
		{
			long long curr = prev;
			int num = 10*(s[i-2] - '0')+(s[i-1] - '0');
			if(num <= 26)
				curr += prev_prev;
			prev_prev = prev;
			prev = curr;
		}
		cout << prev << '\n';
	}
}