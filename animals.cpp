#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, m;
	ifstream fin("input.txt");
	ofstream fout("output.txt");
	fin >> n >> m;
	vector <int> food(n);
	for (int i = 0; i < n; i++) {
		fin >> food[i];
		food[i] = food[i] * (n-i);
	}
	sort(food.begin(), food.end());
	int idx = 0;
	while (idx < n && m >= food[idx]) {
		m -= food[idx];
		idx++;
	}
	fout << idx;
	return 0;
}