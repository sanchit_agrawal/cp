#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int n_fixed_pts = 0;
	bool can_switch = false;
	for(int i = 0; i < n; i++)
	{
		if(v[i] == i)
			n_fixed_pts++;
		else
		{
			if(v[v[i]] == i)
				can_switch = true;
		}
	}
	if(n_fixed_pts == n)
		cout << n_fixed_pts;
	else if(can_switch)
		cout << n_fixed_pts + 2;
	else
		cout << n_fixed_pts + 1;
	return 0;
}