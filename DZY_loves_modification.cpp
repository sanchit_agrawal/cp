#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

int main() {
	long long n, m, k, p;
	cin >> n >> m >> k >> p;
	vector <vector <int>> v(n, vector <int>(m));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cin >> v[i][j];
		}
	}
	vector <int> row_sums(n, 0);
	vector <int> col_sums(m, 0);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			row_sums[i] += v[i][j];
			col_sums[j] += v[i][j];
		}
	}
	priority_queue <long long> row_pq(row_sums.begin(), row_sums.end());
	priority_queue <long long> col_pq(col_sums.begin(), col_sums.end());
	vector <long long> row_ans(k+1);
	vector <long long> col_ans(k+1);
	row_ans[0] = col_ans[0] = 0;
	for (int i = 1; i <= k; i++) {
		long long row_top = row_pq.top();
		row_ans[i] = row_ans[i-1] + row_top;
		row_pq.pop();
		row_pq.push(row_top - p*m);
		long long col_top = col_pq.top();
		col_ans[i] = col_ans[i-1] + col_top;
		col_pq.pop();
		col_pq.push(col_top - p*n);
	}
	long long out = -1000000000000000000;
	for (int i = 0; i <= k; i++) {
		out = max(out, row_ans[i] + col_ans[k-i] - i*(k-i)*p);
	}
	cout << out;
	return 0;
}