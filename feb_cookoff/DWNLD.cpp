#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n, k;
		cin >> n >> k;
		vector <int> time(n);
		vector <int> rate(n);
		for(int i = 0; i < n; i++)
			cin >> time[i] >> rate[i];
		int time_sum = 0;
		int i = -1;
		while(i < n && time_sum + time[i+1] < k)
		{
			i++;
			time_sum += time[i];
		}
		i++;
		if(i < n)
		{
			int ans = (time_sum + time[i] - k) * rate[i];
			for(int j = i+1; j < n; j++)
				ans += time[j] * rate[j];
			cout << ans << '\n';
		}
		else
			cout << 0 << '\n';
	}
	return 0;
}