#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		string s;
		cin >> s;
		int max_cont = 0;
		int i = 0;
		while(i < n)
		{
			if(s[i] == '0')
				i++;
			else
			{
				int j = i;
				while(j < n && s[j] == '1')
					j++;
				max_cont = max(max_cont, j-i);
				i = j;
			}
		}
		int start = 0, end = 0;
		i = 0;
		while(i < n && s[i] == '1')
			i++;
		start = i;
		i = n-1;
		while(i >= 0 && s[i] == '1')
			i--;
		end = (n-1)-i;
		max_cont = max(max_cont, min(start + end, n));
		int total_children = 0;
		for(char c : s)
			if(c == '1')
				total_children++;
		cout << total_children - max_cont << '\n';
	}
	return 0;
}