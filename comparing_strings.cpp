#include <iostream>
#include <string>

using namespace std;

int main()
{
	string a, b;
	cin >> a >> b;
	if(a.length() != b.length())
		cout << "NO" << '\n';
	else
	{
		int l = 0, r = a.length()-1;
		while(a[l] == b[l])
			l++;
		while(a[r] == b[r])
			r--;
		if(a[l] == b[r] && a[r] == b[l])
		{
			l++;
			r--;
			bool same = true;
			while(l < r)
			{
				if(a[l] != b[l])
				{
					same = false;
					break;
				}
				l++;
			}
			cout << (same ? "YES" : "NO") << '\n';
		}
		else
			cout << "NO" << '\n';
	}
	return 0;
}