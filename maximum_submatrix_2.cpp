#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int v[5000][5000];
char str[5000];
int dps[5000];

int main() {
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 0; i < n; i++) {
		scanf("%s", str);
		for (int j = 0; j < m; j++) {
			v[i][j] = str[j] - '0';
		}
	}
	for (int i = 0; i < n; i++) {
		for (int j = 1; j < m; j++) {
			if (v[i][j] != 0)
				v[i][j] = 1 + v[i][j-1];
		}
	}
	int ans = 0;
	for (int j = 0; j < m; j++) {
		for (int i = 0; i < n; i++) {
			dps[i] = v[i][j];
		}
		sort(dps, dps+n);
		for (int i = n-1; i >= 0; i--) {
			ans = max(ans, (n-i) * (dps[i]));
		}
	}
	printf("%d", ans);
	return 0;
}