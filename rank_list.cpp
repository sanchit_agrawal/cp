#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

int main()
{
	int n, k;
	cin >> n >> k;
	vector <pair<int, int>> v(n);
	for(int i = 0; i < n; i++)
	{
		int p, t;
		cin >> p >> t;
		v[i].first = p;
		v[i].second = -t;
	}
	sort(v.begin(), v.end());
	reverse(v.begin(), v.end());
	// for(int i = 0; i < n; i++)
	// 	cout << v[i].first << ' ' << v[i].second << endl;
	int l = k-1, r = k-1;
	while(l > 0 && v[l-1].first == v[k-1].first && v[l-1].second == v[k-1].second)
		l--;
	while(r < n && v[r+1].first == v[k-1].first && v[r+1].second == v[k-1].second)
		r++;
	cout << r-l+1;
	return 0;
}