#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void dfs(int root, vector <vector<int>>& adjacency_list, vector <bool>& visited)
{
	visited[root] = true;
	for(int i : adjacency_list[root])
		if(!visited[i])
			dfs(i, adjacency_list, visited);
}

int main()
{
	int n;
	cin >> n;
	vector <pair<int, int>> pos(n);
	for(int i = 0; i < n; i++)
	{
		cin >> pos[i].first >> pos[i].second;
	}
	vector <vector <int>> adjacency_list(n);
	for(int i = 0; i < n; i++)
		for(int j = 0; j < i; j++)
			if(pos[i].first == pos[j].first || pos[i].second == pos[j].second)
			{
				adjacency_list[i].push_back(j);
				adjacency_list[j].push_back(i);
			}
	vector <bool> visited(n, false);
	int n_comp = 0;
	for(int i = 0; i < n; i++)
		if(!visited[i])
		{
			n_comp++;
			dfs(i, adjacency_list, visited);
		}
	cout << n_comp-1;
	return 0;
}