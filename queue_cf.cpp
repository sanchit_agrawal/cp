#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
using namespace std;

int main() {
	string s;
	cin >> s;
	int n = s.size();
	int prev_girl = n;
	for (int i = 0; i < n; i++) {
		if (s[i] == 'F') {
			prev_girl = i;
			break;
		}
	}
	if (prev_girl == n) {
		cout << 0;
	}
	else {
		int prev_waiting_time = 0;
		int prev_total_time = prev_girl;
		int n_girls = 0;
		// cout << prev_total_time << ' ' << prev_waiting_time << '\n';
		for (int i = prev_girl+1; i < n; i++) {
			if (s[i] == 'F') {
				n_girls++;
				int curr_waiting_time;
				int curr_total_time;
				int boys_between = i-prev_girl-1;
				if (boys_between == 0) {
					if (prev_total_time == 0) {
						curr_total_time = 0;
						curr_waiting_time = 0;
					} else {
						curr_total_time = prev_total_time + 1;
						curr_waiting_time = prev_waiting_time + 1;
					}
				} else {
					if (prev_waiting_time >= boys_between) {
						curr_total_time = prev_total_time + 1;
						curr_waiting_time = prev_waiting_time + 1 - boys_between;
					} else {
						curr_waiting_time = 0;
						curr_total_time = i - n_girls;
					}
				}
				prev_waiting_time = curr_waiting_time;
				prev_total_time = curr_total_time;
				prev_girl = i;
				// cout << prev_total_time << ' ' << prev_waiting_time << '\n';
			}
		}
		cout << prev_total_time;
	}
	return 0;
}