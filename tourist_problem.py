n = int(raw_input())
ls = map(int, raw_input().split())
s = 0
ls.sort()
sums = [0]*n
sums[0] = ls[0]
for i in range(1, n):
	sums[i] = sums[i-1] + ls[i]
for i in range(1, n):
	s += i*ls[i] - sums[i-1]
s *= 2
s += sum(ls)
temp = n
for i in range(n, 1, -1):
	if s % i == 0 and temp % i == 0:
		temp /= i
		s /= i
print s, temp