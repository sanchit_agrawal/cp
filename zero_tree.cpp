#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

pair <long long, long long> dfs(int root, int parent, vector <vector <int>>& adjacency_list, vector <int>& vals) {
	vector <long long> add_list;
	vector <long long> sub_list;
	for (int child : adjacency_list[root]) {
		if (child != parent) {
			auto p = dfs(child, root, adjacency_list, vals);
			add_list.push_back(p.first);
			sub_list.push_back(p.second);
		}
	}
	long long add = 0;
	long long sub = 0;
	for (long long a : add_list) {
		add = max(add, a);
	}
	for (long long s : sub_list) {
		sub = max(sub, s);
	}
	if (vals[root] + add - sub > 0) {
		sub += vals[root] + add - sub;
	} else {
		add -= vals[root] + add - sub;
	}
	return make_pair(add, sub);
}

int main() {
	int n;
	cin >> n;
	vector <vector <int>> adjacency_list(n);
	for (int i = 0; i < n-1; i++) {
		int u, v;
		cin >> u >> v;
		adjacency_list[u-1].push_back(v-1);
		adjacency_list[v-1].push_back(u-1);
	}
	vector <int> vals(n);
	for (int i = 0; i < n; i++) {
		cin >> vals[i];
	}
	auto p = dfs(0, -1, adjacency_list, vals);
	cout << p.first + p.second;
	return 0;
}