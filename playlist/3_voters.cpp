#include <iostream>
#include <set>
#include <vector>

using namespace std;

int main() {
	int n1, n2, n3;
	cin >> n1 >> n2 >> n3;
	set <int> s1, s2, s3, s;
	for (int i = 0; i < n1; i++) {
		int temp;
		cin >> temp;
		s1.insert(temp);
		s.insert(temp);
	}
	for (int i = 0; i < n2; i++) {
		int temp;
		cin >> temp;
		s2.insert(temp);
		s.insert(temp);
	}
	for (int i = 0; i < n3; i++) {
		int temp;
		cin >> temp;
		s3.insert(temp);
		s.insert(temp);
	}
	set<int>::iterator i;
	vector <int> v;
	for (i = s.begin(); i != s.end(); ++i) {
		int n = *i;
		int count = s1.count(n) + s2.count(n) + s3.count(n);
		if (count >= 2) {
			v.push_back(n);
		}
	}
	cout << v.size() << '\n';
	for (int i = 0; i < v.size(); i++) {
		cout << v[i] << '\n';
	}
	return 0;
}