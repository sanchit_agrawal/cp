#include <iostream>
#include <vector>

using namespace std;

int n, k;

void swap(int i, int j, vector <int>& v) {
	int temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

void next_perm(vector <int>& v) {
	int top_index = n - 1;
	while (v[top_index] < v[top_index - 1]) {
		top_index--;
	}
	if (top_index == 0) {
		return;
	}
	top_index--;
	int upper_bound_top_index = top_index + 1;
	while (upper_bound_top_index < n && v[upper_bound_top_index] > v[top_index]) {
		upper_bound_top_index++;
	}
	upper_bound_top_index--;
	swap(top_index, upper_bound_top_index, v);
	for (int i = top_index + 1; i <= (n + top_index)/2; i++) {
		swap(i, n - i + top_index, v);
	}
}

int main() {
	cin >> n >> k;
	vector <int> v(n);
	for (int t = 0; t < k; t++) {
		for (int i = 0; i < n; i++) {
			cin >> v[i];
		}
		next_perm(v);
		for (int i = 0; i < n; i++) {
			cout << v[i] << ' ';
		}
		cout << '\n';
	}
	return 0;
}