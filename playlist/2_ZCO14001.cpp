#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
	int n, h;
	cin >> n >> h;
	vector <int> boxes(n);
	for (int i = 0; i < n; i++) {
		cin >> boxes[i];
	}
	int curr_pos = 0;
	bool has_box = false;
	int cmd;
	while (true) {
		cin >> cmd;
		if (cmd == 0) {
			for (int i = 0; i < n; i++) {
				cout << boxes[i] << ' ';
			}
			return 0;
		} else if (cmd == 1) {
			if (curr_pos != 0) {
				curr_pos--;
			}
		} else if (cmd == 2) {
			if (curr_pos != n-1) {
				curr_pos++;
			}
		} else if (cmd == 3) {
			if (boxes[curr_pos] > 0 && !has_box) {
				has_box = true;
				boxes[curr_pos]--;
			}
		} else {
			if (has_box && boxes[curr_pos] < h) {
				has_box = false;
				boxes[curr_pos]++;
			}
		}
	}
}