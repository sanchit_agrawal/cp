#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int m;
	scanf("%d", &m);
	vector <int> v(1000002, 0);
	int l, w;
	for (int i = 0; i < m; i++) {
		scanf("%d%d", &l, &w);
		l = min(l, w);
		v[l]++;
	}
	int curr = 0;
	for (int i = 1000000; i >= 1; i--) {
		curr += v[i];
		v[i] = curr;
	}
	int maximum = 1;
	int sub = 0;
	while (maximum <= 1000000) {
		if (v[maximum] - sub < 1)
			break;
		else {
			int unique = v[maximum] - v[maximum+1];
			sub = max(0, sub - unique + 1);
		}
		maximum++;
	}
	printf("%d", maximum - 1);
	return 0;
}