n = int(raw_input())
s = ''
for i in range(n):
	s_ = raw_input()
	s += '\n'+s_
s = s.replace("'", ' ')
s = s.replace(".", ' ')
s = s.replace(",", ' ')
s = s.replace(";", ' ')
s = s.replace(":", ' ')
s = s.lower()
ls = set(s.split())
for l in sorted(ls):
	if l != '':
		print l
