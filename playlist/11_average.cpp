#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v_(n);
	for (int i = 0; i < n; i++) {
		cin >> v_[i];
	}
	sort(v_.begin(), v_.end());
	vector <pair <int, int> > v;
	int t = 1;
	int count = 0;
	while (t < n) {
		if (v_[t] == v_[t-1]) {
			count++;
		} else {
			v.push_back(make_pair(v_[t-1], count + 1));
			count = 0;
		}
		t++;
	}
	v.push_back(make_pair(v_[n-1], count + 1));
	n = v.size();
	vector <bool> is_avg(n, false);
	for (int i = 0; i < n; i++) {
		int times = v[i].second;
		if (times > 1)
			is_avg[i] = true;
	}
	int i = 0, j, k;
	while (i < n) {
		j = i + 1;
		k = i + 2;
		while (k < n && j < n) {
			if (v[i].first + v[k].first == 2*v[j].first) {
				is_avg[j] = true;
				j++;
				k++;
			} else if (v[i].first + v[k].first < 2*v[j].first) {
				k++;
			} else {
				j++;
			}
		}
		i++;
	}
	int ans = 0;
	for (int i = 0; i < n; i++) {
		if (is_avg[i]) {
			ans += v[i].second;
		}
	}
	cout << ans;
	return 0;
}