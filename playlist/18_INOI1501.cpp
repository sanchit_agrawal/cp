#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <long long> a(n);
	vector <long long> b(n);
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	for (int i = 0; i < n; i++) {
		cin >> b[i];
	}
	// Upto, but not including i.
	vector <long long> fbsum(n);
	fbsum[0] = 0;
	for (int i = 1; i < n; i++) {
		fbsum[i] = fbsum[i-1] + b[i-1];
	}
	// index of min fsum upto (and including) this point.
	vector <long long> minfind(n);
	minfind[0] = 0;
	for (int i = 1; i < n; i++) {
		if (fbsum[minfind[i-1]] + b[minfind[i-1]] - a[minfind[i-1]] < fbsum[i] + b[i] - a[i]) {
			minfind[i] = minfind[i-1];
		} else {
			minfind[i] = i;
		}
	}
	// Upto, but not including i.
	vector <long long> bbsum(n);
	bbsum[n-1] = 0;
	for (int i = n-2; i >= 0; i--) {
		bbsum[i] = bbsum[i+1] + b[i+1];
	}
	// index of max bsum upto (and including) this point.
	vector <long long> maxbind(n);
	maxbind[n-1] = n-1;
	for (int i = n-2; i >= 0; i--) {
		if (bbsum[maxbind[i+1]] + a[maxbind[i+1]] > bbsum[i] + a[i]) {
			maxbind[i] = maxbind[i+1];
		} else {
			maxbind[i] = i;
		}
	}
	long long ans = 0;
	for (int i = 0; i < n-1; i++) {
		ans = max(ans, fbsum[i] + a[i] + bbsum[maxbind[i+1]] + a[maxbind[i+1]]);
	}
	for (int i = 1; i < n; i++) {
		ans = max(ans, fbsum[i] + a[i] - fbsum[minfind[i-1]] - b[minfind[i-1]] + a[minfind[i-1]]);
	}
	for (int i = 0; i < n; i++) {
		ans = max(ans, a[i]);
	}
	cout << ans;
	return 0;
}