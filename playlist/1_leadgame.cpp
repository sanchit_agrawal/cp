#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int max_leader = 1;
	int max_lead = 0;
	int player_1_score = 0, player_2_score = 0;
	for (int i = 0; i < n; i++) {
		int player_1_score_t, player_2_score_t;
		cin >> player_1_score_t >> player_2_score_t;
		player_1_score += player_1_score_t;
		player_2_score += player_2_score_t;
		int curr_leader, curr_lead;
		if (player_1_score > player_2_score) {
			curr_leader = 1;
			curr_lead = player_1_score - player_2_score;
		} else {
			curr_leader = 2;
			curr_lead = player_2_score - player_1_score;
		}
		if (curr_lead > max_lead) {
			max_leader = curr_leader;
			max_lead = curr_lead;
		}
	}
	cout << max_leader << ' ' << max_lead;
	return 0;
}