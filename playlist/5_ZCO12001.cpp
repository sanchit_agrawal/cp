#include <iostream>
#include <stack>
#include <vector>

using namespace std;

int main() {
	int n;
	cin >> n;
	int max_nest_depth = 0;
	int max_nest_depth_index = 0;
	int max_between = 0;
	int max_between_index = 0;
	stack <int> st;
	for (int i = 0; i < n; i++) {
		int symbol;
		cin >> symbol;
		if (symbol == 1) {
			st.push(i);
			if (st.size() > max_nest_depth) {
				max_nest_depth = st.size();
				max_nest_depth_index = i;
			}
		} else {
			int between = i - st.top() + 1;
			if (between > max_between) {
				max_between = between;
				max_between_index = st.top();
			}
			st.pop();
		}
	}
	cout << max_nest_depth << ' ' << max_nest_depth_index+1 << ' ' << max_between << ' ' << max_between_index+1;
	return 0;
}