#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, k;
	cin >> n >> k;
	vector <int> a(n);
	vector <int> b(n);
	for (int i = 0; i < n; i++)
		cin >> a[i];
	for (int i = 0; i < n; i++)
		cin >> b[i];
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());
	int best;
	vector <int> a_cpy = a;
	vector <int> b_cpy = b;
	int i = 0, j = n-1, used = 0;
	while (used < k && a_cpy[i] < b_cpy[j]) {
		int temp = a_cpy[i];
		a_cpy[i] = b_cpy[j];
		b_cpy[j] = temp;
		i++;
		j--;
		used++;
	}
	int amax = 0, bmax = 0;
	for (int i = 0; i < n; i++) {
		if (a_cpy[i] > amax)
			amax = a_cpy[i];
		if (b_cpy[i] > bmax)
			bmax = b_cpy[i];
	}
	best = amax + bmax;
	a_cpy = b;
	b_cpy = a;
	i = 0, j = n-1, used = 0;
	while (used < k && a_cpy[i] < b_cpy[j]) {
		int temp = a_cpy[i];
		a_cpy[i] = b_cpy[j];
		b_cpy[j] = temp;
		i++;
		j--;
		used++;
	}
	amax = 0, bmax = 0;
	for (int i = 0; i < n; i++) {
		if (a_cpy[i] > amax)
			amax = a_cpy[i];
		if (b_cpy[i] > bmax)
			bmax = b_cpy[i];
	}
	best = min(best, amax + bmax);
	cout << best;
	return 0;
}