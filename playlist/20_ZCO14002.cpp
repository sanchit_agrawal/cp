#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	if (n < 3) {
		cout << *min_element(v.begin(), v.end());
	}
	vector <int> ans(n);
	ans[0] = v[0];
	ans[1] = v[1];
	ans[2] = v[2];
	for (int i = 3; i < n; i++) {
		ans[i] = v[i] + min(ans[i-1], min(ans[i-2], ans[i-3]));
	}
	int out = min(ans[n-1], min(ans[n-2], ans[n-3]));
	cout << out;
	return 0;
}