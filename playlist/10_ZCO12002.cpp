#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

int smaller(int needle, vector <int>& v, int l, int r) {
	if (l > r) return -1;
	else if (l == r) {
		if (v[l] > needle) return -1;
		else return l;
	}
	int mid = (l + r + 1)/2;
	if (v[mid] > needle) return smaller(needle, v, l, mid - 1);
	else return smaller(needle, v, mid, r);
}

int larger(int needle, vector <int>& v, int l, int r) {
	if (l > r) return -1;
	else if (l == r) {
		if (v[l] < needle) return -1;
		else return l;
	}
	int mid = (l + r)/2;
	if (v[mid] < needle) return larger(needle, v, mid + 1, r);
	else return larger(needle, v, l, mid);
}

int main() {
	int n, x, y;
	cin >> n >> x >> y;
	vector <pair<int, int> > event(n);
	for (int i = 0; i < n; i++) {
		cin >> event[i].first >> event[i].second;
	}
	vector <int> v(x);
	for (int i = 0; i < x; i++) {
		cin >> v[i];
	}
	vector <int> w(y);
	for (int i = 0; i < y; i++) {
		cin >> w[i];
	}
	sort(v.begin(), v.end());
	sort(w.begin(), w.end());
	int best = 2000000000;
	for (int i = 0; i < n; i++) {
		int l = smaller(event[i].first, v, 0, x-1);
		int r = larger(event[i].second, w, 0, y-1);
		if (l != -1 && r != -1) {
			int curr = w[r] - v[l] + 1;
			best = min(best, curr);
		}
	}
	cout << best;
	return 0;
}