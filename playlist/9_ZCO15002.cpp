#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, k;
	cin >> n >> k;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	sort(v.begin(), v.end());
	long long ans = 0;
	int i = 0, j = 0;
	while (j < n) {
		if (v[j] - v[i] < k) {
			j++;
		} else {
			ans += (n - j);
			i++;
		}
	}
	cout << ans;
	return 0;
}