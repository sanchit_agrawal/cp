n = int(raw_input())
ls = []
for i in range(n):
	a, b, c = map(int, raw_input().split())
	ls.append((b + c, a))
ls.sort(reverse = True)
curr_time = 0
ans = 0
for l in ls:
	curr = curr_time + l[0] + l[1]
	curr_time += l[1]
	ans = max(ans, curr)
print ans