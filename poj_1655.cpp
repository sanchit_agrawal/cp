#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int min_balance, min_balance_ind;

void func(int root, vector <vector <int> >& adjacency_list, vector <vector <int> >& adjacency_list2, vector <bool>& visited, vector <int>& size)
{
	visited[root] = true;
	for(int i = 0; i < adjacency_list[root].size(); i++)
		if(!visited[adjacency_list[root][i]])
		{
			adjacency_list2[root].push_back(adjacency_list[root][i]);
			func(adjacency_list[root][i], adjacency_list, adjacency_list2, visited, size);
		}
	size[root] = 1;
	int curr_balance = 0;
	for(int i = 0; i < adjacency_list2[root].size(); i++)
	{
		size[root] += size[adjacency_list2[root][i]];
		curr_balance = max(curr_balance, size[adjacency_list2[root][i]]);
	}
	curr_balance = max(curr_balance, (int)adjacency_list.size()-size[root]);
	if(min_balance == -1 || curr_balance < min_balance || (curr_balance == min_balance && root < min_balance_ind))
	{
		min_balance = curr_balance;
		min_balance_ind = root;
	}
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		min_balance = min_balance_ind = -1;
		int n;
		cin >> n;
		vector <vector <int> > adjacency_list(n);
		for(int i = 0; i < n-1; i++)
		{
			int u, v;
			cin >> u >> v;
			u--;
			v--;
			adjacency_list[u].push_back(v);
			adjacency_list[v].push_back(u);
		}
		// Make tree directed, and find size of each subtree.
		vector <vector <int> > adjacency_list2(n);
		vector <bool> visited(n, false);
		vector <int> size(n, 0);
		func(0, adjacency_list, adjacency_list2, visited, size);
		cout << min_balance_ind+1 << ' ' << min_balance << endl;
	}
	return 0;
}