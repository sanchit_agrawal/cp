#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	string s, t;
	cin >> s;
	cin >> t;
	int n = s.size();
	for(int i = 0; i < n; i++)
	{
		s[i] = (s[i]-'0')^(t[i]-'0')+'0';
	}
	cout << s << endl;
	return 0;
}