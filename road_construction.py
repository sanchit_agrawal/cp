n, m = map(int, raw_input().split())
can_be_center = [True]*n
for i in range(m):
	a, b = map(int, raw_input().split())
	can_be_center[a-1] = False
	can_be_center[b-1] = False
for i, b in enumerate(can_be_center):
	if b:
		break
print n-1
for j in range(1, i+1):
	print i+1, j
for j in range(i+2, n+1):
	print i+1, j