#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main() {
	int m, s;
	cin >> m >> s;
	if ((s == 0 && m > 1) || s > 9*m) {
		cout << -1 << ' ' << -1;
	} else if (s == 0 && m == 1){
		cout << '0' << ' ' << '0';
	} else {
		string out_max;
		while (s >= 9) {
			out_max += '9';
			s -= 9;
		}
		while (out_max.size() < m) {
			out_max += s + '0';
			s = 0;
		}
		string out_min = out_max;
		reverse(out_min.begin(), out_min.end());
		if (out_min[0] == '0') {
			out_min[0] = '1';
			int i = 1;
			while (out_min[i] == '0') {
				i++;
			}
			out_min[i]--;
		}
		cout << out_min << ' ' << out_max;
	}
	return 0;
}