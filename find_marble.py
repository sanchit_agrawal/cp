n, s, t = map(int, raw_input().split())
s -= 1
t -= 1
ps = map(lambda x: int(x)-1, raw_input().split())
visited = [False]*n
visited[s] = True
time = 0
while s != t and not visited[ps[s]]:
	s = ps[s]
	visited[s] = True
	time += 1
if s != t:
	print -1
else:
	print time