n, m, p, q = map(int, raw_input().split())
# dp[#footmen_remaining, #horsemen_remaining, is_last_placed_footmen]
dp = [[[None, None] for j in range(101)] for k in range(101)]
dp[0][0][True] = dp[0][0][False] = 1
def ans(tn, tm, footbool):
	if tn == tm == 0:
		return 1
	elif (footbool and tm == 0) or (not footbool and tn == 0):
		return 0
	elif dp[tn][tm][footbool] != None:
		return dp[tn][tm][footbool]
	else:
		ret = 0
		if footbool:
			for i in range(1, min(tm, q)+1):
				ret = (ret + ans(tn, tm-i, not footbool)) % 100000000
		else:
			for i in range(1, min(tn, p)+1):
				ret = (ret + ans(tn-i, tm, not footbool)) % 100000000
		dp[tn][tm][footbool] = ret
		return ret
print (ans(n, m, True) + ans(n, m, False)) % 100000000