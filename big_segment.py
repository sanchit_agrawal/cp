n = int(raw_input())
ls = []
for i in range(n):
	l, r = map(int, raw_input().split())
	ls.append((l, r))
minl = min([l[0] for l in ls])
maxr = max([l[1] for l in ls])
index = -2
for i, (l, r) in enumerate(ls):
	if l == minl and r == maxr:
		index = i
print index+1