#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		float f;
		cin >> v[i];
		cin >> f;
	}
	vector <int> dp(n);
	dp[0] = 1;
	for (int i = 1; i < n; i++) {
		int best = 0;
		for (int j = 0; j < i; j++) {
			if (v[j] <= v[i]) {
				best = max(best, dp[j]);
			}
		}
		dp[i] = best + 1;
	}
	int lis = 0;
	for (int i = 0; i < n; i++) {
		lis = max(lis, dp[i]);
	}
	cout << n - lis;
	return 0;
}