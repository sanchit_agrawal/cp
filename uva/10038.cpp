#include <iostream>
#include <vector>

#define abs(x) ((x) >= 0 ? (x) : -(x))

using namespace std;

int main()
{
	int n;
	while(cin >> n)
	{
		if(n == 1)
		{
			cin >> n;
			cout << "Jolly" << '\n';
			continue;
		}
		vector <bool> v(n-1, false);
		int prev;
		cin >> prev;
		for(int i = 1; i < n; i++)
		{
			int curr;
			cin >> curr;
			if(abs(curr - prev) - 1 >= 0 && abs(curr - prev) - 1 <= n-2)
				v[abs(curr - prev) - 1] = true;
			prev = curr;
		}
		bool total = true;
		for(int i = 0; i < n-1; i++)
			total = total && v[i];
		cout << (total ? "Jolly" : "Not jolly") << '\n';
	}
	return 0;
}