#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	string s;
	while(true)
	{
		cin >> s;
		if(s == "#")
			return 0;
		bool next = next_permutation(s.begin(), s.end());
		if(next)
			cout << s << '\n';
		else
			cout << "No Successor" << '\n';
	}
	return 0;
}