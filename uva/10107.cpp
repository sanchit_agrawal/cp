#include <iostream>
#include <map>

using namespace std;

int main()
{
	int n;
	map <int, int> left;
	map <int, int> right;
	int total_left = 0;
	int total_right = 0;

	while (cin >> n) {
		if (total_left == 0) {
			left[n]++;
			total_left++;
			cout << n << '\n';
		}
		else if (total_right == 0) {
			if (n >= left.begin()->first) {
				right[n]++;
			}
			else {
				right[left.begin()->first]++;
				left.erase(left.begin());
				left[n]++;
			}
			total_right++;
			cout << (left.begin()->first + right.begin()->first)/2 << '\n';
		}
		else {		
			auto left_end = left.end();
			left_end--;
			auto left_last = (left_end)->first;
			auto right_first = (right.begin())->first;
			if (n <= left_last) {
				left[n]++;
				total_left++;
			}
			else {
				right[n]++;
				total_right++;
			}
			if (total_left > total_right+1) {
				auto key = (left_end)->first;
				auto val = (left_end)->second;
				if (val == 1) {
					left.erase(left_end);
				}
				else {
					left[key]--;
				}
				right[key]++;	
				total_left--;
				total_right++;
			}
			else if (total_right > total_left) {
				auto key = (right.begin())->first;
				auto val = (right.begin())->second;
				if(val == 1) {
					right.erase(right.begin());
				}
				else {
					right[key]--;
				}
				left[key]++;
				total_right--;
				total_left++;
			}
			left_end = left.end();
			left_end--;
			if (total_left > total_right) {
				cout << (left_end)->first << '\n';
			}
			else {
				cout << ((left_end)->first + (right.begin())->first)/2 << '\n';
			}
		}
	}
	return 0;
}