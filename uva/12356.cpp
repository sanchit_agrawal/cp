#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int s, b;
	while(true)
	{
		cin >> s >> b;
		if(s == 0 && b == 0)
			return 0;
		vector <int> left(s+2);
		vector <int> right(s+2);
		for(int i = 0; i <= s+1; i++)
		{
			left[i] = i-1;
			right[i] = i+1;
		}
		for(int i = 0; i < b; i++)
		{
			int l, r;
			cin >> l >> r;
			right[left[l]] = right[r];
			left[right[r]] = left[l];
			if(left[l] == 0)
				cout << '*' << ' ';
			else
				cout << left[l] << ' ';
			if(right[r] == s+1)
				cout << '*' << '\n';
			else
				cout << right[r] << '\n';
		}
		cout << '-' << '\n';
	}
}