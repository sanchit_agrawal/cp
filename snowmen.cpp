#include <fstream>
#include <vector>

using namespace std;

struct snowman
{
	int prev;
	int top;
	int total;
};

int main()
{
	ifstream in("snowmen.in");
	ofstream out("snowmen.out");
	int n;
	in >> n;
	vector <snowman> v(n+1);
	int last = 0;
	v[0].prev = 0;
	v[0].top = 0;
	v[0].total = 0;
	for(int i = 0; i < n; i++)
	{
		int t, m;
		in >> t >> m;
		last++;
		if(m == 0)
		{
			v[last].prev = v[v[t].prev].prev;
			v[last].top = v[v[t].prev].top;
			v[last].total = v[v[t].prev].total;
		}
		else
		{
			v[last].prev = t;
			v[last].top = m;
			v[last].total = v[t].total + m;
		}
	}
	long long ans = 0;
	for(int i = 0; i <= n; i++)
		ans += v[i].total;
	out << ans;
}