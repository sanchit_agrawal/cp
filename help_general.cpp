#include <iostream>
#include <algorithm>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	cout << max(m*(n/4+1), n*(m/4+1));
	return 0;
}