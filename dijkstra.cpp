#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	vector <vector<pair<int, int>>> adjacency_list(n);
	for(int i = 0; i < m; i++)
	{
		int u, v, w;
		cin >> u >> v >> w;
		adjacency_list[u].push_back(make_pair(v, w));
		adjacency_list[v].push_back(make_pair(u, w));
	}
	vector <int> distance(n, 1000000000);
	distance[0] = 0;
	vector <bool> fixed(n, false);
	priority_queue <pair<int, int>> pq;
	for(int i = 0; i < n; i++)
		pq.push(make_pair(-distance[i], i));
	int count = 0;
	while(count != n)
	{
		auto top = pq.top();
		pq.pop();
		if(!fixed[top.second])
		{
			fixed[top.second] = true;
			count++;
			int curr_dist = -top.first;
			for(auto p : adjacency_list[top.second])
			{
				int new_dist = curr_dist + p.second;
				if(new_dist < distance[p.first])
				{
					distance[p.first] = new_dist;
					pq.push(make_pair(-new_dist, p.first));
				}
			}
		}
	}
	for(int i = 0; i < n; i++)
		cout << distance[i] << endl;
	return 0;
}