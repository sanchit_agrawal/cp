#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, t;
	cin >> n >> t;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int l = 0, r = -1;
	int best = 0;
	int curr_time = 0;
	int curr_books = 0;
	while(r < n-1)
	{
		if(curr_time + v[r+1] <= t)
		{
			curr_books++;
			curr_time += v[r+1];
			r++;
			best = max(best, curr_books);
		}
		else
		{
			l++;
			if(r < l)
			{
				r = l-1;
				curr_time = 0;
				curr_books = 0;
			}
			else
			{
				curr_time -= v[l-1];
				curr_books--;
			}
		}
	}
	cout << best;
	return 0;
}