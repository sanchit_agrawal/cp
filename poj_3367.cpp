#include <iostream>
#include <string>

using namespace std;

bool is_lower(char c)
{
	return c >= 'a' && c <= 'z';
}

bool is_upper(char c)
{
	return !is_lower(c);
}

int is_complex_string(string& s, int r)
{
	return is_upper(s[r]);
}

string get_prime_string(string& s, int r)
{
	if(!is_complex_string(s, r))
	{
		string s2 = "";
		s2 += s[r];
		// cout << "S2: " << s2 << endl;
		// cout << s[r] << endl;
		return s2;
	}
	else
	{
		bool is_s2_complex = is_complex_string(s, r-1);
		string s2_prime = get_prime_string(s, r-1);
		// cout << "s2_prime_size: " << s2_prime.size() << endl;
		bool is_s1_complex = is_complex_string(s, r-1-s2_prime.size()-is_s2_complex);
		string s1_prime = get_prime_string(s, r-1-s2_prime.size()-is_s2_complex);
		// cout << "S2bool: " << is_s2_complex << endl;
		// cout << "S1bool: " << is_s1_complex << endl;
		// cout << "S2prime: " << s2_prime << endl;
		// cout << "S1prime: " << s1_prime << endl;
		if(is_s2_complex)
		{
			string s2_op = "";
			s2_op += s[r-1];
			if(is_s1_complex)
			{
				string s1_op = "";
				s1_op += s[r-1-s2_prime.size()-1];
				return (s2_prime + s1_prime + s2_op + s1_op);
			}
			else
			{
				return (s2_prime + s2_op + s1_prime);	
			}
		}
		else
		{
			if(is_s1_complex)
			{
				string s1_op = "";
				s1_op += s[r-1-s2_prime.size()];
				return (s1_prime + s2_prime + s1_op);
			}
			else
			{
				return (s2_prime + s1_prime);
			}
		}
	}
}


int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		string s;
		cin >> s;
		cout << get_prime_string(s, s.size()-1) + s[s.size()-1] << '\n';
	}
	return 0;
}