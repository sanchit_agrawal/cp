#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> s(n);
	for(int i = 0; i < n; i++)
		cin >> s[i];
	int m;
	cin >> m;
	vector <int> t(m);
	for(int i = 0; i < m; i++)
		cin >> t[i];
	sort(s.begin(), s.end());
	sort(t.begin(), t.end());
	int besta = 3*n;
	int bestb = 3*m;
	// (i, n-1] is going to represent the moves that give 3 points.
	// (j, m-1] is going to represent the moves that give 3 points.
	int i = -1, j = -1;
	while(i < n - 1 && j < m - 1)
	{
		if(s[i+1] < t[j+1])
		{
			int d = s[i+1];
			while(i < n - 1 && s[i+1] == d)
				i++;
		}
		else if(t[j+1] < s[i+1])
		{
			int d = t[j+1];
			while(j < m - 1 && t[j+1] == d)
				j++;
		}
		else
		{
			int d = s[i+1];
			while(i < n - 1 && s[i+1] == d)
				i++;
			while(j < m - 1 && t[j+1] == d)
				j++;
		}
		int curr_a = 3*(n - i - 1) + 2*(i + 1);
		int curr_b = 3*(m - j - 1) + 2*(j + 1);
		if(curr_a - curr_b > besta - bestb || curr_a - curr_b == besta - bestb && curr_a > besta)
		{
			besta = curr_a;
			bestb = curr_b;
		}
	}
	if(2*n - 2*m > besta - bestb)
	{
		besta = 2*n;
		bestb = 2*m;
	}
	cout << besta << ':' << bestb;
	return 0;
}