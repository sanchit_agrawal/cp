n = int(raw_input())
ls = map(int, raw_input().split())
sums = [ls[0]]*n
for i in range(1, n):
	sums[i] = sums[i-1] + ls[i]
if sums[-1] % 3:
	print 0
else:
	ind1 = []
	ind2 = []
	for i, s in enumerate(sums):
		if s == sums[-1]/3:
			ind1.append(i)
		if s == 2*sums[-1]/3:
			ind2.append(i)
	if len(ind2) == 0 or len(ind1) == 0:
		print 0
	else:
		if ind2[-1] == n-1:
			ind2.pop()
		l, r = 0, 0
		ans = 0
		while l < len(ind1) and r < len(ind2):
			if ind2[r] <= ind1[l]:
				r += 1
			else:
				ans += (len(ind2)-r)
				l += 1
		print ans