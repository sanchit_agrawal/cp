#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	set <int> ls;
	vector <int> left_index(n);
	vector <int> right_index(n);
	left_index[0] = 0;
	for (int i = 1; i < n; i++) {
		int j;
		for (j = i; j > 0 && v[j-1] % v[i] == 0; j = left_index[j-1]);
		left_index[i] = j;
	}
	right_index[n-1] = n-1;
	for (int i = n-2; i >= 0; i--) {
		int j;
		for (j = i; j < n-1 && v[j+1] % v[i] == 0; j = right_index[j+1]);
		right_index[i] = j;
	}
	int max = 0;
	for (int i = 0; i < n; i++) {
		if (right_index[i] - left_index[i] + 1 > max) {
			max = right_index[i] - left_index[i] + 1;
		}
	}
	for (int i = 0; i < n; i++) {
		if (right_index[i] - left_index[i] + 1 == max) {
			ls.insert(left_index[i]);
		}
	}
	cout << ls.size() << ' ';
	cout << max - 1 << '\n';
	for (int l : ls) {
		cout << l + 1 << ' ';
	}
	return 0;
}