#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	vector <int> d(100001, 0);
	int ans = 0;
	for (int i = 0; i < n; i++) {
		int best = 0;
		for (int j = 2; j*j <= v[i]; j++) {
			if (v[i] % j == 0) {
				best = max(max(d[j], d[v[i]/j]), best);
			}
		}
		int j;
		for (j = 1; j*j <= v[i]; j++) {
			if (v[i] % j == 0) {
				d[j] = d[v[i]/j] = best + 1;
			}
		}
		ans = max(ans, best + 1);
	}
	cout << ans;
	return 0;
}