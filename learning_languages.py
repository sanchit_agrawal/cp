n, m = map(int, raw_input().split())
parent = list(range(m))
def find(x):
	curr = x
	while parent[curr] != curr:
		curr = parent[curr]
	final = curr
	curr = x
	while curr != final:
		curr_next = parent[curr]
		parent[curr] = final
		curr = curr_next
	return final
def union(t):
	ps = map(find, t)
	for p in ps[1:]:
		parent[p] = ps[0]
	return ps[0]
for i in range(n):
	ls = map(int, raw_input().split())[1:]
	ls = map(lambda x: x-1, ls)
	union(ls)
print len(set(parent[1:]))-1