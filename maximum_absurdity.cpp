#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, k;
	cin >> n >> k;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	long long curr = 0;
	for(int i = 0; i < k; i++)
		curr += v[i];
	int a = 0;
	long long best = curr;
	for(int i = 1; i < n-k+1; i++)
	{
		curr = curr - v[i-1] + v[i+k-1];
		if(i <= k-1 && i > n-2*k)
			continue;
		if(curr > best)
		{
			a = i;
			best = curr;
		}
	}
	int b = -1;
	best = 0;
	if(k-1 < a)
	{
		b = 0;
		curr = 0;
		for(int i = 0; i < k; i++)
			curr += v[i];
		best = curr;
		for(int i = 1; i+k-1 < a; i++)
		{
			curr = curr - v[i-1] + v[i+k-1];
			if(curr > best)
			{
				b = i;
				best = curr;
			}
		}
	}
	if(n-(a+k) >= k)
	{
		curr = 0;
		for(int i = a+k; i-(a+k)+1 <= k; i++)
			curr += v[i];
		if(curr > best)
		{
			b = a+k;
			best = curr;
		}
		for(int i = a+k+1; i+k-1 < n; i++)
		{
			curr = curr - v[i-1] + v[i+k-1];
			if(curr > best)
			{
				b = i;
				best = curr;
			}
		}
	}
	cout << min(a, b)+1 << ' ' << max(a, b)+1;
	return 0;
}