n = int(raw_input())
ls = map(int, raw_input().split())
maximum, minimum = max(ls), min(ls)
if maximum == minimum:
	print 0, len(ls)*(len(ls)-1)/2
else:
	n_max = sum((l == maximum for l in ls))
	n_min = sum((l == minimum for l in ls))
	print maximum-minimum, n_max*n_min