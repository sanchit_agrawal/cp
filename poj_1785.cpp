#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <cstdio>
#include <string>
#include <utility>

using namespace std;

void swap(vector<pair<string, int> >&v, int a, int b)
{
	pair<string, int> temp = v[a];
	v[a] = v[b];
	v[b] = temp;
}

void pretty_print(vector<pair<string, int> >& v, int l, int r)
{
	if(l > r)
		return;
	cout << "(";
	int root = 0;
	for(int i = l; i <= r; i++)
		if(v[i].second > v[root].second)
			root = i;
	swap(v, root, r);
	int front = l, end = r-1;
	while(front < end)
	{
		if(v[front].first < v[r].first)
			front++;
		else if(v[end].first > v[r].first)
			end--;
		else
			swap(v, front, end);
	}
	if(v[front].first < v[r].first)
	{
		pretty_print(v, l, front);
		cout << v[r].first << '/' << v[r].second;
		pretty_print(v, front+1, r-1);
	}
	else
	{
		pretty_print(v, l, front-1);
		cout << v[r].first << '/' << v[r].second;
		pretty_print(v, front, r-1);
	}
	cout << ")";
}

int main()
{
	while(true)
	{
		int n;
		cin >> n;
		if(n == 0)
			return 0;
		vector <pair<string, int> > v(n);
		for(int i = 0; i < n; i++)
		{
			string s;
			cin >> s;
			int slash_pos = s.find("/");
			v[i].first = s.substr(0, slash_pos);
			v[i].second = atoi(s.substr(slash_pos+1).c_str());
		}
		pretty_print(v, 0, n-1);
		cout << '\n';
	}
}