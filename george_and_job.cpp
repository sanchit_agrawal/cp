#include <cstdio>
#include <vector>
#include <list>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);
	vector <long long> sums(n);
	cin >> sums[0];
	for(int i = 1; i < n; i++)
	{
		cin >> sums[i];
		sums[i] += sums[i-1];
	}
	list <vector <long long>> dp;
	for(int i = 0; i < m; i++)
	{
		vector <long long> v(k+1, -1000000000000000);
		v[0] = 0;		
		dp.push_back(v);
	}
	for(int i = m; i <= n; i++)
	{
		vector <long long> v(k+1, 0);
		auto oldest = dp.front();
		auto last = dp.back();
		for(int j = 1; j <= k; j++)
			v[j] = max(oldest[j-1]+sums[i-1]-(i-m-1>=0?sums[i-m-1]:0),last[j]);
		dp.pop_front();
		dp.push_back(v);
	}
	cout << dp.back()[k];
}