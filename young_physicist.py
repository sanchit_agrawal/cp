n = int(raw_input())
x, y, z = 0, 0, 0
for i in range(n):
	a, b, c = map(int, raw_input().split())
	x += a
	y += b
	z += c
if (x, y, z) == (0, 0, 0):
	print 'YES'
else:
	print 'NO'