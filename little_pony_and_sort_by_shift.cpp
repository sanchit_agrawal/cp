#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	int prev = 0;
	int i = 0;
	while(i < n && v[i] >= prev)
	{
		prev = v[i];
		i++;
	}
	bool fine = true;
	int smallest_ind = i;
	for(i = i+1; i < n; i++)
		if(v[i] < v[i-1])
		{
			fine = false;
			break;
		}
	if(!fine)
		cout << -1;
	else if(smallest_ind == n)
		cout << 0;
	else if(v[n-1] > v[0])
	{
		cout << -1;
	}
	else
		cout << (n-smallest_ind);
	return 0;
}