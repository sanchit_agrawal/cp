#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int count = 0;
	if(n % 2)
	{
		count = 1 + (n-3)/2;
		cout << count << '\n';
		cout << 3 << ' ';
		for(int i = 0; i < count-1; i++)
			cout << 2 << ' ';
	}
	else
	{
		count = n/2;
		cout << count << '\n';
		for(int i = 0; i < count; i++)
			cout << 2 << ' ';
	}
	return 0;
}