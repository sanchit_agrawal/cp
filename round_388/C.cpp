#include <iostream>
#include <list>
#include <string>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	string s;
	cin >> s;
	list <int> ds;
	list <int> rs;
	for(int i = 0; i < s.size(); i++)
		if(s[i] == 'D')
			ds.push_back(i);
		else
			rs.push_back(i);
	list<int>::iterator i = ds.begin();
	list<int>::iterator j = rs.begin();
	while(ds.size() > 0 && rs.size() > 0)
	{
		if(i == ds.end())
		{
			while(ds.size() > 0 && j != rs.end())
			{
				ds.erase(ds.begin());
				++j;
			}
			j = rs.begin();
			i = ds.begin();
		}
		else if(j == rs.end())
		{
			while(rs.size() > 0 && i != ds.end())
			{
				rs.erase(rs.begin());
				++i;
			}
			i = ds.begin();
			j = rs.begin();
		}
		else if(*i < *j)
		{
			auto temp = j;
			++j;
			rs.erase(temp);
			++i;
		}
		else
		{
			auto temp = i;
			++i;
			ds.erase(temp);
			++j;
		}
	}
	if(ds.size() > 0)
		cout << 'D' << '\n';
	else
		cout << 'R' << '\n';
	return 0;
}
