#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int n, k;

bool ans(int st, int m, vector <vector <int>>& dp) {
	if (m < 0 || m > k || st < 0 || st > 2*k) {
		return false;
	}
	if (dp[st][m] != -1) {
		return dp[st][m];
	}
	bool out = ans(st+1, m-1, dp) || ans(st-1, m-1, dp);
	return dp[st][m] = out;
}

int main() {
	cin >> n >> k;
	vector <int> q(n);
	for (int i = 0; i < n; i++) {
		cin >> q[i];
		q[i]--;
	}
	vector <int> s(n);
	for (int i = 0; i < n; i++) {
		cin >> s[i];
		s[i]--;
	}
	vector <int> q_inv(n);
	for (int i = 0; i < n; i++){
		q_inv[q[i]] = i;
	}
	vector <vector <int>> perms(2*k+1, vector <int>(n));
	for (int i = 0; i < n; i++) {
		perms[k][i] = i;
	}
	for (int i = 1; i <= k; i++) {
		for (int j = 0; j < n; j++) {
			perms[k+i][j] = perms[k+i-1][q[j]];
			perms[k-i][j] = perms[k-i+1][q_inv[j]];
		}
	}
	vector <vector <int>> dp(2*k+1, vector <int>(k+1, -1));
	for (int i = 0; i < 2*k+1; i++) {
		if (perms[i] == s) {
			dp[i][0] = 1;
			for (int j = 1; j <= k; j++) {
				dp[i][j] = 0;
			}
		} else {
			dp[i][0] = 0;
		}
	}
	cout << (ans(k, k, dp) ? "YES" : "NO");
	return 0;
}