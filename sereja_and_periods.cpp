#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int rem_c[101][101];
int done[101][101];
int pre[101][26];

int main() {
	int b, d;
	cin >> b >> d;
	string a, c;
	cin >> a >> c;
	int n = a.size(), m = c.size();
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < 26; j++) {
			pre[i+1][j] = pre[i][j];
		}
		pre[i+1][a[i]-'a'] = i+1;
	}
	for (int j = 0; j <= m; j++) {
		rem_c[0][j] = j;
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			if (pre[i][c[j-1]-'a'] == 0) {
				rem_c[i][j] = j;
				done[i][j] = 0;
			} else {
				if (j == 1) {
					rem_c[i][j] = rem_c[pre[i][c[j-1]-'a']-1][m];
					done[i][j] = done[pre[i][c[j-1]-'a']-1][m] + 1;
				} else {
					rem_c[i][j] = rem_c[pre[i][c[j-1]-'a']-1][j-1];
					done[i][j] = done[pre[i][c[j-1]-'a']-1][j-1];
				}
			}
		}
	}
	int y = m;
	int bcpy = b;
	int completed = 0;
	while (bcpy > 0) {
		completed += done[n][y];
		y = rem_c[n][y];
		bcpy--;
	}
	cout << completed/d;
	return 0;
}