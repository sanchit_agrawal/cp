answers = {}
numbers = map(int, raw_input().split())
n = len(numbers)
goal = int(raw_input())

def subset_sum(numbers, goal, n):
	if n == -1:
		return goal
	elif (goal, n) in answers:
		return answers[(goal, n)]
	else:
		if numbers[n] > goal:
			answers[(goal, n)] = subset_sum(numbers, goal, n-1)
		else:
			answers[(goal, n)] = min(subset_sum(numbers, goal, n-1), subset_sum(numbers, goal-numbers[n], n-1))
		return answers[(goal, n)]

def get_set(numbers, goal, n, s):
	if n == -1:
		return s
	else:
		if numbers[n] > goal:
			return get_set(numbers, goal, n-1, s)
		else:
			if subset_sum(numbers, goal-numbers[n], n-1) < subset_sum(numbers, goal, n-1):
				s.add(numbers[n])
				return get_set(numbers, goal-numbers[n], n-1, s)
			else:
				return get_set(numbers, goal, n-1, s)


s = get_set(numbers, goal, n-1, set())
print s, goal-sum(s)
