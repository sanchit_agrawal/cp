#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	vector <vector <int>> count(n, vector <int>(n, 0));
	vector <vector <int>> adjacency_list(n);
	for (int i = 0; i < m; i++) {
		int u, v;
		cin >> u >> v;
		u--;
		v--;
		adjacency_list[u].push_back(v);
	}
	for (int i = 0; i < n; i++) {
		for (int j : adjacency_list[i]) {
			for (int k : adjacency_list[j]) {
				count[i][k]++;
			}
		}
	}
	long long ans = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i == j) {
				continue;
			} else {
				ans += (long long) count[i][j] * (count[i][j] - 1) / 2;
			}
		}
	}
	cout << ans;
	return 0;
}