#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int max_len = 4*n+1;
	vector <string> v;
	for(int i = 0; i <= n; i++)
	{
		string curr;
		int padding = (max_len-(4*i+1))/2;
		for(int j = 0; j < padding; j++)
			curr += ' ';
		for(int j = 0; j <= i; j++)
		{
			curr += (j+'0');
			curr += ' ';
		}
		for(int j = i-1; j >= 0; j--)
		{
			curr += (j+'0');
			if(j != 0)
				curr += ' ';
		}
		// for(int j = 0; j < padding-1; j++)
		// 	curr += ' ';
		v.push_back(curr);
	}
	for(int i = 0; i <= n; i++)
		cout << v[i] << '\n';
	for(int i = n-1; i >= 0; i--)
		cout << v[i] << '\n';
	return 0;
}