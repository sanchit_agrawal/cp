#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int between(int i, int j, vector <int>& sum)
{
	if(i == 0)
		return sum[j];
	else
		return sum[j]-sum[i-1];
}

int main()
{
	int n;
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	vector <int> sum(n);
	sum[0] = v[0];
	for(int i = 1; i < n; i++)
		sum[i] = sum[i-1] + v[i];
	if(sum[n-1] == n)
	{
		cout << n-1;
		return 0;
	}
	int best_ans = 0;
	for(int i = 0; i < n; i++)
	{
		// Flip all
		int curr_ans = (i+1-sum[i])+sum[n-1]-sum[i];
		for(int j = 1; j <= i; j++)
			curr_ans = max(curr_ans, sum[n-1]-sum[i]+sum[j-1]+(i-j+1-sum[i]+sum[j]));
		best_ans = max(curr_ans, best_ans);
	}
	cout << best_ans;
	return 0;
}