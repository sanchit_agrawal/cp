#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

int main() {
	string s;
	cin >> s;
	int n = s.size();
	vector <int> f(n+1);
	f[0] = f[1] = 0;
	for (int i = 2; i <= n; i++) {
		int j = f[i-1];
		while (true) {
			if (s[j] == s[i-1]) {
				f[i] = j + 1;
				break;
			} else {
				if (j == 0) {
					f[i] = 0;
					break;
				} else {
					j = f[j];
				}
			}
		}
	}
	if (f[n] == 0) {
		cout << "Just a legend";
	} else {
		bool found = false;
		for (int i = 0; i < n; i++) {
			if (f[i] == f[n]) {
				found = true;
				break;
			}
		}
		if (found) {
			cout << s.substr(0, f[n]);
		} else {
			if (f[f[n]] == 0) {
				cout << "Just a legend";
			} else {
				cout << s.substr(0, f[f[n]]);
			}
		}
	}
	return 0;
}