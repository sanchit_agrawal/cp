#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int n;

long long ans(int state, bool sub_bool, vector <long long>& add_dp, vector <long long>& sub_dp, vector <int>& a) {
	if (state <= 0 || state > n) {
		return 0;
	}
	if (sub_bool) {
		if (sub_dp[state] == -3) {
			return sub_dp[state] = -2;
		}
		if (sub_dp[state] != -1) {
			return sub_dp[state];
		}
		sub_dp[state] = -3;
		long long out = ans(state - a[state], !sub_bool, add_dp, sub_dp, a);
		if (out == -2) {
			return -2;
		}
		return sub_dp[state] = a[state] + out;
	} else {
		if (add_dp[state] == -3) {
			return add_dp[state] = -2;
		}
		if (add_dp[state] != -1) {
			return add_dp[state];
		}
		add_dp[state] = -3;
		long long out = ans(state + a[state], !sub_bool, add_dp, sub_dp, a);
		if (out == -2) {
			return -2;
		}
		return add_dp[state] = a[state] + out;
	}
}

int main() {
	cin >> n;
	vector <int> a(n+1);
	for (int i = 2; i <= n; i++) {
		cin >> a[i];
	}
	// -1 => unvisited
	// -2 => never ends
	// -3 => visited
	vector <long long> add_dp(n+1, -1);
	vector <long long> sub_dp(n+1, -1);
	add_dp[1] = -2;
	for (int i = 2; i <= n; i++) {
		long long out = ans(i, true, add_dp, sub_dp, a);
		if (out == -2) {
			cout << -1 << '\n';
		} else {
			cout << out + i - 1 << '\n';
		}
	}
	return 0;
}