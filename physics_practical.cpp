#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	ifstream in("input.txt");
	in >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		in >> v[i];
	sort(v.begin(), v.end());
	int l = 0, r = 0;
	int ans = n;
	for(int l = 0; l < n; l++)
	{
		while(r < n && v[r] <= 2*v[l])
			r++;
		ans = min(ans, n-(r-l));
	}
	ofstream out("output.txt");
	out << ans;
}