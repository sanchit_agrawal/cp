#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
	string s;
	cin >> s;
	int n = s.size();
	int odd_a = 0, even_a = 0, odd_b = 0, even_b = 0;
	long long odd = 0;
	long long even = 0;
	for (int i = 0; i < n; i++) {
		if (s[i] == 'a') {
			if (i % 2) {
				odd += odd_a;
				even += even_a;
				odd_a++;
			} else {
				odd += even_a;
				even += odd_a;
				even_a++;
			}
		} else {
			if (i % 2) {
				odd += odd_b;
				even += even_b;
				odd_b++;
			} else {
				odd += even_b;
				even += odd_b;
				even_b++;
			}
		}
		odd++;
	}
	cout << even << ' ' << odd;
	return 0;
}