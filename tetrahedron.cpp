#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	long long other = 0;
	long long d = 1;
	long long other_cpy;
	long long d_cpy;
	for(int i = 1; i <= n; i++)
	{
		d_cpy = (3*other) % 1000000007;
		other_cpy = (2*other + d) % 1000000007;
		d = d_cpy;
		other = other_cpy;
	}
	cout << d_cpy;
}