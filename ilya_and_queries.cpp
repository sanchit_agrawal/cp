#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
	string s;
	cin >> s;
	int n = s.length();
	// count[i] = number of j < i, such that s[j] = s[j+1]
	vector <int> count(n);
	count[0] = 0;
	for(int i = 1; i < n; i++)
		count[i] = count[i-1] + (s[i-1] == s[i] ? 1 : 0);
	int m;
	cin >> m;
	for(int i = 0; i < m; i++)
	{
		int l, r;
		cin >> l >> r;
		l--;
		r--;
		cout << count[r]-count[l] << '\n';
	}
	return 0;
}