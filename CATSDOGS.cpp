#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		long long c, d, l;
		cin >> c >> d >> l;
		if(l % 4)
			cout << "no" << '\n';
		else if(l <= 4*(c+d) && l >= 4*d+max((long long)0, 4*(c-2*d)))
			cout << "yes" << '\n';
		else
			cout << "no" << '\n';
	}
	return 0;
}