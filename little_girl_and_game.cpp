#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
	string s;
	cin >> s;
	int count[26] = {0};
	for(char c: s)
		count[c-'a']++;
	// initial_odd_nos + n_moves = final_odd_nos (mod 2)
	// n_moves = 1 - initial_odd_nos (mod 2)
	int initial_odd_nos = 0;
	for(int i = 0; i < 26; i++)
		if(count[i] % 2)
			initial_odd_nos++;
	if(initial_odd_nos == 0)
	{
		cout << "First";
		return 0;
	}
	int n_moves = (1 + initial_odd_nos) % 2;
	if(n_moves == 1)
		cout << "Second";
	else
		cout << "First";
	return 0;
}