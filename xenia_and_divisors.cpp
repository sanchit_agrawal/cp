#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	int count[8] = {0};
	for(int i = 0; i < n; i++)
	{
		int curr;
		cin >> curr;
		count[curr]++;
	}
	if(count[5] > 0 || count[7] > 0)
	{
		cout << -1;
	}
	else if(count[1] != count[2]+count[3])
	{
		cout << -1;
	}
	else if(count[4] > count[2])
	{
		cout << -1;
	}
	else if(count[6] != count[2]+count[3]-count[4])
	{
		cout << -1;
	}
	else
	{
		for(int i = 0; i < count[4]; i++)
			cout << 1 << ' ' << 2 << ' ' << 4 << '\n';
		for(int i = 0; i < count[2]-count[4]; i++)
			cout << 1 << ' ' << 2 << ' ' << 6 << '\n';
		for(int i = 0; i < count[3]; i++)
			cout << 1 << ' ' << 3 << ' ' << 6 << '\n';
	}

}