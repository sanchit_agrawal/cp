#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <vector <int> > v(n, vector <int>(n));
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			cin >> v[i][j];
	for(int i = 1; i < n; i++)
		for(int j = 1; j < n; j++)
			v[i][j] += v[i][j-1];
	int max_ans = 0;
	vector <vector <int> > prev(n, vector<int>(n, 0));
	vector <vector <int> > curr(n, vector<int>(n, 0));
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			for(int k = j; k < n; k++)
			{
				int curr_sum = v[i][k]-(j > 0 ? v[i][j-1]:0);
				int prev_sum = prev[j][k];
				if(prev_sum > 0)
					curr_sum += prev_sum;
				if(curr_sum > max_ans)
					max_ans = curr_sum;
				curr[j][k] = curr_sum;
			}
		}
		prev = curr;
	}
	cout << max_ans << endl;
	return 0;
}