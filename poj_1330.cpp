#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

pair <bool, bool> has_a_b(int root, int a, int b, vector <vector <int> >& adjacency_list)
{
	bool has_a = false;
	bool has_b = false;
	if(root == a)
		has_a = true;
	else if(root == b)
		has_b = true;
	for(int i = 0; i < adjacency_list[root].size(); i++)
	{
		pair <bool, bool> curr_answer = has_a_b(adjacency_list[root][i], a, b, adjacency_list);
		has_a = has_a || curr_answer.first;
		has_b = has_b || curr_answer.second;
	}
	return make_pair(has_a, has_b);
}

int lca(int root, int a, int b, vector <vector <int> >& adjacency_list)
{
	vector <pair <bool, bool> > answers(adjacency_list[root].size());
	for(int i = 0; i < adjacency_list[root].size(); i++)
		answers[i] = has_a_b(adjacency_list[root][i], a, b, adjacency_list);
	int which_descendant;
	bool a_b_separate = true;
	for(int i = 0; i < answers.size(); i++)
		if(answers[i].first && answers[i].second)
		{
			which_descendant = adjacency_list[root][i];
			a_b_separate = false;
			break;
		}
	if(a_b_separate)
		return root;
	return lca(which_descendant, a, b, adjacency_list);
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector <vector <int> > adjacency_list(n);
		vector <bool> is_root(n, true);
		for(int i = 0; i < n-1; i++)
		{
			int u, v;
			cin >> u >> v;
			adjacency_list[u-1].push_back(v-1);
			is_root[v-1] = false;
		}
		int root;
		for(int i = 0; i < n; i++)
			if(is_root[i] == true)
			{
				root = i;
				break;
			}
		int a, b;
		cin >> a >> b;
		a--;
		b--;
		cout << lca(root, a, b, adjacency_list)+1 << '\n';
	}
	return 0;
}