#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>
#include <map>
#include <utility>

using namespace std;

char s[100001];
char t[100001];
int a, b, k;

int ans(int n, int m, map <pair<int, int>, int>& rabbit)
{
	if(n == 0)
		return a*m;
	if(m == 0)
		return a*n;
	auto f = rabbit.find(make_pair(n, m));
	if(f != rabbit.end())
		return f->second;
	if(s[n] == t[m])
		return rabbit[make_pair(n, m)] = ans(n-1, m-1, rabbit);
	int ans1 = ans(n-1, m, rabbit);
	if(ans1 != -1)
	{
		ans1 += a;
		if(ans1 > k)
			ans1 = -1;
	}
	int ans2 = ans(n, m-1, rabbit);
	if(ans2 != -1)
	{
		ans2 += a;
		if(ans2 > k)
			ans2 = -1;
	}
	int ans3 = ans(n-1, m-1, rabbit);
	if(ans3 != -1)
	{
		ans3 += b;
		if(ans3 > k)
			ans3 = -1;
	}
	if(ans1 == -1 && ans2 == -1 && ans3 == -1)
		return rabbit[make_pair(n, m)] = -1;
	else if(ans1 == -1 && ans2 == -1)
		return rabbit[make_pair(n, m)] = ans3;
	else if(ans2 == -1 && ans3 == -1)
		return rabbit[make_pair(n, m)] = ans1;
	else if(ans1 == -1 && ans3 == -1)
		return rabbit[make_pair(n, m)] = ans2;
	else if(ans1 == -1)
		return rabbit[make_pair(n, m)] = min(ans2, ans3);
	else if(ans2 == -1)
		return rabbit[make_pair(n, m)] = min(ans1, ans3);
	else if(ans3 == -1)
		return rabbit[make_pair(n, m)] = min(ans2, ans1);
	else
		return rabbit[make_pair(n, m)] = min(ans1, min(ans2, ans3));
}

int main()
{
	int test;
	scanf("%d", &test);
	while(test--)
	{
		scanf("%s%s", s, t);
		int n = strlen(s), m = strlen(t);
		scanf("%d%d%d", &a, &b, &k);
		map <pair<int, int>, int> rabbit;
		int l = ans(n, m, rabbit);
		printf("%d\n", l);
	}
	return 0;
}