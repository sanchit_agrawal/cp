#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

vector <int> primes;

int binsearch(int req, int l, int r)
{
	if(l == r)
		return l;
	int mid = (l+r+1)/2;
	if(primes[mid] < req)
		return binsearch(req, mid, r);
	else
		return binsearch(req, l, mid-1);
}

int main()
{
	vector <bool> is_prime(1000005, true);
	is_prime[1] = false;
	for(int i = 3; i <= 1000000; i+=2)
		if(is_prime[i])
		{
			primes.push_back(i);
			for(int j = 3*i; j <= 1000000; j += 2*i)
				is_prime[j] = false;
		}

	// cout << "here1" << endl;

	while(true)
	{
		int n;
		scanf("%d", &n);
		if(n == 0)
			return 0;
		int r = binsearch(n, 0, primes.size()-1);
		// cout << "here2" << endl;
		while(primes[r] >= n/2)
		{
			if(is_prime[n-primes[r]])
			{
				printf("%d = %d + %d\n", n, n-primes[r], primes[r]);
				// cout << n << " = " << primes[l] << " + " << n-primes[l] << endl;
				break;
			}
			r--;
		}
	}
}