#include <iostream>
#include <vector>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	int x;
	cin >> x;
	int l = 0, r = 0;
	while (r != n) {
		if (v[r] < x) {
			int temp = v[l];
			v[l] = v[r];
			v[r] = temp;
			l++;
		}
		r++;
	}
	for (int i = 0; i < n; i++) {
		cout << v[i] << ' ';
	}
	return 0;
}