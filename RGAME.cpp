#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

#define mod(x) ((x) % 1000000007)

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector <long long> numbers(n+1);
		for(int i = 0; i <= n; i++)
			cin >> numbers[i];
		vector <long long> ans(n+1, 0);
		vector <long long> nways(n+1, 0);
		nways[0] = mod(2*numbers[0]);
		long long pow2 = 1;
		for(int i = 1; i <= n; i++)
		{
			pow2 = mod(pow2*2);
			nways[i] = mod(nways[i-1] + pow2*numbers[i]);
		}
		ans[0] = 0;
		for(int i = 1; i <= n; i++)
		{
			ans[i] = mod(2*ans[i-1] + numbers[i]*nways[i-1]);
		}
		cout << ans[n] << endl;
	}
	return 0;
}