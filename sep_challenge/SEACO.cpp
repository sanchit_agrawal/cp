#include <iostream>
#include <vector>

using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n, m;
		cin >> n >> m;
		vector <long long> nsum(n, 0);
		vector <long long> qsum(m+2, 0);
		vector <long long> ls(m);
		vector <long long> rs(m);
		for (int i = 0; i < m; i++) {
			int t, l, r;
			cin >> t >> l >> r;
			if (t == 1) {
				ls[i] = l;
				rs[i] = r;
			} else {
				ls[i] = -l;
				rs[i] = -r;
			}
			qsum[i]++;
			qsum[i+1]--;
		}
		long long prev = qsum[m];
		qsum[m] = 0;
		for (int i = m-1; i >= 0; i--) {
			long long new_prev = qsum[i];
			qsum[i] = qsum[i+1] - prev;
			prev = new_prev;
			if (ls[i] < 0) {
				qsum[-ls[i]-1] += qsum[i];
				if (-rs[i]-1 != i-1) {
					qsum[-rs[i]] -= qsum[i]; 
				} else {
					prev -= qsum[i];
				}
			}
		}
		for (int i = 0; i < m; i++) {
			qsum[i] = qsum[i] % 1000000007;
		}
		for (int i = 0; i < m; i++) {
			if (ls[i] < 0) {
				continue;
			}
			nsum[ls[i]-1] += qsum[i];
			if (rs[i]-1 != n-1) {
				nsum[rs[i]] -= qsum[i];
			}
		}
		for (int i = 1; i < n; i++) {
			nsum[i] = (nsum[i-1] + nsum[i]) % 1000000007;
		}
		for (int a : nsum) {
			cout << a << ' ';
		}
		cout << '\n';
	}
	return 0;
}