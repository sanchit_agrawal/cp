#include <iostream>

using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		int best_ind = -1;
		int best_val = -1;
		for (int i = 0; i < n; i++) {
			int a;
			cin >> a;
			if (best_ind == -1 || a < best_val) {
				best_ind = i+1;
				best_val = a;
			}
		}
		cout << best_ind << '\n';
	}
	return 0;
}