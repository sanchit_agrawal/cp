#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>
#include <set>

using namespace std;

void dfs0(int root, vector <vector<int>>& alist, vector <int>& comp, int c) {
	comp[root] = c;
	for (int next : alist[root]) {
		if (comp[next] == -1) {
			dfs0(next, alist, comp, c);
		}
	}
}

bool dfs1(int root, vector <set<int>>& alist, vector<int>& color, int c) {
	int rc = color[root];
	if (rc != -1 && rc != c) {
		return true;
	} else {
		if (rc == c) {
			return false;
		} else {
			color[root] = c;
			bool inconsistent = false;
			for (int next : alist[root]) {
				bool d = dfs1(next, alist, color, 1-c);
				inconsistent = inconsistent || d;
			}
			return inconsistent;
		}
	}
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n, q;
		cin >> n >> q;
		vector <vector<int>> alist0(n);
		vector <vector<int>> alist1(n);
		for (int i = 0; i < q; i++) {
			int u, v, val;
			cin >> u >> v >> val;
			u--;
			v--;
			if (val == 0) {
				if (u != v) {
					alist0[u].push_back(v);
					alist0[v].push_back(u);
				}
			} else {
				alist1[u].push_back(v);
				alist1[v].push_back(u);
			}
		}
		int cn = 0;
		vector <int> comp(n, -1);
		for (int i = 0; i < n; i++) {
			if (comp[i] == -1) {
				dfs0(i, alist0, comp, cn);
				cn++;
			}
		}
		vector <set<int>> clist(cn);
		for (int i = 0; i < n; i++) {
			for (int j : alist1[i]) {
				clist[comp[i]].insert(comp[j]);
				clist[comp[j]].insert(comp[i]);
			}
		}
		bool inconsistent = false;
		vector <int> color(cn, -1);
		for (int i = 0; i < cn; i++) {
			if (color[i] == -1) {
				bool d = dfs1(i, clist, color, 0);
				inconsistent = inconsistent || d;
			}
		}
		if (inconsistent) {
			cout << "no";
		} else {
			cout << "yes";
		}
		cout << '\n';
	}
	return 0;
}