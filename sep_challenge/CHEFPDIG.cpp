#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		string s;
		cin >> s;
		vector <int> count(10, 0);
		for (char c : s) {
			count[c-'0']++;
		}
		vector <bool> possible(100, false);
		for (int i = 0; i < 10; i++) {
			if (count[i] == 0) {
				continue;
			}
			if (count[i] > 1) {
				possible[10*i+i] = true;
			}
			for (int j = i+1; j < 10; j++) {
				if (count[j] == 0) {
					continue;
				}
				possible[10*i+j] = true;
				possible[10*j+i] = true;
			}
		}
		for (int i = 'A'; i <= 'Z'; i++) {
			if (possible[i]) {
				cout << (char) i;
			}
		}
		cout << '\n';
	}
	return 0;
}