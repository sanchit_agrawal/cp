#include <iostream>
#include <vector>

using namespace std;

long long mod(long long x) {
	if (x < 0) {
		x += 1000000007;
	}
	return x % 1000000007;
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n, m;
		cin >> n >> m;
		vector <long long> nsum(n, 0);
		vector <long long> qsum(m, 0);
		vector <long long> ls(m);
		vector <long long> rs(m);
		for (int i = 0; i < m; i++) {
			int t, l, r;
			cin >> t >> l >> r;
			if (t == 1) {
				ls[i] = l;
				rs[i] = r;
			} else {
				ls[i] = -l;
				rs[i] = -r;
			}
			qsum[i]++;
			if (i != 0) {
				qsum[i-1]--;
			}
		}
		// cout << "qsum: ";
		// for (int q : qsum) {
		// 	cout << q << ' ';
		// }
		// cout << '\n';
		for (int i = m-1; i >= 0; i--) {
			if (i != m-1)
				qsum[i] = mod(qsum[i+1] + qsum[i]);
			if (ls[i] < 0) {
				int l = -ls[i]-1;
				int r = -rs[i]-1;
				qsum[r] += qsum[i];
				if (l != 0) {
					qsum[l-1] -= qsum[i];
				}
			}
		}
		// cout << "qsum: ";
		// for (int q : qsum) {
		// 	cout << q << ' ';
		// }
		// cout << '\n';
		for (int i = 0; i < m; i++) {
			if (ls[i] < 0) {
				continue;
			}
			int l = ls[i]-1;
			int r = rs[i]-1;
			nsum[l] += qsum[i];
			if (r != n-1) {
				nsum[r+1] -= qsum[i];
			}
		}
		for (int i = 1; i < n; i++) {
			nsum[i] = mod(nsum[i-1] + nsum[i]);
		}
		for (int a : nsum) {
			cout << a << ' ';
		}
		cout << '\n';
	}
	return 0;
}