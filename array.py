n, k = map(int, raw_input().split())
ls = map(int, raw_input().split())
count = [0]*(10**5+1)
types = 0
for l in ls:
	if count[l] == 0:
		types += 1
	count[l] += 1
l, r = 0, n-1
if types < k:
	print -1, -1
else:
	while True:
		count[ls[l]] -= 1
		if count[ls[l]] == 0:
			types -= 1
			if types < k:
				types += 1
				count[ls[l]] += 1
				break
		l += 1
	while True:
		count[ls[r]] -= 1
		if count[ls[r]] == 0:
			count[ls[r]] += 1
			break
		r -= 1
	print l+1, r+1