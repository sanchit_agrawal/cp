#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int parent[100001];
bool invert[100001];

int find(int x)
{
	bool tot_invert = false;
	int p = x;
	while(parent[p] != 0)
	{
		tot_invert = (tot_invert != invert[p]);
		p = parent[p];
	}
	int t = x;
	while(t != p)
	{
		int next = parent[t];
		bool old_invert = invert[t];
		invert[t] = tot_invert;
		parent[t] = p;
		tot_invert = (tot_invert != old_invert);
		t = next;
	}
	return p;
}

void diff(int a, int b)
{
	int a_p = find(a);
	int b_p = find(b);
	parent[a_p] = b_p;
	invert[a_p] = (invert[a_p] == invert[b_p]);
}

int find_if_same(int a, int b)
{
	int a_p = find(a);
	int b_p = find(b);
	if(a_p != b_p)
		return -1;
	else
		return invert[a] == invert[b];
}

int main()
{
	int t;
	scanf("%d", &t);
	while(t--)
	{
		int n, m;
		scanf("%d %d", &n, &m);
		for(int i = 1; i <= n; i++)
		{
			parent[i] = 0;
			invert[i] = false;
		}
		while(m--)
		{
			char c[2];
			int a, b;
			scanf("%s%d%d", c, &a, &b);
			if(c[0] == 'D')
			{
				diff(a, b);
			}
			else
			{
				int ans = find_if_same(a, b);
				if(ans == -1)
					printf("Not sure yet.\n");
				else if(ans == 0)
					printf("In different gangs.\n");
				else
					printf("In the same gang.\n");
			}
		}
	}
	return 0;
}