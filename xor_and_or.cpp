#include <iostream>
#include <string>

using namespace std;

int main()
{
	string s;
	string t;
	cin >> s;
	cin >> t;
	if(s.size() != t.size())
	{
		cout << "NO";
		return 0;
	}
	bool a = true;
	for(char c : s)
		if(c == '1')
		{
			a = false;
			break;
		}
	bool b = true;
	for(char c : t)
		if(c == '1')
		{
			b = false;
			break;
		}
	if(a == b)
		cout << "YES";
	else
		cout << "NO";
	return 0;
}