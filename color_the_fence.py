n = int(raw_input())
ls = map(int, raw_input().split())
ls = [(l, -(i+1)) for i, l in enumerate(ls)]
ls.sort()
ls = [(l, -x) for l, x in ls]
# print ls
if ls[0][0] > n:
	print -1
else:
	ans = [ls[0][1]]*(n/ls[0][0])
	n -= (n/ls[0][0])*ls[0][0]
	# print n
	for i in range(len(ans)):
		final_cost = ls[0][0]
		for cost, dig in ls:
			if dig > ans[i] and cost-ls[0][0] <= n:
				final_cost = cost
				ans[i] = dig
				# print final_cost
		n -= (final_cost - ls[0][0])
	print ''.join(map(str, ans))