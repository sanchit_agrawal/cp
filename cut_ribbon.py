n, a, b, c = map(int, raw_input().split())
moves = sorted([a, b, c])
max_pieces = [0]*(n+1)
for i in range(1, n+1):
	if i < moves[0]:
		max_pieces[i] = float('-inf')
	elif i < moves[1]:
		max_pieces[i] = max_pieces[i-moves[0]] + 1
	elif i < moves[2]:
		max_pieces[i] = max(max_pieces[i-moves[0]], max_pieces[i-moves[1]])+1
	else:
		max_pieces[i] = max(max_pieces[i-moves[0]], max_pieces[i-moves[1]], max_pieces[i-moves[2]])+1
print max_pieces[n]