#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v_(n);
	for (int i = 0; i < n; i++) {
		cin >> v_[i];
	}
	set <int> s(v_.begin(), v_.end());
	vector <int> v(s.begin(), s.end());
	n = v.size();
	int max_until[2000005];
	int l = 0;
	for (int i = 0; i < n; i++) {
		while (l < v[i]) {
			max_until[++l] = v[i];
		}
		max_until[l] = v[i];
	}
	while (l < 2000005) {
		max_until[++l] = v[n-1];
	}
	int ans = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 2*v[i]-1; j < 2000005; j+= v[i]) {
			ans = max(ans, max_until[j] % v[i]);
		}
	}
	cout << ans;
	return 0;
}