#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int tree(int l, int size_log, int ind, vector <vector<int>>& ans)
{
	int size = 1 << size_log;
	if(size == 1)
		return ans[0][l];
	int ans1, ans2;
	if(ind > (l+ size/2 - 1))
	{
		ans1 = ans[size_log - 1][l];
		if(ans1 == -1)
			ans1 = tree(l, size_log - 1, ind, ans);
		ans2 = tree(l + size/2, size_log - 1, ind, ans);
	}
	else
	{
		ans1 = tree(l, size_log - 1, ind, ans);
		ans2 = ans[size_log - 1][l + size/2];
		if(ans2 == -1)
			ans2 = tree(l + size/2, size_log - 1, ind, ans);
	}
	int ans3 = (size_log % 2 ? ans1 | ans2 : ans1 ^ ans2);
	return ans[size_log][l] = ans3;
}

int main()
{
	int n, m;
	cin >> n >> m;
	int n_pow = 1 << n;
	vector <vector <int>> ans(n+1, vector <int>(n_pow, -1));
	for(int i = 0; i < n_pow; i++)
		cin >> ans[0][i];
	for(int i = 0; i < m; i++)
	{
		int p, b;
		cin >> p >> b;
		p--;
		ans[0][p] = b;
		cout << tree(0, n, p, ans) << '\n';
	}
	return 0;
}