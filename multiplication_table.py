n, m, k = map(int, raw_input().split())
l, r = 1, m*n
while l < r:
	mid = (l + r + 1)/2
	count = 0
	for i in range(1, n+1):
		count += min(m, (mid - 1)/i)
	if count < k:
		l = mid
	else:
		r = mid - 1
print l