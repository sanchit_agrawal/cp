#include <iostream>
#include <vector>

using namespace std;

int n, m;

void dfs_helper(int root, vector <vector <int> >& adjacency_list, vector <int>& temp, vector <bool>& visited)
{
	temp.push_back(root);
	visited[root] = true;
	for(int next : adjacency_list[root])
		if(!visited[next])
			dfs_helper(next, adjacency_list, temp, visited);
}


void dfs(vector <vector <int> >& adjacency_list, vector <vector <int> >& teams, vector <int>& singles)
{
	vector <bool> visited(n, false);
	for(int i = 0; i < n; i++)
		if(!visited[i])
		{
			vector <int> temp;
			dfs_helper(i, adjacency_list, temp, visited);
			if(temp.size() == 1)
				singles.push_back(i);
			else
				teams.push_back(temp);
		}
}

int main()
{
	cin >> n >> m;
	vector <vector <int> > adjacency_list(n);
	for(int i = 0; i < m; i++)
	{
		int a, b;
		cin >> a >> b;
		adjacency_list[a-1].push_back(b-1);
		adjacency_list[b-1].push_back(a-1);
	}
	vector <vector <int> > teams;
	vector <int> singles;
	dfs(adjacency_list, teams, singles);
	int greater_than_3_count = 0;
	int two_count = 0;
	int three_count = 0;
	int one_count = singles.size();
	for(auto& v : teams)
		if(v.size() > 3)
			greater_than_3_count++;
		else if(v.size() == 2)
			two_count++;
		else if(v.size() == 3)
			three_count++;
	if(greater_than_3_count)
		cout << -1;
	else if(one_count < two_count)
		cout << -1;
	else if((one_count - two_count) % 3 != 0)
		cout << -1;
	else
	{
		int singles_ind = 0;
		for(int i = 0; i < teams.size(); i++)
		{
			if(teams[i].size() == 3)
				cout << teams[i][0]+1 << ' ' << teams[i][1]+1 << ' ' << teams[i][2]+1 << '\n';
			else
			{
				cout << teams[i][0]+1 << ' ' << teams[i][1]+1 << ' ' << singles[singles_ind++]+1 << '\n';
			}
		}
		for(int i = singles_ind; i+2 < singles.size(); i+=3)
			cout << singles[i]+1 << ' ' << singles[i+1]+1 << ' ' << singles[i+2]+1 << '\n';
	}
	return 0;
}