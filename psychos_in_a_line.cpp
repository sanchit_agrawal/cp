#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v(n);
	vector <int> t(n, 0);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	stack <int> s;
	for (int i = 0; i < n; i++) {
		int m = 0;
		while (s.size() > 0 && v[s.top()] <= v[i]) {
			m = max(m, t[s.top()]);
			s.pop();
		}
		if (s.size() == 0) {
			t[i] = 0;
		} else {
			t[i] = m+1;
		}
		s.push(i);
	}
	int ans = 0;
	for (int d : t) {
		ans = max(ans, d);
	}
	cout << ans;
	return 0;
}