#include <cstdio>
#include <vector>
#include <string>
using namespace std;

#define abs(x) ((x) < 0 ? -(x) : (x))

int main()
{
	int n;
	scanf("%d", &n);
	int total = 0;
	int a, g;
	for(int i = 0; i < n; i++)
	{
		scanf("%d%d", &a, &g);
		total += a;
	}
	int give_away = 0;
	for(give_away = 0; give_away <= n; give_away++)
		if(abs(total - 1000*give_away) <= 500)
			break;
	if(give_away > n)
		printf("-1");
	else
	{
		char * s = new char[n+1];
		s[n] = 0;
		for(int i = 0; i < give_away; i++)
			s[i] = 'G';
		for(int i = give_away; i < n; i++)
			s[i] = 'A';
		printf("%s", s);	
		delete s;
	}
	return 0;
}