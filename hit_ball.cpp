#include <iostream>
#include <algorithm>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {
	double a, b, m, vx, vy, vz;
	cin >> a >> b >> m >> vx >> vy >> vz;
	double t = -m/vy;
	double z_bounces = vz*t/b;
	double z = fmod(z_bounces, 2);
	if (z >= 1) {
		z = b - (z - 1)*b;
	} else {
		z = b*z;
	}
	int x_dir = vx < 0 ? -1 : 1;
	double x_bounces = fabs(2*vx*t/a);
	double x = fmod(x_bounces, 4);
	if (x <= 1) {
		x = x*a/2;
	} else if (x <= 2) {
		x = (2-x)*a/2;
	} else if (x <= 3) {
		x = -(x-2)*a/2;
	} else {
		x = -(4-x)*a/2;
	}
	x = x * x_dir;
	x += a/2;
	cout << setprecision(8) << x << ' ' << z;
	return 0;
}