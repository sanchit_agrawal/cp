#include <iostream>
#include <list>
#include <utility>
#include <unordered_map>

using namespace std;

class LRUCache {
	private:
		unordered_map <int, list<pair<int,int>>::iterator> m;
		list <pair<int,int>> ls;
		int cap;
	public:
		LRUCache(int c) {
			cap = c;
		}
		void print() {
			for (auto c : ls) {
				cout << c.second << ' ';
			}
			cout << '\n';
		}
		int get(int key) {
			auto it = m.find(key);
			if (it == m.end()) {
				return -1;
			} else {
				auto itt = it->second;
				int val = itt->second;
				ls.erase(itt);
				ls.push_back(make_pair(key, val));
				m[key] = ls.end();
				--m[key];
				return val;
			}
		}
		int set(int key, int val) {
			auto it = m.find(key);
			if (it == m.end()) {
				if (ls.size() < cap) {
					ls.push_back(make_pair(key, val));
					m[key] = ls.end();
					--m[key];
				} else {
					int k = ls.front().first;
					ls.erase(ls.begin());
					m.erase(k);
					ls.push_back(make_pair(key, val));
					m[key] = ls.end();
					--m[key];
				}
			} else {
				auto itt = it->second;
				ls.erase(itt);
				ls.push_back(make_pair(key, val));
				m[key] = ls.end();
				--m[key];
			}
		}
};

int main() {
	int n;
	cin >> n;
	LRUCache cache(n);
	for (int i = 1; i <= n; i++) {
		cache.set(i, i);
	}
	cache.print();
	for (int i = n; i >= 1; i--) {
		cache.get(i);
	}
	cache.print();
	cache.set(n+1, n+1);
	cache.print();
	cache.set(n/2, -1);
	cache.print();
	return 0;
}