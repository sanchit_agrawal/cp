def modulus(x):
	if x >= 0:
		return x % 1000000007
	else:
		return (x + 1000000007*(x/1000000007 + 1)) % 1000000007

def matmul(a, b):
	c = [[0, 0], [0, 0]]
	c[0][0] = modulus(a[0][0]*b[0][0] + a[0][1]*b[1][0])
	c[0][1] = modulus(a[0][0]*b[0][1] + a[0][1]*b[1][1])
	c[1][0] = modulus(a[1][0]*b[0][0] + a[1][1]*b[1][0])
	c[1][1] = modulus(a[1][0]*b[0][1] + a[1][1]*b[1][1])
	return c

def matpow(a, b):
	if b == 1:
		return a
	ans = matpow(a, b/2)
	ans = matmul(ans, ans)
	if b % 2:
		ans = matmul(ans, a)
	return ans

x, y = map(int, raw_input().split())
n = int(raw_input())
if n == 1:
	print modulus(x)
elif n == 2:
	print modulus(y)
else:
	n = n-2
	ans = matpow([[0, 1], [-1, 1]], n)
	out = modulus(ans[1][0]*x + ans[1][1]*y)
	print out