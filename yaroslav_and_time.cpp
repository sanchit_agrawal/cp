#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int n, d;

bool check(int start_fuel, vector <int>& fuel, vector <int>& x, vector <int>& y) {
	vector <int> best(n, -1);
	best[0] = start_fuel;
	for (int i = 0; i < n; i++) {
		for (int u = 0; u < n; u++) {
			if (best[u] != -1) {
				for (int v = 0; v < n; v++) {
					if (u == v) {
						continue;
					}
					int dist = abs(x[u] - x[v]) + abs(y[u] - y[v]);
					if (best[u] >= d*dist) {
						best[v] = max(best[v], best[u] - d*dist + fuel[v]);
					}
				}
			}
		}
	}
	return best[n-1] > -1;
}

int main() {
	cin >> n >> d;
	vector <int> fuel(n, 0);
	for (int i = 1; i < n-1; i++) {
		cin >> fuel[i];
	}
	vector <int> x(n);
	vector <int> y(n);
	for (int i = 0; i < n; i++) {
		cin >> x[i] >> y[i];
	}
	int l = 0, r = 400*d;
	while (l < r) {
		int mid = (l + r)/2;
		if (check(mid, fuel, x, y)) {
			r = mid;
		} else {
			l = mid + 1;
		}
	}
	cout << l << '\n';
	return 0;
}