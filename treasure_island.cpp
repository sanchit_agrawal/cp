#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	
	vector <string> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	
	int k;
	cin >> k;
	
	vector <char> dir(k);
	vector <int> len(k);
	for (int i = 0; i < k; i++) {
		cin >> dir[i] >> len[i];
	}
	
	vector <vector <int>> n_max(n, vector <int>(m));
	vector <vector <int>> e_max(n, vector <int>(m));
	vector <vector <int>> w_max(n, vector <int>(m));
	vector <vector <int>> s_max(n, vector <int>(m));
	
	for (int i = 0; i < n; i++) {
		w_max[i][0] = 0;
		e_max[i][m-1] = 0;
	}
	
	for (int i = 0; i < m; i++) {
		n_max[0][i] = 0;
		s_max[n-1][i] = 0;
	}
	
	for (int i = 1; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (v[i][j] != '#') {
				if (v[i-1][j] == '#') {
					n_max[i][j] = 0;
				} else {
					n_max[i][j] = 1 + n_max[i-1][j];
				}
			}
		}
	}

	for (int i = n-2; i >= 0; i--) {
		for (int j = 0; j < m; j++) {
			if (v[i][j] != '#') {
				if (v[i+1][j] == '#') {
					s_max[i][j] = 0;
				} else {
					s_max[i][j] = 1 + s_max[i+1][j];
				}
			}
		}
	}

	for (int i = 0; i < n; i++) {
		for (int j = 1; j < m; j++) {
			if (v[i][j] != '#') {
				if (v[i][j-1] == '#') {
					w_max[i][j] = 0;
				} else {
					w_max[i][j] = 1 + w_max[i][j-1];
				}
			}
		}
	}

	for (int i = 0; i < n; i++) {
		for (int j = m-2; j >= 0; j--) {
			if (v[i][j] != '#') {
				if (v[i][j+1] == '#') {
					e_max[i][j] = 0;
				} else {
					e_max[i][j] = 1 + e_max[i][j+1];
				}
			}
		}
	}

	vector <char> outs;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (v[i][j] >= 'A' && v[i][j] <= 'Z') {
				bool possible = true;
				int x = i, y = j;
				for (int l = 0; l < k; l++) {
					// cout << "x, y: " << x << ' ' << y << endl;
					if (dir[l] == 'N') {
						if (n_max[x][y] >= len[l]) {
							x -= len[l];
						} else {
							// cout << "At " << v[i][j] << ": " << x << ", " << y << ". Cannot go " << dir[l] << " " << len[l] << "steps.\n"; 
							possible = false;
							break;
						}
					} else if (dir[l] == 'S') {
						if (s_max[x][y] >= len[l]) {
							x += len[l];
						} else {
							// cout << "At " << v[i][j] << ": " << x << ", " << y << ". Cannot go " << dir[l] << " " << len[l] << "steps.\n"; 
							possible = false;
							break;
						}
					} else if (dir[l] == 'E') {
						if (e_max[x][y] >= len[l]) {
							y += len[l];
						} else {
							// cout << "At " << v[i][j] << ": " << x << ", " << y << ". Cannot go " << dir[l] << " " << len[l] << "steps.\n"; 
							possible = false;
							break;
						}
					} else {
						if (w_max[x][y] >= len[l]) {
							y -= len[l];
						} else {
							// cout << "At " << v[i][j] << ": " << x << ", " << y << ". Cannot go " << dir[l] << " " << len[l] << "steps.\n"; 
							possible = false;
							break;
						}
					}
				}
				if (possible) {
					outs.push_back(v[i][j]);
				}
			}
		}
	}

	if (outs.size()) {
		sort(outs.begin(), outs.end());
		for (int i = 0; i < outs.size(); i++) {
			cout << outs[i];
		}
	} else {
		cout << "no solution";
	}
	return 0;
}