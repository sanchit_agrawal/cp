#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector <vector<int> > v(n);
	for(int i = 0; i < n; i++)
		for(int j = 0; j <= i; j++)
		{
			int temp;
			cin >> temp;
			v[i].push_back(temp);
		}
	for(int i = n-2; i >= 0; i--)
	{
		for(int j = 0; j <= i; j++)
			v[i][j] += max(v[i+1][j], v[i+1][j+1]);
	}
	cout << v[0][0] << endl;
	return 0;
}