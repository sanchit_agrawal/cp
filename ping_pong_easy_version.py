n = int(raw_input())
ls = []
adjacency_list = []

def dfs(root, target, visited):
	if root == target:
		return True
	visited[root] = True
	for next_node in adjacency_list[root]:
		if not visited[next_node]:
			if dfs(next_node, target, visited):
				return True
	return False

for i in range(n):
	q, x, y = map(int, raw_input().split())
	if q == 1:
		ls.append((x, y))
		adjacency_list.append([])
		for j in range(len(ls)-1):
			if x < ls[j][0] < y or x < ls[j][1] < y:
				adjacency_list[j].append(len(ls)-1)
			if ls[j][0] < x < ls[j][1] or ls[j][0] < y < ls[j][1]:
				adjacency_list[len(ls)-1].append(j)
	else:
		visited = [False]*len(ls)
		print 'YES' if dfs(x-1, y-1, visited) else 'NO'