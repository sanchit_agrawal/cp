#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

bool can_be_zero(int a, int b, int c, int d)
{
	if(d == 0)
		return true;
	else if(c == 0)
		return true;
	else if(c+d == 0)
		return true;
	else if(b == 0)
		return true;
	else if(b+d == 0)
		return true;
	else if(b+c == 0)
		return true;
	else if(b+c+d == 0)
		return true;
	else if(a == 0)
		return true;
	else if(a+d == 0)
		return true;
	else if(a+c == 0)
		return true;
	else if(a+c+d == 0)
		return true;
	else if(a+b == 0)
		return true;
	else if(a+b+d == 0)
		return true;
	else if(a+b+c == 0)
		return true;
	else if(a+b+c+d == 0)
		return true;
	return false;
}

int main()
{
	int t;
	scanf("%d", &t);
	while(t--)
	{
		int a, b, c, d;
		scanf("%d%d%d%d", &a, &b, &c, &d);
		printf("%s\n", can_be_zero(a, b, c, d) ? "Yes":"No");
	}
	return 0;
}