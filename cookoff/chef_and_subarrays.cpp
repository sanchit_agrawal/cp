#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int t;
	scanf("%d", &t);
	int v[100000];
	int v1[100000];
	int v2[100000];
	while(t--)
	{
		int n, k;
		scanf("%d%d", &n, &k);
		for(int i = 0; i < n; i++)
			scanf("%d", v+i);
		for(int i = 0; i < n; i++)
			v1[i] = v[i];
		long long count = 0;
		for(int i = 0; i < n; i++)
			if(v[i] >= k)
				count++;
		for(int len = 2; len <= n; len++)
		{
			for(int i = 0; i+len-1 <= n-1; i++)
			{
				v2[i] = v1[i] | v[i+len-1];
				if(v2[i] >= k)
					count++;
			}
			for(int i = 0; i < n; i++)
				v1[i] = v2[i];
		}
		printf("%lld\n", count);
	}
	return 0;
}