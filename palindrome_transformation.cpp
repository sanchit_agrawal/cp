#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int dist(char c, int d) {
	return min(abs(c-d), 26-abs(c-d));
}

int main() {
	int n, p;
	cin >> n >> p;
	string s;
	cin >> s;
	int count = 0;
	for (int i = 0; i < n; i++) {
		count += dist(s[i], s[n-i-1]);
	}
	count /= 2;
	bool need_left = false, need_right = false;
	if (p <= (n-1)/2) {
		for (int i = 0; i < p; i++) {
			if (s[i] != s[n-i-1]) {
				need_left = true;
			}
		}
		for (int i = p+1; i <= (n-1)/2; i++) {
			if (s[i] != s[n-i-1]) {
				need_right = true;
			}
		}
		if (need_left && need_right) {
			count += min(2*p + (n-2)/2-p, 2*((n-2)/2-p) + p);
		} else if (need_left) {
			count += p;
		} else if (need_right) {
			count += (n-2)/2-p;
		}
	} else {
		for (int i = (n-1)/2; i < p; i++) {
			if (s[i] != s[n-i-1]) {
				need_left = true;
			}
		}
		for (int i = p+1; i < n; i++) {
			if (s[i] != s[n-i-1]) {
				need_right = true;
			}
		}
		if (need_left && need_right) {
			count += min(2*(n-1-p) + p - (n+1)/2, 2*(p-(n+1)/2) + n-1 - p);
		} else if (need_left) {
			count += p-(n+1)/2;
		} else if (need_right) {
			count += n-1-p;
		}
	}
	cout << count;
	return 0;
}