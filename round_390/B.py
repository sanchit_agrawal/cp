ls = []
for i in range(4):
	s = raw_input()
	ls.append(s)
def is_winning(st):
	return (st == 'x.x' or st == '.xx' or st == 'xx.')
done = False
for i in range(4):
	done = done or is_winning(ls[i][:3]) or is_winning(ls[i][1:])
	done = done or is_winning(ls[0][i]+ls[1][i]+ls[2][i]) or is_winning(ls[1][i]+ls[2][i]+ls[3][i])
done = done or is_winning(ls[0][0]+ls[1][1]+ls[2][2]) or is_winning(ls[1][1]+ls[2][2]+ls[3][3])
done = done or is_winning(ls[0][3]+ls[1][2]+ls[2][1]) or is_winning(ls[1][2]+ls[2][1]+ls[3][0])
done = done or is_winning(ls[1][0]+ls[2][1]+ls[3][2]) or is_winning(ls[0][1]+ls[1][2]+ls[2][3])
done = done or is_winning(ls[1][3]+ls[2][2]+ls[3][1]) or is_winning(ls[0][2]+ls[1][1]+ls[2][0])
print ('YES' if done else 'NO')