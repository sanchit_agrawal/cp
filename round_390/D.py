n, k = map(int, raw_input().split())
coupon = [None]*n
ls = set()
for i in range(n):
	l, r = map(int, raw_input().split())
	coupon[i] = (l, r)
	ls.add(l)
coupon = [(coupon[i], i) for i in range(len(coupon))]
coupon.sort()
print coupon
ls = sorted(ls)
def contains(c, x):
	return c[0] <= x <= c[1]
l, r = 0, 0
best_ans = 0
best_l = 0
best_r = 0
for smallest in ls:
	while not contains(coupon[l][0], smallest):
		l += 1
	r = max(l, r)
	while r < n and contains(coupon[r][0], smallest):
		r += 1
	r -= 1
	if r-k+1 < l:
		continue
	else:
		first = coupon[r-k+1][0]
		curr_ans = first[1] - smallest
		if curr_ans > best_ans:
			best_l = l
			best_r = r
print best_ans
for i in range(best_l, best_r+1):
	print coupon[i][1],
