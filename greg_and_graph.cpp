#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <vector <int>> adj_mat(n, vector<int>(n));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cin >> adj_mat[i][j];
		}
	}
	vector <int> remove(n);
	for(int i = 0; i < n; i++) {
		cin >> remove[i];
		remove[i]--;
	}
	vector <long long> ans(n, 0);
	vector <bool> on(n, false);
	for (int k = n-1; k >= 0; k--) {
		on[remove[k]] = true;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				adj_mat[i][j] = min(adj_mat[i][j], adj_mat[i][remove[k]] + adj_mat[remove[k]][j]);
				if (on[i] && on[j]) {
					ans[k] += adj_mat[i][j];
				}
			}
		}
	}
	for (int i = 0; i < n; i++) {
		cout << ans[i] << ' ';
	}
	return 0;
}