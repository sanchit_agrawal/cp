#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;

int find(int a, vector <int>& parent)
{
	int curr = a;
	while(parent[curr] != -1)
		curr = parent[curr];
	int temp = a;
	while(temp != curr)
	{
		int next = parent[temp];
		parent[temp] = curr;
		temp = next;
	}
	return curr;
}

void join(int a, int b, vector <int>& parent)
{
	int a_ = find(a, parent);
	int b_ = find(b, parent);
	parent[a_] = b_;
}

int main()
{
	int n;
	while(cin >> n)
	{
		vector <int> parent(n, -1);
		vector <vector <int> > adjacency_matrix(n, vector <int>(n));
		vector <pair<int, pair<int, int> > > edges;
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
			{
				cin >> adjacency_matrix[i][j];
				if(i < j)
					edges.push_back(make_pair(adjacency_matrix[i][j], make_pair(i, j)));
			}
		sort(edges.begin(), edges.end());
		int cost = 0;
		int edge_count = 0;
		for(int i = 0; edge_count < n-1; i++)
		{
			int w = edges[i].first;
			int u = (edges[i].second).first;
			int v = (edges[i].second).second;
			if(find(u, parent) == find(v, parent))
				continue;
			else
			{
				join(u, v, parent);
				cost += w;
				edge_count++;
			}
		}
		cout << cost << endl;
	}
	return 0;
}