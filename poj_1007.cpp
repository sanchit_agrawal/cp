#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <utility>

using namespace std;

int inv(string& s, int l, int r)
{
	if(l == r)
		return 0;
	int mid = (l+r)/2;
	int ans = inv(s, l, mid) + inv(s, mid+1, r);
	string s2 = "";
	for(int i = l, j = mid+1; i <= mid || j <= r;)
	{
		if(i > mid)
		{
			s2 += s[j++];
		}
		else if(j > r)
		{
			s2 += s[i++];
		}
		else if(s[i] <= s[j])
		{
			s2 += s[i++];
		}
		else
		{
			ans += (mid-i+1);
			s2 += s[j++];
		}
	}
	for(int i = l; i <= r; i++)
		s[i] = s2[i-l];
	return ans;
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector <pair<int, string> > v(m);
	for(int i = 0; i < m; i++)
	{
		cin >> v[i].second;
		string scpy = v[i].second;
		v[i].first = inv(scpy, 0, n-1);
	}
	stable_sort(v.begin(), v.end());
	for(int i = 0; i < m; i++)
	{
		// cout << v[i].first << ' ';
		cout << v[i].second << endl;
	}
	return 0;
}