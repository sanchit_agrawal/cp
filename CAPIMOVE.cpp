#include <iostream>
#include <map>
#include <utility>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector <int> population(n);
		for(int i = 0; i < n; i++)
			cin >> population[i];
		vector <vector<int> > adjacency_list(n);
		for(int i = 0; i < n-1; i++)
		{
			int u, v;
			cin >> u >> v;
			adjacency_list[u-1].push_back(v-1);
			adjacency_list[v-1].push_back(u-1);
		}
		map <int, int> m;
		for(int i = 0; i < n; i++)
			m[-population[i]] = i;
		for(int i = 0; i < n; i++)
		{
			m.erase(-population[i]);
			for(int neighbour : adjacency_list[i])
				m.erase(-population[neighbour]);
			if(m.begin() != m.end())
				cout << m.begin()->second+1 << ' ';
			else
				cout << 0 << ' ';
			m[-population[i]] = i;
			for(int neighbour : adjacency_list[i])
				m[-population[neighbour]] = neighbour;	
		}
		cout << '\n';
	}
	return 0;
}