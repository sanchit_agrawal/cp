#include <iostream>
#include <vector>

using namespace std;

vector <long long> v(101, -1);

long long f(int n)
{
	if(v[n] != -1)
		return v[n];
	long long ans = 0;
	for(int i = 0; i <= n-1; i++)
		ans += f(i) * f(n-1-i);
	return v[n] = ans;
}

int main()
{
	v[0] = 1;
	v[1] = 1;
	int n;
	while(true)
	{
		cin >> n;
		if(n == -1)
			return 0;
		cout << f(n) << endl;
	}
}