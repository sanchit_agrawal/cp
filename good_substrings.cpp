#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector <bool> valid(26);
string s;
int k;

struct node {
	node * next[26];
};

void insert(string& t, node * head, int l, int r) {
	if (l > r) {
		return;
	}
	if (!head->next[t[l]-'a']) {
		head->next[t[l]-'a'] = new node();
	}
	insert(t, head->next[t[l]-'a'], l+1, r);
}

int dfs(node * root, int bad_count = 0) {
	if (!root || bad_count > k) {
		return 0;
	}
	int ans = 1;
	for (int i = 0; i < 26; i++) {
		if (root->next[i]) {
			ans += dfs(root->next[i], bad_count + (!valid[i]));
		}
	}
	return ans;
}

int main() {
	cin >> s;
	for (int i = 0; i < 26; i++) {
		char c;
		cin >> c;
		valid[i] = c - '0';
	}
	cin >> k;
	int n = s.size();
	node * root = new node();
	for (int i = 0; i < n; i++) {
		insert(s, root, i, n-1);
	}
	cout << dfs(root, 0)-1;
	return 0;
}