#include <iostream>
#include <vector>
#include <stack>
#include <utility>

using namespace std;

int main()
{
	int n, m;
	cin >> m;
	vector <int> u(m);
	for(int i = 0; i < m; i++)
		cin >> u[i];
	cin >> n;
	vector <int> v(n);
	for(int i = 0; i < n; i++)
		cin >> v[i];
	vector <vector <int> > ans(m, vector<int>(n, 0));
	vector <vector <pair<int, int> > > prev(m, vector<pair<int, int> >(n, make_pair(-1, -1)));
	pair <int, int> best_so_far = make_pair(-1, -1);
	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if(u[i] == v[j])
			{
				ans[i][j] = 1;
				for(int k1 = 0; k1 < i; k1++)
					for(int k2 = 0; k2 < j; k2++)
						if(u[k1] == v[k2] && u[k1] < u[i] && 1+ans[k1][k2] > ans[i][j])
						{
							ans[i][j] = ans[k1][k2]+1;
							prev[i][j] = make_pair(k1, k2);
						}
				if(best_so_far == make_pair(-1, -1) || ans[i][j] > ans[best_so_far.first][best_so_far.second])
					best_so_far = make_pair(i, j);
			}
		}
	}
	if(best_so_far.first == -1)
		cout << 0 << '\n';
	else
	{
		cout << ans[best_so_far.first][best_so_far.second] << '\n';
		pair<int, int> curr = best_so_far;
		stack <pair<int, int> > s;
		while(curr.first != -1)
		{
			s.push(curr);
			curr = prev[curr.first][curr.second];
		}
		while(s.size() > 0)
		{
			curr = s.top();
			cout << u[curr.first] << ' ';
			s.pop();
		}
	}
	return 0;
}