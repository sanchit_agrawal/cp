def does_intersect(p1, p2):
	return (not(p1[1] <= p2[0] or p1[0] >= p2[1])) and ((p1[0] > p2[0] and p1[1] > p2[1]) or (p1[0] < p2[0] and p1[1] < p2[1]))
	
n = int(raw_input())
ls = map(int, raw_input().split())
pairs = [(min(ls[i], ls[i+1]), max(ls[i], ls[i+1])) for i in range(n-1)]
intersects = False
for i, p1 in enumerate(pairs):
	for p2 in pairs[:i]:
		if does_intersect(p1, p2):
			# print p1, p2
			intersects = True
			
print ('yes' if intersects else 'no')