def strip_zeros(s):
    i = 0
    while i < len(s) and s[i] == '0':
        i += 1
    if i == len(s):
        return '0'
    return s[i:]

def add(a, b):
    carry = 0
    a = '0'*(max(len(a), len(b))-len(a)) + a
    b = '0'*(max(len(a), len(b))-len(b)) + b
    ls = []
    for i in range(len(a)-1, -1, -1):
        curr = ord(a[i])+ord(b[i])-2*ord('0')+carry
        ls.append(curr % 10)
        carry = curr / 10
    ls.append(carry)
    ls.reverse()
    ls = map(str, ls)
    s = "".join(ls)
    return strip_zeros(s)
    

def multiply(A, B):
    if len(B) > len(A):
        return multiply(B, A)
    if len(B) == 1:
        m = ord(B)-ord('0')
        ls = []
        carry = 0
        for i in range(len(A)-1, -1, -1):
            curr = ((ord(A[i])-ord('0'))*m)+carry
            ls.append(curr % 10)
            carry = curr / 10
        ls.append(carry)
        ls = map(str, ls)
        ls.reverse()
        s = "".join(ls)
        s = strip_zeros(s)
        print "1 len:", s
        return s
    m = len(B)
    s = multiply(A, B[:m/2]) + '0'*(len(B)-m/2)
    s2 = multiply(A, B[m/2:])
    print s, s2
    return add(s, s2)
                
s = raw_input()
t = raw_input()
print multiply(s, t)