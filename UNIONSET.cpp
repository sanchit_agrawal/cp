#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int last(vector <int>& u, vector <int>& v) {
	int i = 0, j = 0;
	int n = u.size(), m = v.size();
	for (int w = 1; ; w++) {
		bool found = false;
		while (i < n && u[i] < w) {
			i++;
		}
		while (j < m && v[j] < w) {
			j++;
		}
		if (i < n && u[i] == w || j < m && v[j] == w) {
			found = true;
		}
		if (!found) {
			return w;
		}
	}
}

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		int n, k;
		scanf("%d%d", &n, &k);
		vector <vector <int>> v(n);
		for (int i = 0; i < n; i++) {
			int len;
			scanf("%d", &len);
			for (int j = 0; j < len; j++) {
				int x;
				scanf("%d", &x);
				v[i].push_back(x);
			}
			sort(v[i].begin(), v[i].end());
		}
		int count = 0;
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				int l = last(v[i], v[j]);
				if (l == k+1) {
					count++;
				}
			}
		}
		printf("%d\n", count);
	}
	return 0;
}