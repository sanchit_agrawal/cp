n, x = map(int, raw_input().split())
ls = map(int, raw_input().split())
if x in ls:
	ls.sort()
	first_pos = n
	last_pos = -1
	for i, l in enumerate(ls):
		if l == x:
			first_pos = min(first_pos, i)
			last_pos = max(last_pos, i)
	if (n+1)/2-1 >= first_pos and (n+1)/2-1 <= last_pos:
		print 0
	else:
		
else:
	ls.append(x)
	ls.sort()
	n += 1
	first_pos = n
	last_pos = -1
	for i, l in enumerate(ls):
		if l == x:
			first_pos = min(first_pos, i)
			last_pos = max(last_pos, i)
	if (n+1)/2-1 >= first_pos and (n+1)/2-1 <= last_pos:
		print 1
	else:
		min_diff = min(abs((n+1)/2-1 - last_pos), abs((n+1)/2-1 - first_pos))
		print 2*min_diff-(n%2)+1