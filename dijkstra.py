import heapq

n, m = map(int, raw_input().split())
adjacency_list = [[] for i in range(n)]
for i in range(m):
	u, v, w = map(int, raw_input().split())
	adjacency_list[u].append((v, w))
dist = [float('inf') for i in range(n)]
src = int(raw_input())
dist[src] = 0
pq = [(d, i) for i, d in enumerate(dist)]
heapq.heapify(pq)
while len(pq) > 0:
	d, u = heapq.heappop(pq)
	if d > dist[u]:
		continue
	for v, w in adjacency_list[u]:
		if dist[u] + w < dist[v]:
			dist[v] = dist[u] + w
			heapq.heappush(pq, (dist[v], v))
print dist