#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

int main()
{
	int A[100001];
	int B[100001];
	int test;
	scanf("%d", &test);
	char s[100001];
	char t[100001];
	while(test--)
	{
		scanf("%s%s", s, t);
		int n = strlen(s), m = strlen(t);
		int a, b, k;
		scanf("%d%d%d", &a, &b, &k);
		for(int j = 0; j <= m; j++)
			A[j] = a*j;
		for(int i = 1; i <= n; i++)
		{
			int * curr = (i % 2 ? B : A);
			int * prev = (i % 2 ? A : B);
			curr[0] = a*i;
			for(int j = 1; j <= m; j++)
				if(s[i] == t[j])
					curr[j] = prev[j-1];
				else
					curr[j] = min(a + prev[j], min(a + curr[j-1], b + prev[j-1]));
		}
		int * ans = (n % 2 ? B : A);
		printf("%d\n", ans[m] > k ? -1 : ans[m]);
	}
	return 0;
}