#include <iostream>
#include <vector>
#include <deque>

using namespace std;

int main() {
	int n;
	cin >> n;
	vector <int> v(n);
	for (int i = 0; i < n; i++) {
		cin >> v[i];
	}
	int k;
	cin >> k;
	deque <int> q;
	for (int i = 0; i < k; i++) {
		while (q.size() > 0 && q.back() < v[i]) {
			q.pop_back();
		}
		q.push_back(v[i]);
	}
	cout << q.front() << ' ';
	for (int i = k; i < n; i++) {
		if (v[i-k] == q.front()) {
			q.pop_front();
		}
		while (q.size() > 0 && q.back() < v[i]) {
			q.pop_back();
		}
		q.push_back(v[i]);
		cout << q.front() << ' ';
	}
	return 0;
}