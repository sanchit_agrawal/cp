#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
using namespace std;

vector <int> weights;

bool possible(int balance, int turns, vector <vector<bool>>& dp, vector <vector<bool>>& done)
{
	if(turns == 0)
	{
		return dp[0][balance+10] = true;
	}
	else if(done[turns][balance+10])
		return dp[turns][balance+10];
	else
	{
		bool ans = false;
		for(int w: weights)
			if(w > abs(balance))
				if(balance >= 0)
					ans = ans || possible(balance - w, turns - 1, dp, done);
				else
					ans = ans || possible(w + balance, turns - 1, dp, done);
		done[turns][balance+10] = true;
		return dp[turns][balance+10] = ans;
	}
}

void backtrace(int balance, int turns, vector <vector<bool>>& dp, vector <int>& answer)
{
	if(turns == 0)
		return;
	for(int w: weights)
		if(w > abs(balance))
			if(balance >= 0)
			{
				if(dp[turns-1][balance-w+10])
				{
					backtrace(balance - w, turns - 1, dp, answer);
					answer.push_back(w);
					return;
				}
			}
			else
			{
				if(dp[turns-1][w+balance+10])
				{
					backtrace(w + balance, turns - 1, dp, answer);
					answer.push_back(w);
					return;
				}
			}
}

int main()
{
	string s;
	cin >> s;
	for(int i = 0; i < 10; i++)
		if(s[i] == '1')
			weights.push_back(i+1);
	int m;
	cin >> m;
	vector <vector<bool>> dp(m+1, vector <bool>(21, false));
	vector <vector<bool>> done(m+1, vector <bool>(21, false));
	if(possible(0, m, dp, done))
	{
		cout << "YES" << '\n';
		vector <int> ans;
		backtrace(0, m, dp, ans);
		for(int w : ans)
			cout << w << ' ';
	}
	else
	{
		cout << "NO";
	}
	return 0;
}