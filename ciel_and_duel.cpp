#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	vector <int> atk;
	vector <int> def;
	for (int i = 0; i < n; i++) {
		string s;
		int v;
		cin >> s >> v;
		if (s == "ATK") {
			atk.push_back(v);
		} else {
			def.push_back(v+1);
		}
	}
	vector <int> ciro(m);
	for (int i = 0; i < m; i++) {
		cin >> ciro[i];
	}
	sort(atk.begin(), atk.end());
	sort(def.begin(), def.end());
	sort(ciro.begin(), ciro.end());
	vector <bool> used(m, false);
	int damage_1 = 0;
	bool done = true;
	int i = 0, j = 0;
	while (true) {
		if (i == def.size()) {
			break;
		} else if (j == m) {
			done = false;
			break;
		} else if (ciro[j] >= def[i]) {
			used[j] = true;
			i++;
		}
		j++;
	}
	if (done) {
		i = 0;
		j = 0;
		while (true) {
			if (i == atk.size() && j == m) {
				break;
			} else if (j == m) {
				done = false;
				break;
			} else if (used[j]) {
				j++;
				continue;
			} else if (i == atk.size()) {
				damage_1 += ciro[j];
			} else if (ciro[j] >= atk[i]) {
				damage_1 += (ciro[j] - atk[i]);
				i++;
			} else {
				damage_1 += ciro[j];
			}
			j++;
		}
	}
	if (!done) {
		damage_1 = 0;
	}
	int damage_2 = 0;
	i = 0, j = m-1;
	while (true) {
		if (i == atk.size() || j == -1) {
			break;
		} else if (ciro[j] < atk[i]) {
			break;
		} else {
			damage_2 += (ciro[j] - atk[i]);
			i++;
		}
		j--;
	}
	cout << max(damage_1, damage_2);
	return 0;
}