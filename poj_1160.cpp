#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int v, p;
	scanf("%d%d", &v, &p);
	vector <int> pos(v);
	for(int i = 0; i < v; i++)
	{
		int temp;
		scanf("%d", &temp);
		pos[i] = temp;
	}
	vector <vector <int> > ans(v, vector<int>(p, 1000000000));
	for(int i = 0; i < v; i++)
	{
		ans[i][0] = 0;
		for(int j = 0; j < i; j++)
			ans[i][0] += pos[i]-pos[j];
	}
	for(int i = 0; i < p; i++)
		ans[i][i] = 0;
	for(int j = 1; j < p; j++)
	{
		for(int i = j+1; i < v; i++)
		{
			ans[i][j] = -1;
			for(int k = j-1; k < i; k++)
			{
				int curr_ans = ans[k][j-1];
				for(int t = k+1; t < i; t++)
					curr_ans += min(pos[t]-pos[k], pos[i]-pos[t]);
				if(ans[i][j] == -1 || curr_ans < ans[i][j])
					ans[i][j] = curr_ans;
			}
		}
	}
	int min_ans = 1000000000;
	for(int i = p-1; i < v; i++)
	{
		int curr_ans = ans[i][p-1];
		for(int j = i+1; j < v; j++)
			curr_ans += pos[j] - pos[i];
		if(min_ans == -1 || curr_ans < min_ans)
			min_ans = curr_ans;
	}
	printf("%d\n", min_ans);
	return 0;
}