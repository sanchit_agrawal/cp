#include <iostream>
#include <algorithm>
#include <iomanip>

using namespace std;

int main() {
	int n;
	cin >> n;
	double probs[100];
	for (int i = 0; i < n; i++) {
		cin >> probs[i];
	}
	sort(probs, probs+n);
	if (probs[n-1] == 1.0) {
		cout << 1.0;
		return 0;
	}
	int i;
	double p_sum = 0;
	for (i = n-1; p_sum < 1 && i >= 0 ; i--) {
		p_sum += probs[i] / (1.0 - probs[i]);
	}
	i++;
	double prod = 1;
	for (; i < n; i++) {
		prod *= (1.0 - probs[i]);
	}
	cout << setprecision(9) << prod * p_sum;
	return 0;
}
