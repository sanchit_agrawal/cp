n = int(raw_input())
ls = map(int, raw_input().split())
costs = map(int, raw_input().split())
counts = [0]*5
total_points = 0
for p in ls:
	total_points += p
	for i in range(4, -1, -1):
		num = total_points/costs[i]
		total_points -= costs[i]*num
		counts[i] += num
for c in counts:
	print c,
print ''
print total_points